#!/usr/bin/perl
#
# USAGE: qsystcl.pl <board> <proc> <cache> <out_dir>
#

use warnings;
use strict;

my ($board, $proc, $cache) = @ARGV;

# create the file to write to
my $file = "legup_system.tcl";
open FILE, '>'.$file;

print FILE "package require -exact qsys 13.0\n";
print FILE "load_system legup_system.qsys\n\n";

# choose the processor
if ($proc eq "MIPS"){
	print FILE "add_instance proc tiger_mips\n\n";
	print FILE "add_connection proc.data_master uart_0.s1\n";
	print FILE "set_connection_parameter_value proc.data_master/uart_0.s1 baseAddress \"0xf0001000\"\n\n";
	if ($board eq "DE2"){
		print FILE "add_connection clk_0.clk proc.clock\n";
		print FILE "add_connection clk_0.clk_reset proc.reset\n\n";
	}
}


# choose the cache
if ($cache eq "TIGER"){
	print FILE "add_instance cyc_count execution_cycle_counter\n";
	print FILE "add_instance icache tiger_icache\n";
	print FILE "add_instance av_b2p_adapter_0 av_burst_to_pipeline_adapter\n";
	print FILE "add_instance dcache tiger_dcache\n";
	print FILE "add_instance av_b2p_adapter_1 av_burst_to_pipeline_adapter\n\n";
	print FILE "add_connection proc.instruction_master cyc_count.slave\n";
	print FILE "add_connection cyc_count.master icache.icache_slave\n";
	print FILE "add_connection icache.icache_master av_b2p_adapter_0.adapter_slave\n";
	print FILE "add_connection av_b2p_adapter_0.adapter_master sdram.s1\n";
	print FILE "add_connection proc.data_master dcache.dcache_slave_0\n";
	print FILE "add_connection dcache.dcache_master_0 av_b2p_adapter_1.adapter_slave\n";
	print FILE "add_connection av_b2p_adapter_1.adapter_master sdram.s1\n\n";
	if ($board eq "DE2"){
		print FILE "add_connection clk_0.clk cyc_count.clock\n";
		print FILE "add_connection clk_0.clk_reset cyc_count.reset\n";
		print FILE "add_connection clk_0.clk icache.clock\n";
		print FILE "add_connection clk_0.clk_reset icache.reset\n";
		print FILE "add_connection clk_0.clk dcache.clock\n";
		print FILE "add_connection clk_0.clk_reset dcache.reset\n";
		print FILE "add_connection clk_0.clk av_b2p_adapter_0.clock\n";
		print FILE "add_connection clk_0.clk_reset av_b2p_adapter_0.reset\n";
		print FILE "add_connection clk_0.clk av_b2p_adapter_1.clock\n";
		print FILE "add_connection clk_0.clk_reset av_b2p_adapter_1.reset\n\n";
	}
}

print FILE "\nsave_system";

close FILE;
