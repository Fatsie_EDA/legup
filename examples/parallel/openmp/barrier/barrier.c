#define NUM_ACCEL 4
#define ARRAY_SIZE 20
#define OPS_PER_ACCEL ARRAY_SIZE/NUM_ACCEL

#include <stdio.h>

int input[ARRAY_SIZE] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};

int final_result=0;

int add ()
{
	int result=0;
	int i;
	for (i=0; i<ARRAY_SIZE; i++)
	{
        #pragma omp critical 
        {
    		final_result += input[i];
        }
	}
    #pragma omp barrier
	result = final_result;

    return result;
}


int
main ()
{
	int main_result=0;
	int i;
	//initialize barrier with the number of threads
	int result[NUM_ACCEL] = {0};

    #pragma omp parallel num_threads(NUM_ACCEL)
    {
        int tid = omp_get_thread_num();
		result[tid] = add();
	}
	 
	//check result
	for (i=0; i<NUM_ACCEL; i++) {
		printf("result[%d] = %d\n", i, result[i]);
		main_result += (result[i] == 840);
	}

	printf ("Result: %d\n", main_result);
	if (main_result == NUM_ACCEL) {
		printf("RESULT: PASS\n");
	} else {
		printf("RESULT: FAIL\n");
	}

	return 0;
}
