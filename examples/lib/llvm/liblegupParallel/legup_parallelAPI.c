//#define OMP_NUM_THREADS	(volatile int*)  0xC5000004
//#define OMP_THREAD_ID	(volatile int*)  0xC5000000

#define LOCK	(volatile int*)  0xC5000000
#define UNLOCK	(volatile int*)  0xC5000010
#define LOCK_OFFSET	8

#define BARRIER_INIT	(volatile int*)  0xC5001000
//#define BARRIER_WAIT	(volatile int*)  0xC5000044
#define BARRIER_WAIT	(volatile int*)  0xC5001010

/*
void legup_set_num_threads (int n) {
	*OMP_NUM_THREADS = n;
}

int legup_get_num_threads() {
	return *OMP_NUM_THREADS;
}

int legup_get_thread_num() {
	return *OMP_THREAD_ID;
}*/

void legup_lock(int threadID, int mutexNum) {
	do {
		*(LOCK+LOCK_OFFSET*mutexNum) = threadID;
    } while (*(LOCK+LOCK_OFFSET*mutexNum) != threadID);
}

void legup_unlock(int threadID, int mutexNum) {
	*(UNLOCK+LOCK_OFFSET*mutexNum) = threadID;
}

void legup_barrier_init(int n) {
	*BARRIER_INIT = n;
}

void legup_barrier_wait() {
	*BARRIER_WAIT = 1;
	while (*BARRIER_WAIT != 0) {
	}
}
