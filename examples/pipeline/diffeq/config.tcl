# turn on loop pipelining for loop
loop_pipeline "loop"

set_parameter PRINTF_CYCLES 1

set_parameter LOCAL_RAMS 1
set_parameter PARALLEL_LOCAL_RAMS 1
