module DE0-Nano(
	// Simple
	input 			CLOCK_50,
	input 	[0:0]	KEY,

	//  Memory (SDRAM)
	inout 	[15:0]	DRAM_DQ,
	output 	[12:0]	DRAM_ADDR,
	output 	[1:0]	DRAM_BA,
	output 			DRAM_CLK,
	output 			DRAM_CKE,
	output 			DRAM_CS_N,
	output 			DRAM_CAS_N,
	output 			DRAM_RAS_N,
	output 			DRAM_WE_N,
	output 	[1:0]	DRAM_DQM
);

	wire CLOCK_50i;
	
	// 50 MHz pll giving out -3ns phase shift for SDRAM clock
	pll50MHz pll50(.inclk0(CLOCK_50), .c0(CLOCK_50i), .c1(DRAM_CLK));

	legup_system legup_system_qsys(
		//clock and reset
		.clk_clk(CLOCK_50i),                     //                        clk.clk
		.reset_reset_n(KEY[0]),                  //                      reset.reset_n
		//UART
		.uart_0_external_connection_rxd(), 		 // uart_0_external_connection.rxd
		.uart_0_external_connection_txd(), 		 //                           .txd
		//SDRAM
		.sdram_wire_addr(DRAM_ADDR),             //                 sdram_wire.addr
		.sdram_wire_ba(DRAM_BA),                 //                           .ba
		.sdram_wire_cas_n(DRAM_CAS_N),           //                           .cas_n
		.sdram_wire_cke(DRAM_CKE),               //                           .cke
		.sdram_wire_cs_n(DRAM_CS_N),             //                           .cs_n
		.sdram_wire_dq(DRAM_DQ),                 //                           .dq
		.sdram_wire_dqm(DRAM_DQM),               //                           .dqm
		.sdram_wire_ras_n(DRAM_RAS_N),           //                           .ras_n
		.sdram_wire_we_n(DRAM_WE_N)              //                           .we_n
	);

endmodule