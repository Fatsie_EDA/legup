//===- SwOnly.cpp - LegUp pre-LTO pass ------------------------------------===//
//
// This file is distributed under the LegUp license. See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
// SwOnly - Replace accelerated functions with wrappers and create
// legup_wrappers.c. Create tcl files (legup_sopc.tcl, _hw.tcl) to 
// control SOPC builder to add the accelerator to the system. 
//===----------------------------------------------------------------------===//

#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/Module.h"
#include "llvm/Instructions.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/FormattedStream.h"
#include "llvm/Support/FileUtilities.h"
#include "llvm/Support/IRBuilder.h"
#include "llvm/Support/Signals.h"
#include "LegupConfig.h"
#include "utils.h"
#include <math.h>
#include <set>
#include <sstream>
#include "llvm/LLVMContext.h"

using namespace llvm;

namespace legup {

// SwOnly - Replace accelerated functions with wrappers and create
// legup_wrappers.c. Create tcl files (legup_sopc.tcl, _hw.tcl) to 
// control SOPC builder to add the accelerator to the system. 
class SwOnly  : public ModulePass {
public:

	static char ID; // Pass identification, replacement for typeid
	SwOnly() : ModulePass(ID),
	//initialize constant cache parameters
	FPGABoard(LEGUP_CONFIG->getFPGABoard()), dcachesize(LEGUP_CONFIG->getCacheParameters("dcachesize")), icachesize(LEGUP_CONFIG->getCacheParameters("icachesize")),
	//initialize other parameters
	sharedMemory(false) {
		//initialize other cache parameters (these are not constant since if they are not defined, they need to be set to default values later)
		dcachelinesize = LEGUP_CONFIG->getCacheParameters("dcachelinesize");
		dcacheway = LEGUP_CONFIG->getCacheParameters("dcacheway");
		dcacheports = LEGUP_CONFIG->getCacheParameters("dcacheports");
		icachelinesize = LEGUP_CONFIG->getCacheParameters("icachelinesize");
		icacheway = LEGUP_CONFIG->getCacheParameters("icacheway"); 
		dcacheType = LEGUP_CONFIG->getDCacheType();

		if (dcacheports > 2) {
			multiportedCache = true;
		} else {
			multiportedCache = false;
		}

		//DE2 processor does not support multi-ported caches due to CycloneII chip size
		if (FPGABoard == "DE2") {
			multiportedCache = false;
			dcacheports = 2;
			dcacheType = "";
		} 

		//ompUsed = false;
		lockUsed = false;
		barrierUsed = false;
		pthreadHeaderIncluded = false;
        perfCounterUsed = false;
        parallelAccelUsed = false;
        numPerfCounter = 0;
        numOMPatomic = 0;
	}

	virtual bool doInitialization(Module &M);
	virtual bool doFinalization(Module &M);
	virtual bool runOnModule(Module &M);

	virtual void getAnalysisUsage(AnalysisUsage &AU) const {
		AU.addRequired<LoopInfo>();
	}

private:

	bool replaceHwCallWithWrapper(CallInst *CI, Function *calledFunction, wrapperType type);
	void printWrapper(raw_ostream &Out, Function *F, const wrapperType type, const int AddressSize, const int numAccelerated, const std::vector<accelFcts> &AcceleratedFcts);
	void printWrapperPCIe(raw_ostream &Out, Function *F, const unsigned long long startAddress, const wrapperType type, const int wrapperNum, const int numAccelerated, const int AddressSize);
	void printWrapperPrototype(raw_ostream &Out, Function *F, bool &voidtype, const wrapperType type);
	void printWrapperBody(raw_ostream &Out, Function *F, const wrapperType type, const bool voidtype, const std::string AccelName, const int numAccelerated, const int AddressSize, const int numPthreadTypesWithMultipleThreads, const std::vector<accelFcts> &AcceleratedFcts);
	void printWrapperSendArguments(raw_ostream &Out, Function *F, const wrapperType type, const std::string AccelName, const int AddressSize);
	void printWrapperReturn(raw_ostream &Out, const bool voidtype, const wrapperType type, const std::string AccelName);
	void printWrapperThreadIDArgument(raw_ostream &Out, const wrapperType type, const std::string AccelName, const int AddressSize);
	void printWrapperSTATUSArgument(raw_ostream &Out, const wrapperType type, const std::string AccelName, const int AddressSize);
	void printWrapperAccelID(raw_ostream &Out, Function *F, const wrapperType type, const int numAccelerated, const int AddressSize);
	void printWrapperStoreMMAddress(raw_ostream &Out, const std::string AccelName);
	void printWrapperLoadMMAddress(raw_ostream &Out);
	void printWrapperOMPLoop(raw_ostream &Out, const int numAccelerated);
	void printWrapperPthreadGlobalVar(raw_ostream &Out, const std::string AccelName, const int numAccelerated);
	void printWrapperFreeAccelCaseStatement(raw_ostream &Out, const int numPthreadTypesWithMultipleThreads, const std::vector<accelFcts> &AcceleratedFcts);
	//void printWrapperFreeAccelCaseStatement(raw_ostream &Out, const int numPthreadTypesWithMultipleThreads, const std::vector<accelFcts> &AcceleratedFcts);
	unsigned long long printMemAddr(raw_ostream &Out, Function *F, unsigned long long CurAddr, wrapperType type, unsigned countOfAccelType = 1);
	void printDATApointer(raw_ostream &Out, const std::string AccelName, const Type *rT, unsigned long long &CurAddr);
	void printSTATUSpointer(raw_ostream &Out, const std::string AccelName, unsigned long long &CurAddr);
	void printARGpointers(raw_ostream &Out, const std::string AccelName, const wrapperType type, Function *F, unsigned long long &CurAddr);
	void printHWtcl(Function * F, wrapperType type, int AddrOffset);
	//void printHWtcl(Function * F, int AddrOffset);
	void printSopcFileInitial(raw_ostream &sopc);
	void printSopcFileAPIcores(raw_ostream &sopc);
	//void printSopcFile(raw_ostream &sopc, Function * F, unsigned long long baseAddr, int AccelCount);
	void printSopcFile(raw_ostream &sopc, Function * F, wrapperType type, unsigned long long baseAddr, int AccelCount, int AccelIndex);
	void printQSYSFileInitial(raw_ostream &qsys);
    void printQSYSFileAPIcores(raw_ostream &qsys);
   // void printQSYSFile(raw_ostream &qsys, Function * F, unsigned long long baseAddr, int AccelCount);
	void printQSYSFile(raw_ostream &qsys, Function * F, wrapperType type, unsigned long long baseAddr, int AccelCount, int AccelIndex);
	void printQSYSFilePCIe(raw_ostream &sopc, Function * F, unsigned long long baseAddr, int AccelCount);
	void printQSYSFilePCIeShared(raw_ostream &sopc, Function * F, unsigned long long baseAddr, int AccelCount);
	void printCacheParametersFile ();
	void printCacheHWtcl();
	void printSWfiles(std::vector<accelFcts> &AcceleratedFcts);
	void printSWfilesPCIe(const std::vector<accelFcts> &AcceleratedFcts);
	const char * printIntType (const Type * Ty);
	const char * printFloatType (const Type* Ty);
	std::string printType(const Type * Ty, bool MemAddr);
	void getSwFunctions(Module &M, std::set<GlobalValue*> &SwFcts);
	void addSwFunctions(Function *F, std::set<GlobalValue*> &SwFcts);
	void getdeleteFunctions(Module &M, std::set<GlobalValue*> SwFcts, std::set<GlobalValue*> &deleteFcts);
	void initWaveFile(raw_ostream &wave);
	void addAcceltoWaveFile(raw_ostream &wave, Function *F, int AccelCount);
	void finishWaveFile(raw_ostream &wave);
	bool replacePthreadCreate(Module &M, CallInst *CI, BasicBlock::iterator I, accelFcts &accel, const std::set<Function*> &internalAccels, unsigned loopTripCount);
	void replacePthreadJoin(Module &M, CallInst *CI, BasicBlock::iterator I, accelFcts &accel);
	void generateParallelAccelConfigs(const std::vector<accelFcts> &AcceleratedFcts);
	void setCacheParameters();
	void setdefaultCacheParameters(const std::string cacheType, const int cachesize, int &cachelinesize, int &cacheway, int &cachenumlines);
	void checkLinesizes();
	void checkMaximumLinesize(const std::string cacheType, const int cachelinesize, const int maxValue);
	void addAcceleratedFct(const bool isAvalonAccel, accelFcts accel, std::vector<accelFcts> &AcceleratedFcts);
	void updateAcceleratedFct(const bool isAvalonAccel, accelFcts accel, std::vector<accelFcts> &AcceleratedFcts);
	bool replaceCalls(Module &M, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts);
	bool replaceCallsPCIe(Module &M, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts);	
    bool addPerformanceCounters(CallInst *CI, Function *calledFunction);
	bool replaceOMPFunctions(Module &M, Function::iterator BB, BasicBlock::iterator I, CallInst *CI, Function *calledFunction, int &numOMPthreads, std::string &ompFuncName, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts);
	bool replacePthreadFunctions(Module &M, Function *F, Function::iterator BB, BasicBlock::iterator I, CallInst *CI, Function *calledFunction, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts);
	void updateAcceleratedFcts(Function *calledFunction, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts, unsigned numAccelerated);
	void addAcceleratedFcts(Function *calledFunction, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts);
	bool replaceSequentialFunction(CallInst *CI, Function *calledFunction);
	void checkSharedFunction(Function *calledFunction);
	void identifyPthreadFunctionsPCIe(Module &M, CallInst *CI, std::vector<accelFcts> &AcceleratedFcts, const std::set<Function*> &internalAccels, unsigned loopTripCount);
	unsigned int getLoopTripCount(Function *pF, Function::iterator BB);
	void updateAccelCount(Function *F,  const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts, unsigned accelCount);
	
	//variables which hold cache parameters and type of fpga board
	const std::string FPGABoard;
	const int dcachesize, icachesize;
	int dcachelinesize, icachelinesize; 
	int dcacheway, icacheway;
	int dcachenumlines, icachenumlines;
	int dcacheports;
    int numPerfCounter;   
    int numOMPatomic; 
	std::string dcacheType;
	bool multiportedCache;
	//bool ompUsed, lockUsed, barrierUsed;
	bool lockUsed, barrierUsed, perfCounterUsed, parallelAccelUsed;
	bool pthreadHeaderIncluded;

	LoopInfo *LI;
	bool sharedMemory;

	std::map<std::string, int> mutexMap;
	std::map<int, std::set<Function*> > mutexFunctionMap;
};

char SwOnly::ID = 0;
static RegisterPass<SwOnly> Z("legup-sw-only",
	"Replace accelerated functions with wrappers and produce legup_wrappers");

bool SwOnly::doInitialization(Module &M) {
	return false;
}

bool SwOnly::doFinalization(Module &M) {
	return false;
}

bool SwOnly::runOnModule(Module &M) {
	bool modified = false;
	modified |= doInitialization(M);

	std::set<GlobalValue*> SwFcts;
	std::set<GlobalValue*> deleteFcts;
	std::set<Function*> internalAccels;

	//vector of structs accelFcts which hold the functions to be accelerated
	std::vector<accelFcts> AcceleratedFcts;
	//map for pthread functions, where key is the thread variable and value is the name of the function being forked to
	//std::map<Value*, std::string> pthreadFcts;
	
	getSwFunctions(M, SwFcts);	
	getdeleteFunctions(M, SwFcts, deleteFcts);		

	//if accelerated function is main
	if (LEGUP_CONFIG->isAccelerated("main")) {
		assert(0 && "main function cannot be accelerated!\n");
	}

	//find all accelerators which are internal to another accelerator
	//internal accelerators are created when an accelerated function contains pthreads or openmp 
	//Only the uppermost function (whether it's a user-designated function, pthread function, or openmp function) are created as Avalon accelerators.
	findInternalAccels(M, internalAccels);

	if (!LEGUP_CONFIG->isPCIeFlow()) {
		modified |= replaceCalls(M, internalAccels, AcceleratedFcts);
	} else {
		modified |= replaceCallsPCIe(M, internalAccels, AcceleratedFcts);
	}
	
	generateParallelAccelConfigs(AcceleratedFcts);
	setCacheParameters();
	if (!LEGUP_CONFIG->isPCIeFlow()) {
		printSWfiles(AcceleratedFcts);
	} else {
		printSWfilesPCIe(AcceleratedFcts);
	}

	modified |= deleteGV(deleteFcts);

	modified |= doFinalization(M);
	return modified;
}

void SwOnly::addAcceleratedFct(const bool isAvalonAccel, accelFcts accel, std::vector<accelFcts> &AcceleratedFcts) {
	//add to AcceleratedFcts only if it doesn't exist already
	if ((find(AcceleratedFcts.begin(), AcceleratedFcts.end(), accel) == AcceleratedFcts.end()) && isAvalonAccel) {
		AcceleratedFcts.push_back(accel);
		// errs() << " Accelerator: " << accel.fct->getName().str() << "\n";
		// errs() << " Added " << accel.numAccelerated << " accelerators\n";
	}
}

void SwOnly::updateAcceleratedFct(const bool isAvalonAccel, accelFcts accel, std::vector<accelFcts> &AcceleratedFcts) {
	//add to AcceleratedFcts only if it doesn't exist already
	std::vector<accelFcts>::iterator found = find(AcceleratedFcts.begin(), AcceleratedFcts.end(), accel);

	// errs() << " Accelerator: " << accel.fct->getName().str() << "\n";
	if ((found == AcceleratedFcts.end()) && isAvalonAccel) {
		AcceleratedFcts.push_back(accel);	
		// errs() << " Added " << accel.numAccelerated << " accelerators\n";
	} else {
		// errs() << "  Updating numAccelerated from: " << found->numAccelerated << " to ";
		found->numAccelerated += accel.numAccelerated;

		// errs() << found->numAccelerated << "\n";
	}
}

void SwOnly::printSWfilesPCIe(const std::vector<accelFcts> &AcceleratedFcts) {

	unsigned long long StartAddr_Wrapper = 0x00000000; //The actual memory address assigned to accelerator, will be the one printed out in C wrapper file
	unsigned long long StartAddr_qsys = 0x00000000; //Memory address used in qsys. Pipeline bridges base address set to f0000000, hence the accelerator only needs the offset from Pipeline bridges base address
	unsigned long long CurrAddr_Wrapper;
	std::string AccelName;	
	int AccelCount = 0, AddrOffset = 0, AddressBusWidth = 0, AddressSize = 0;
	int numAccelerated;

	//make file stream for C wrapper file
	formatted_raw_ostream *file;	
	string wrapperFile = "legup_pcie_wrappers_generated.c"; //wrapper file for accelerator
	file = GetOutputStream(wrapperFile);
	assert(file);
	raw_ostream &Out = *file;

	//make file stream for qsys commands
	formatted_raw_ostream *qsys_file;
	string qsysFile = "legup_qsys_pcie_generated.tcl"; //tcl script to run QSYS
	qsys_file = GetOutputStream(qsysFile);
	assert(qsys_file);
	raw_ostream &qsys = *qsys_file;

	// print C wrapper for all accelerated functions
	// note: we don't need a wrapper for functions _called_ by accelerated
	// functions
	if (!AcceleratedFcts.empty()) {
		wrapperType type;
		Function *F;

		qsys << "load_system pcie_tutorial/qsys_system.qsys\n";

		Out << "#include \"alt_up_pci_lib_helper.h\"\n";
		Out << "#include \"legup_shared_accelerators.h\"\n\n";
		Out << "#ifdef DEBUG_SIM\n"
		    << "#include <stdio.h>\n"
		    << "#endif\n\n";

		for (std::vector<accelFcts>::const_iterator I = AcceleratedFcts.begin(), E = AcceleratedFcts.end(); I != E; ++I) {
			type = (*I).type;	
			F = (*I).fct;
			// For parallel accelerators
			// 		Generate Wrapper
			// 		Generate HW.tcl file
			//		Add in QSYS system
			numAccelerated = (*I).numAccelerated;


			unsigned long long tempStartAddrWrapper = StartAddr_Wrapper & 0x0fffffff;
			CurrAddr_Wrapper = printMemAddr(Out, F, StartAddr_Wrapper, type, numAccelerated);
			AddrOffset = (CurrAddr_Wrapper - StartAddr_Wrapper) / numAccelerated;
			AddressBusWidth = ceil(log (AddrOffset/4) / log (2));
			AddressSize = 4 * pow(2, AddressBusWidth);
			AccelCount++;
			printWrapperPCIe(Out, F, tempStartAddrWrapper, type, AccelCount, numAccelerated, AddressSize);

			if (!sharedMemory) {
				printQSYSFilePCIe(qsys, F, StartAddr_qsys, numAccelerated);
			}
			else {
				printQSYSFilePCIeShared(qsys, F, StartAddr_qsys, numAccelerated);
			}

			printHWtcl(F, type, AddrOffset);
			StartAddr_Wrapper = StartAddr_Wrapper + AddressSize;
			StartAddr_qsys = StartAddr_qsys + AddressSize;
		}
	} else {
		assert(0 && "Accelerated function is never called or optimized away!\n");
	}

	delete file;
	delete qsys_file;

}

void SwOnly::printSWfiles(std::vector<accelFcts> &AcceleratedFcts) {

	unsigned long long StartAddr_Wrapper = 0xf0000000; //The actual memory address assigned to accelerator, will be the one printed out in C wrapper file
	unsigned long long StartAddr_Sopc = 0x00000000; //Memory address used in SOPC Builder. Pipeline bridges base address set to f0000000, hence the accelerator only needs the offset from Pipeline bridges base address
	unsigned long long StartAddr_Qsys = 0x00000000; //Memory address used in QSYS. Pipeline bridges base address set to f0000000, hence the accelerator only needs the offset from Pipeline bridges base address
	unsigned long long CurrAddr_Wrapper;
	std::string AccelName;	
	int AccelCount = 0, AddrOffset = 0, AddressBusWidth = 0, AddressSize = 0;
	int numAccelerated;

	//make file stream for C wrapper file
	formatted_raw_ostream *file;	
	string wrapperFile = "legup_wrappers.c"; //wrapper file for accelerator
	file = GetOutputStream(wrapperFile);
	assert(file);
	raw_ostream &Out = *file;

	//make file stream for Modelsim waves
	formatted_raw_ostream *wave_file;
	string waveFile = "wave.do"; //wave.do file for Modelsim (automatically adds certain signals to wave window)
	wave_file = GetOutputStream(waveFile);
	assert(wave_file);
	raw_ostream &wave = *wave_file;

	//make file stream for Sopc commands
	formatted_raw_ostream *sopc_file;
	string sopcFile = "legup_sopc.tcl"; //tcl script to run SOPC builder
	sopc_file = GetOutputStream(sopcFile);
	assert(sopc_file);
	raw_ostream &sopc = *sopc_file;

	//make file stream for QSYS commands
	formatted_raw_ostream *qsys_file;
	string qsysFile = "legup_qsys.tcl"; //tcl script to run QSYS
	qsys_file = GetOutputStream(qsysFile);
	assert(qsys_file);
	raw_ostream &qsys = *qsys_file;

	//initializing modelsim wave file with common processor signals
	initWaveFile(wave);

	// print C wrapper for all accelerated functions
	// note: we don't need a wrapper for functions _called_ by accelerated
	// functions
	if (!AcceleratedFcts.empty()) {
		wrapperType type;
		Function *F;
		bool pthreadPollUsed = false;

		if(LEGUP_CONFIG->getParameterInt("USE_QSYS")) {
			qsys << "package require -exact qsys 13.0\n";
			qsys << "load_system tiger/tiger.qsys\n";
			printQSYSFileInitial(qsys);
		} else {
			sopc << "load_system tiger/tiger.sopc\n";
			printSopcFileInitial(sopc);
		}
		printCacheParametersFile();
		printCacheHWtcl();
		for (std::vector<accelFcts>::iterator I = AcceleratedFcts.begin(), E = AcceleratedFcts.end(); I != E; ++I) {
			type = (*I).type;
			F = (*I).fct;
			//F->addFnAttr(1<<11); //adding noinline attribute to function
			//for parallel accelerators, generated wrapper and sopc function for each instance
			numAccelerated = (*I).numAccelerated;
//			for (int i=0; i<numAccelerated; i++) {
				//for pthread/openMP calling wrappers and sequential wrappers
				if (type == pthreadcall || type == ompcall || type == seq) {
					//print address declarations
					CurrAddr_Wrapper = printMemAddr(Out, F, StartAddr_Wrapper, type);
					//calculate offset between current and next wrapper
					AddrOffset = CurrAddr_Wrapper - StartAddr_Wrapper;
					//calculate avalon slave bus width for HW accelrator
					AddressBusWidth = ceil(log (AddrOffset/4) / log (2));
					//calculate the address size mapped to current HW accelerator
					AddressSize = 4 * pow(2, AddressBusWidth);
					(*I).addressSize = AddressSize;

					printWrapper(Out, F, type, AddressSize, numAccelerated, AcceleratedFcts);
					//printWrapper(Out, F, type, numcallingWrapper);
				
					//add the HW accelerator to sopc system as many times as the number of threads	
					for (int i=0; i<numAccelerated; i++) {
						//print tcl which controls SOPC builder to add accelerator to system
						//printSopcFile(sopc, F, type, StartAddr_Sopc, i, AccelCount);

						if(LEGUP_CONFIG->getParameterInt("USE_QSYS")) {
							//print tcl which controls QSYS to add accelerator to system
							printQSYSFile(qsys, F, type, StartAddr_Qsys, i, AccelCount);
						} else {
							//print tcl which controls SOPC builder to add accelerator to system
							printSopcFile(sopc, F, type, StartAddr_Sopc, i, AccelCount);
						}
						printHWtcl(F, type, AddrOffset);
						StartAddr_Wrapper += AddressSize;
						StartAddr_Sopc += AddressSize;
						StartAddr_Qsys += AddressSize;
						//Adding accelerator top signals to Modelsim wave file	
						addAcceltoWaveFile(wave, F, i);	
						AccelCount++;
					}
					//strange issue with address mapping when sequential accelerators some after parallel accelerators?
					//even though address maps do not overlap, sopc complains that HW accelerator cannot be mapped to an address	
					//hack for now
					//StartAddr_Wrapper += 0x10;
					//StartAddr_Sopc += 0x10;
				//for openMP polling wrappers
				} else if (type == omppoll) {
					printWrapper(Out, F, type, AddressSize, numAccelerated, AcceleratedFcts);
				//for Pthread polling wrappers
				} else {
					pthreadPollUsed = true;
				}
//			}
		}
		//pthread polling wrapper needs to be printed out very last
		//since there is only one wrapper for all pthread functions
		//and you need to know how many types of pthread functions have more than 1 thread
		//to print the case statement inside the wrapper
		if (pthreadPollUsed) {
			printWrapper(Out, NULL, pthreadpoll, 0, 1, AcceleratedFcts);
		}
		if(LEGUP_CONFIG->getParameterInt("USE_QSYS")) {
			qsys << "save_system\n\n";
		} else {
			sopc << "save_system\n\n";
			sopc << "generate_system\n";
		}
	} else {
		assert(0 && "Accelerated function is never called or optimized away!\n");
	}

	finishWaveFile(wave);

	delete file;
	delete wave_file;
	if(LEGUP_CONFIG->getParameterInt("USE_QSYS")) {
		delete qsys_file;
	} else {
		delete sopc_file;
	}

}

//returns true if replaced
bool SwOnly::addPerformanceCounters(CallInst *CI, Function *calledFunction) {

	bool replaced = false;
	if (calledFunction->getName().str() == "legup_start_counter") {
        //errs () << "COUNTER FOUND!\n\n";
		replaced = true;
        perfCounterUsed = true;
		Value *argValue = CI->getArgOperand(0);
        if (ConstantInt *constantInt = dyn_cast<ConstantInt>(argValue)) {
            int index = constantInt->getSExtValue();
            if (index > numPerfCounter) 
                numPerfCounter = index;
        } else {
            assert(0 && "counter offset has to be a constant integer!\n");
        }
	} 

	return replaced;
}

//returns true if replaced
bool SwOnly::replacePthreadFunctions(Module &M, Function *F, Function::iterator BB, BasicBlock::iterator I, CallInst *CI, Function *calledFunction, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts) {

	bool replaced = false, isAvalonAccel = false;
	accelFcts accel; unsigned int loopTripCount;
	//pthread_create
	if (calledFunction->getName().str() == "pthread_create") {
		//check if this pthread create is in a loop
		//if so, get the loop trip count to get the number of parallel accelerators required
		loopTripCount = getLoopTripCount(F, BB);
		isAvalonAccel = replacePthreadCreate(M, CI, I, accel, internalAccels, loopTripCount);			
		updateAcceleratedFct(isAvalonAccel, accel, AcceleratedFcts);
        parallelAccelUsed = true;
		replaced = true;
	} 
	//if this is a pthread join
	else if (calledFunction->getName().str() == "pthread_join") {
		replacePthreadJoin(M, CI, I, accel);
		updateAcceleratedFct(true, accel, AcceleratedFcts);
		replaced = true;
	}
	//if locks are used in the program
	else if (calledFunction->getName().str() == "pthread_mutex_lock") {

		//get the name of mutex
		std::string mutexName = CI->getArgOperand(0)->getName().str();
						
		//insert the mutex into map if it hasn't been already
		int mutexNum = mutexMap.size();
		if (mutexMap.find(mutexName) == mutexMap.end()) {
			mutexMap.insert( std::pair<std::string, int>(mutexName, mutexNum));
		std::set<Function*> callerSet;
		callerSet.insert(F);
		//we want to find all the functions that call the current function
		findCallerFunctions2(F, callerSet);
		//this map keeps track of what are the caller functions to the current function which uses the lock
		//this is to be used later to determine which accelerator should connect to which mutex
		//this is needed because the function which uses mutex_lock may not be the top most function for that accelerator
		mutexFunctionMap[mutexNum] = callerSet;
		}


		lockUsed = true;
		//replaced = true;
	}
	//if barriers are used in the program
	else if (calledFunction->getName().str() == "legup_barrier_init") {
		barrierUsed = true;
		//replaced = true;
	}

	return replaced;
}


//replace OpenMp functions, returns true if replaced
bool SwOnly::replaceOMPFunctions(Module &M, Function::iterator BB, BasicBlock::iterator I, CallInst *CI, Function *calledFunction, int &numOMPthreads, std::string &ompFuncName, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts) {

	accelFcts accel;
	bool replaced = false, isAvalonAccel = false;
	//GOMP_parallel_start
	if (calledFunction->getName().str() == "GOMP_parallel_start") {
		
		//OpenMP is used in this program	
		//ompUsed = true;
		//replace call to OMP function call with legup function calls
		isAvalonAccel = replaceOMPParallelStart(M, BB, I, CI, accel, numOMPthreads, ompFuncName, internalAccels);
		//add this accelerated function if it doesn't exist already
		addAcceleratedFct(isAvalonAccel, accel, AcceleratedFcts);
        parallelAccelUsed = true;
		replaced = true;
	}
	//GOMP_parallel_end
	else if (calledFunction->getName().str() == "GOMP_parallel_end") {	

		//OpenMP is used in this program	
		//ompUsed = true;
		//replace call to OMP function call with legup function calls
		isAvalonAccel = replaceOMPParallelEnd(M, I, CI, accel, numOMPthreads, ompFuncName, internalAccels);
		//add this accelerated function if it doesn't exist already
		addAcceleratedFct(isAvalonAccel, accel, AcceleratedFcts);
		replaced = true;
	} 
	//if there is a function call to the OMP function, we delete it, otherwise this function will be executed on the processor
	else if (calledFunction->getName().str().c_str() == ompFuncName) {
		//delete the original call instruction 
		deleteInstruction(CI);
		replaced = true;
	}
	else if (calledFunction->getName().str() == "GOMP_critical_start" || calledFunction->getName().str() == "GOMP_atomic_start") {

		std::string mutexName;
        //this is for unname critical section
        //which means it uses one mutex
        if (calledFunction->getName().str() == "GOMP_critical_start") {
		    mutexName = "OMP";
        } else if (calledFunction->getName().str() == "GOMP_atomic_start") {
            //this is for atomic section, which means it is used for a single memory update right after the pragma
            //and every atomic section uses a different lock
            mutexName = "OMP_atomic_" + utostr(numOMPatomic);
            numOMPatomic++;
        }

						
		//insert the mutex into map if it hasn't been already
		int mutexNum = mutexMap.size();
		if (mutexMap.find(mutexName) == mutexMap.end()) {
			mutexMap.insert( std::pair<std::string, int>(mutexName, mutexNum));
		std::set<Function*> callerSet;
		callerSet.insert(BB->getParent());
		//we want to find all the functions that call the current function
		findCallerFunctions2(BB->getParent(), callerSet);
		//this map keeps track of what are the caller functions to the current function which uses the lock
		//this is to be used later to determine which accelerator should connect to which mutex
		//this is needed because the function which uses mutex_lock may not be the top most function for that accelerator
		mutexFunctionMap[mutexNum] = callerSet;
		}


		lockUsed = true;
		//replaced = true;
	}

	return replaced;
}


bool SwOnly::replaceSequentialFunction(CallInst *CI, Function *calledFunction) {

	bool modified=false;
	if (LEGUP_CONFIG->isAccelerated(*calledFunction)) {
		modified |= replaceHwCallWithWrapper(CI, calledFunction, seq);
	}
	return modified;
}

void SwOnly::checkSharedFunction(Function *F) {

	if (sharedMemory) {
		return;
	}
	if (!LEGUP_CONFIG->isAccelerated(*F)) {
		return;
	}

	// iterate arguments, checking for pointer arguments
	for (Function::arg_iterator i = F->arg_begin(), e = F->arg_end(); i != e && !sharedMemory; ++i) {
		if (i->getType()->isPointerTy()) {
			sharedMemory = true;
		}
	}
}

void SwOnly::updateAcceleratedFcts(Function *calledFunction, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts, unsigned numAccelerated) {
	
	accelFcts accel;
	bool isAvalonAccel;
	if (LEGUP_CONFIG->isAccelerated(*calledFunction)) {
		accel.fct = calledFunction;
		accel.type = seq;
		accel.numAccelerated = numAccelerated;
		isAvalonAccel = (internalAccels.find(calledFunction) == internalAccels.end());
		updateAcceleratedFct(isAvalonAccel, accel, AcceleratedFcts);
	}
}

void SwOnly::addAcceleratedFcts(Function *calledFunction, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts) {

	accelFcts accel;
	bool isAvalonAccel;
	if (LEGUP_CONFIG->isAccelerated(*calledFunction)) {
		accel.fct = calledFunction;
		accel.type = seq;
		accel.numAccelerated = 1;
		isAvalonAccel = (internalAccels.find(calledFunction) == internalAccels.end());
		addAcceleratedFct(isAvalonAccel, accel, AcceleratedFcts);
	}
}

bool SwOnly::replaceCalls(Module &M, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts) {

	bool modified = false;
	int numOMPthreads=0;
	std::string ompFuncName;

	// replace all calls to accelerated functions with calls to the wrapper
	for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {
		for (Function::iterator BB = F->begin(), EE = F->end(); BB != EE; ++BB) {
			for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E;) {
				if (CallInst *CI = dyn_cast<CallInst>(I++)) {

					Function *calledFunction = CI->getCalledFunction();
					//ignore indirect function calls
					if (!calledFunction) continue;

					//replace OpenMP functions if there are any, continue if replaced
					if (replaceOMPFunctions(M, BB, I, CI, calledFunction, numOMPthreads, ompFuncName, internalAccels, AcceleratedFcts)) continue;

					//replace Pthread functions if there are any, continue if replaced
					if (replacePthreadFunctions(M, F, BB, I, CI, calledFunction, internalAccels, AcceleratedFcts)) continue;

					//add performance counters if there are any, continue if replaced
					if (addPerformanceCounters(CI, calledFunction)) continue;

					//replace sequential functions if there are any
					addAcceleratedFcts(calledFunction, internalAccels, AcceleratedFcts);
					modified |= replaceSequentialFunction(CI, calledFunction);
				}
			}
		}
	}
	return modified;
}

bool SwOnly::replaceCallsPCIe(Module &M, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts) {

	bool modified = false;
	unsigned loopTripCount;

	for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {
		for (Function::iterator BB = F->begin(), EE = F->end(); BB != EE; ++BB) {
			for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E;) {
				if (CallInst *CI = dyn_cast<CallInst>(I++)) {

					Function *calledFunction = CI->getCalledFunction();
					// ignore indirect function calls
					if (!calledFunction) continue;

					if (calledFunction->getName().str() == "pthread_create") {
						loopTripCount = getLoopTripCount(F, BB);
						identifyPthreadFunctionsPCIe(M, CI, AcceleratedFcts, internalAccels, loopTripCount);
					}
				}
			}
		}
	}

	for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {

		if (F->getName().str().find("pthread") != std::string::npos) continue;

		for (Function::iterator BB = F->begin(), EE = F->end(); BB != EE; ++BB) {
			for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E;) {
				if (CallInst *CI = dyn_cast<CallInst>(I++)) {

					Function *calledFunction = CI->getCalledFunction();
					//ignore indirect function calls
					if (!calledFunction) continue;
					// errs() << "FirstPass Function : " << calledFunction->getName() << " <- Called by : " << F->getName() << "\n";
					addAcceleratedFcts(calledFunction, internalAccels, AcceleratedFcts);
				}
			}
		}
	}

	// replace all calls to accelerated functions with calls to the wrapper
	for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {
		for (Function::iterator BB = F->begin(), EE = F->end(); BB != EE; ++BB) {
			for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E;) {
				if (CallInst *CI = dyn_cast<CallInst>(I++)) {

					Function *calledFunction = CI->getCalledFunction();
					//ignore indirect function calls
					if (!calledFunction) continue;

					modified |= replaceSequentialFunction(CI, calledFunction);

					checkSharedFunction(calledFunction);
				}
			}
		}
	}

	return modified;
}

/// replaceHwCallWithWrapper - replace the call instruction with a call to the
/// legup wrapper function
bool SwOnly::replaceHwCallWithWrapper(CallInst *CI, Function *calledFunction, wrapperType type)
{
	string wrapperName = getWrapperName(calledFunction, type);

	ReplaceCallWith(wrapperName.c_str(), CI, copyArguments(CI->op_begin(), CI->op_end()-1), calledFunction->getReturnType());

	return false;
}


void SwOnly::printWrapperPrototype(raw_ostream &Out, Function *F, bool &voidtype, const wrapperType type) {
	std::string functionPrototype;
	string wrapperName = getWrapperName(F, type);

	const Type* rT;
	if (type == pthreadpoll) {
		rT = Type::getInt8PtrTy(getGlobalContext());
	} else {
		rT = F->getReturnType();
	} 

	//print return type
	if (type == pthreadcall || type == ompcall || type == omppoll) {
		//if calling function, it's always void
		//omppolling wrapper is always void as well
		voidtype = true;
		functionPrototype.append("void ");
	} else if (type == pthreadpoll) {
		//pthread poll type will always be non-void, as it can be used for both void and non-void functions
		voidtype = false;
		functionPrototype.append(printType(rT, false));
	} else {
		if (rT->getTypeID() == Type::VoidTyID) {
			voidtype = true;
			functionPrototype.append("void ");
		} else {
			functionPrototype.append(printType(rT, false));
		}
	}

/*
	//wrapper function name, add indexes if this is a parallel function
	if (type == call || type == poll) { 
		std::stringstream ss;
		ss << wrapperNum;
		wrapperName+=ss.str();
	}
*/

	functionPrototype.append(wrapperName);		
	functionPrototype.append("(");

	//for either calling or sequential wrapper
	if (type == pthreadcall || type == ompcall || type == seq) {
		//print each argument
		for (Function::arg_iterator i = F->arg_begin(), e = F->arg_end(); i != e; ++i) {
			if (i == F->arg_begin())
				functionPrototype.append("");					
			else
				functionPrototype.append(", ");		
			functionPrototype.append(printType(i->getType(), false));
			std::string argName = i->getName();
			stripInvalidCharacters(argName);
			functionPrototype.append(argName);
		}
//		functionPrototype.append(")");
	}

	//for pthread calling/polling wrappers, pass in the thread variable
	if (type == pthreadcall) {
		if (!F->arg_empty()) {
			functionPrototype.append(", ");		
		}
		functionPrototype.append("pthread_t *threadVar");		
	} else if (type == pthreadpoll) {
		functionPrototype.append("pthread_t threadVar");		
	} else if (type == ompcall) {
	//for openMP calling wrapper, pass in the number of threads
//		functionPrototype.append(", int numThreads");
	}
	functionPrototype.append(")");

	//print out function prototype
	Out << functionPrototype << "\n{\n"; 
}

/*
void SwOnly::printWrapperThreadVariableArgument(raw_ostream &Out, const wrapperType type, const std::string AccelName, const int threadVariable) {

	//the threadID is equal to the memory-mapped address of this instance of HW accelerator
	//add_THREADID+LEGUP_ADDR_OFFSET = (volatile int) add_DATA+THREAD_ADDR_OFFSET;
	if (type == pthreadcall) {
		Out << "\t*" << AccelName << "_THREADID+THREAD_ADDR_OFFSET = (volatile int) " << AccelName << "+THREAD_ADDR_OFFSET;\n"; 
	} else if (type == pthreadpoll) {
		Out << "\t*" << "_ARG" << ArgIdx << " = (volatile int) "; 
	}
}
*/

//print wrapper code for sending threadID to HW accelerator
void SwOnly::printWrapperThreadIDArgument(raw_ostream &Out, const wrapperType type, const std::string AccelName, const int AddressSize) {

	if (type == pthreadcall) {
		Out << "\t*(" << AccelName << "_THREADID+LEGUP_ADDR_OFFSET) = (volatile int) (" << AccelName << "_DATA+LEGUP_ADDR_OFFSET);\n"; 
	} else if (type == ompcall) {
		//addressSize is divided by 4 since STATUS pointer is an integer type
		Out << "\t*(" << AccelName << "_THREADMUTEXID+i*" << AddressSize/4 << ") = (volatile int) " << AccelName << "_STATUS+i*" << AddressSize/4 << ";\n";
	}
}

/*
void SwOnly::printWrapperThreadIDArgument(raw_ostream &Out, const wrapperType type, const std::string AccelName, const int wrapperNum, int &ArgIdx, const int threadID) {

	if (type == call) {
		Out << "\t*" << AccelName << wrapperNum << "_ARG" << ArgIdx << " = (volatile int) "; 
	} else {
		Out << "\t*" << AccelName << "_ARG" << ArgIdx << " = (volatile int) "; 
	}
	Out << threadID << ";\n";
	ArgIdx++;
}
*/
void SwOnly::printWrapperSTATUSArgument(raw_ostream &Out, const wrapperType type, const std::string AccelName, const int AddressSize) {

	//for pthread calling wrapper
	if (type == pthreadcall) {
		Out << "\t*(" << AccelName << "_STATUS+LEGUP_ADDR_OFFSET) = 1;\n";
	//for omp calling wrapper
	} else if (type == ompcall) {
		//addressSize is divided by 4 since STATUS pointer is an integer type
		Out << "\t*(" << AccelName << "_STATUS+i*" << AddressSize/4 << ") = 1;\n";
	//for pthread polling wrapper
	} else if (type == pthreadpoll) {
		Out << "\twhile (*STATUS == 0){}\n";
	//for omp polling wrapper
	} else if (type == omppoll) {
		//addressSize is divided by 4 since STATUS pointer is an integer type
		Out << "\twhile (*(" << AccelName << "_STATUS+i*" << AddressSize/4 << ") == 0){}\n";
	//for sequential wrapper
	} else {
		Out << "\t*" << AccelName << "_STATUS = 1;\n";
	}
}

void SwOnly::printWrapperSendArguments(raw_ostream &Out, Function *F, const wrapperType type, const std::string AccelName, const int AddressSize) {

	int ArgIdx = 1;
	Type * ITy;
	std::string argName;

	//if calling or seq wrapper, send arguments and give start signal
	if (type == pthreadcall || type == ompcall || type == seq) {
		
		//for each argument in this function
		for (Function::arg_iterator i = F->arg_begin(), e = F->arg_end(); i != e; ++i) {
			//if parallel function, also print wrapper number
			if (type == pthreadcall) {
				Out << "\t*(" << AccelName << "_ARG" << ArgIdx << "+LEGUP_ADDR_OFFSET) = "; 
			} else if (type == ompcall) {
				//addressSize is divided by 4 since ARG pointer is an integer type
				Out << "\t*(" << AccelName << "_ARG" << ArgIdx << "+i*" << AddressSize/4 << ") = "; 
			} else {
				Out << "\t*" << AccelName << "_ARG" << ArgIdx << " = "; 
			}
	
			//get the type of this argument
			ITy = i->getType();
			//if integer type
			if (ITy->isIntegerTy()) {
				//if 64 bits
				if (ITy->isIntegerTy(64)) {
					Out << "(volatile long long) ";
				} else {
					Out << "(volatile int) ";
				}
			}
			else if (ITy->isFloatTy()) {
				Out << "(volatile float) ";
			}
			else if (ITy->isDoubleTy()) {
				Out << "(volatile double) ";
			}
			//otherwise, just use int
			else {
				Out << "(volatile int) ";
			}

			//get name of argument
			argName = i->getName();
			stripInvalidCharacters(argName);
			Out << argName << ";\n";
			//increment argument index
			ArgIdx++;	
		} 
		
		//if locks are used, insert an extra argument to pass in the threadID
		if (lockUsed) {
//			printWrapperThreadIDArgument(Out, type, AccelName, wrapperNum, ArgIdx, threadID);
			printWrapperThreadIDArgument(Out, type, AccelName, AddressSize);
		}
	
		//for OpenMP calling wrappers, insert extra arguments to pass in numThreads and threadID
		if (type == ompcall) {
			//addressSize is divided by 4 since ARG pointer is an integer type
//			Out << "\t*(" << AccelName << "_NUMTHREADS+i*" << AddressSize/4 << ") = numThreads;\n"; 

			Out << "\t*(" << AccelName << "_THREADID+i*" << AddressSize/4 << ") = i;\n"; 
		}
	}

	//if calling or seq wrapper, print STATUS pointer to start accelerator
	//if polling wrapper, print polling loop
	printWrapperSTATUSArgument(Out, type, AccelName, AddressSize);
}


void SwOnly::printWrapperReturn(raw_ostream &Out, const bool voidtype, const wrapperType type, const std::string AccelName) {

	//if sequential or pthread polling wrapper, and the function is not a void type, retrieve return value
	if ((type == seq || type == pthreadpoll) && !voidtype) {
		//sequential wrapper
		if (type == seq) {
			Out << "\t*" << AccelName << "_DATA = 1;\n";
			Out << "\treturn *" << AccelName << "_DATA;\n";
		//pthread polling wrapper
		} else {
			Out << "\treturn (char*)*DATA;\n";
		/*
			//check if this function is a pthread function
			//we need to know this since pthreads return a char*
			bool isPthread = isPthreadFunction(AccelName);

			//if pthread function
			if (isPthread) {
				Out << "\treturn (char*)*" << AccelName << wrapperNum << "_DATA;\n";
			//else regular polling function
			} else {
				Out << "\treturn *" << AccelName << wrapperNum << "_DATA;\n";
			}*/
		}
	}
	Out << "}\n\n";
}


/// printWrapper - print the C code of the legup wrapper for a function to the wrapper file.
// look in CBackend
void SwOnly::printWrapper(raw_ostream &Out, Function *F, const wrapperType type, const int AddressSize, const int numAccelerated, const std::vector<accelFcts> &AcceleratedFcts) {
	string AccelName = "";
	//pthread poll will have a null pointer
	if (type != pthreadpoll) {
		AccelName = F->getName();
		stripInvalidCharacters(AccelName);
	}
	bool voidtype = false;
	//keeps track of how many pthread functions have more than 1 thread
	static int numPthreadTypesWithMultipleThreads = 0;
	if (type == pthreadcall && numAccelerated > 1) {
		printWrapperPthreadGlobalVar(Out, AccelName, numAccelerated);
		numPthreadTypesWithMultipleThreads++;
	}
	printWrapperPrototype(Out, F, voidtype, type);
	printWrapperBody(Out, F, type, voidtype, AccelName, numAccelerated, AddressSize, numPthreadTypesWithMultipleThreads, AcceleratedFcts);
}

//print Pthread polling wrapper
//There is only one pthread polling wrapper for all pthread functions, hence the name of function, address size are not needed
//return type is also set to non-void in order to take care of both both and non-void pthread functions
//void SwOnly::printWrapperPthreadPoll(raw_ostream &Out, const int numPthreadTypesWithMultipleThreads, const std::vector<accelFcts> &AcceleratedFcts) {
//	printWrapperPrototype(Out, NULL, false, pthreadpoll);
//	printWrapperBody(Out, NULL, pthreadpoll, false, "", 1, 0, numPthreadTypesWithMultipleThreads, AcceleratedFcts);
//}

//print global variable to keep track of threads
void SwOnly::printWrapperPthreadGlobalVar(raw_ostream &Out, const std::string AccelName, const int numAccelerated) {
	//define the number of instances for this parallel HW accelerator
//	Out << "#define NUM_ACCELS_" << AccelName << " " << numAccelerated << "\n"
//	<< "unsigned legup_IDList_" << AccelName << "[NUM_ACCELS_" << AccelName << "] = {0";
//	for (int i = 1; i < numAccelerated; ++i) {
//		Out << ", " << i;
//	}
//	Out << "};\n"
    Out	<< "int legup_count_" << AccelName << " = 0;\n\n";
//	<< "int legup_accelID_" << AccelName << ";\n\n";
}

//print body of the wrapper function
void SwOnly::printWrapperBody(raw_ostream &Out, Function *F, const wrapperType type, const bool voidtype, const std::string AccelName, const int numAccelerated, const int AddressSize, const int numPthreadTypesWithMultipleThreads, const std::vector<accelFcts> &AcceleratedFcts) {

	//for pthread calling wrapper, print code to calculate accelID 
	//accelID is used to calculate the memory-mapped address offset for an instance of HW accelerator (out of a pool of pthread accelerators for a particular function)
	//then the absolute memory-mapped address needs to be stored in the pthread variable, which is read from the pthread polling wrapper to get which HW accelerator to talk to
	if (type == pthreadcall) {
		printWrapperAccelID(Out, F, type, numAccelerated, AddressSize);
		printWrapperStoreMMAddress(Out, AccelName);
	} else if (type == pthreadpoll) {
	//for pthread polling wrapper, print code to retrieve memory-mapped address of accelerator from thread variable
		printWrapperLoadMMAddress(Out);
	} else if (type == ompcall || type == omppoll) {
	//for omp call and join wrapper, print for loop to iterate over the number of accelerators
		printWrapperOMPLoop(Out, numAccelerated);
	}

	printWrapperSendArguments(Out, F, type, AccelName, AddressSize);
	
	if (type == pthreadpoll && numPthreadTypesWithMultipleThreads >= 1) {
	//	printWrapperFreeAccelCaseStatement(Out, numPthreadTypesWithMultipleThreads, AcceleratedFcts);
	}

	if (type == ompcall || type == omppoll) {
	//for omp call and join wrapper, close the loop
		Out << "\t}\n";
	}

	printWrapperReturn(Out, voidtype, type, AccelName);
}

//iterate through each pthread call function
//check if there are more than 1 thread
//if so, add an if statement to check if the STATUS pointer points to the STATUS pointer for that function
//then call freeAccel with the variables for that function
void SwOnly::printWrapperFreeAccelCaseStatement(raw_ostream &Out, const int numPthreadTypesWithMultipleThreads, const std::vector<accelFcts> &AcceleratedFcts) {
	wrapperType type;
	std::string AccelName;
	int numAccelerated;
	int AddressSize;
	if (numPthreadTypesWithMultipleThreads > 1) {
//		Out << "\tif (
		Out << "\tswitch ((int)STATUS) {\n";
		for (std::vector<accelFcts>::const_iterator I = AcceleratedFcts.begin(), E = AcceleratedFcts.end(); I != E; ++I) {
			type = (*I).type;	
			numAccelerated = (*I).numAccelerated;
			AddressSize = (*I).addressSize;
			if (type == pthreadcall && numAccelerated > 1) {
				AccelName = (*I).fct->getName().str();
				Out << "\t\tcase (int)" << AccelName << "_STATUS:\n"
			//	<< "\t\t\tfreeAccel(&legup_count_" << AccelName << ", legup_accelID_" << AccelName << ", legup_IDList_" << AccelName << ");\n"
				<< "\t\t\tfreeAccel(&legup_count_" << AccelName << ", ((int)(STATUS-" << AccelName << "_STATUS)/" << AddressSize/4 << "), legup_IDList_" << AccelName << ");\n"
				<< "\t\t\tbreak;\n";
			}
		}
		Out << "\t}\n";
	} else if (numPthreadTypesWithMultipleThreads == 1) {
		//errs () << "One Pthread Type with Multiple pthread\n\n";
		for (std::vector<accelFcts>::const_iterator I = AcceleratedFcts.begin(), E = AcceleratedFcts.end(); I != E; ++I) {
			type = (*I).type;	
			numAccelerated = (*I).numAccelerated;
			AddressSize = (*I).addressSize;
			if (type == pthreadcall && numAccelerated > 1) {
				AccelName = (*I).fct->getName().str();
				Out << "\tfreeAccel(&legup_count_" << AccelName << ", ((int)(STATUS-" << AccelName << "_STATUS)/" << AddressSize/4 << "), legup_IDList_" << AccelName << ");\n";
				//Out << "\tfreeAccel(&legup_count_" << AccelName << ", legup_accelID_" << AccelName << ", legup_IDList_" << AccelName << ");\n";
			}
			break;
		}
	}
}

/*
void SwOnly::printWrapperFreeAccel(raw_ostream &Out, const std::string AccelName) {
	Out << "\t\tfreeAccel(&count_" << AccelName << ", accelID_" << AccelName << ", IDList_" << AccelName << ");
}*/

void SwOnly::printWrapperOMPLoop(raw_ostream &Out, const int numAccelerated) {

	Out << "\tint i;\n"
	<< "\tfor (i=0; i<" << numAccelerated << "; i++) {\n";
}


//print pointers to get the address of STATUS and DATA 
void SwOnly::printWrapperLoadMMAddress(raw_ostream &Out) {

	Out << "\t//get the address of STATUS and DATA\n"
	<< "\tvolatile int * STATUS = (volatile int *)(threadVar);\n"
	<< "\tvolatile int * DATA = STATUS - 2;\n";
}

//print code to store the memory-mapped address for this instance of HW accelerator in the pthread variable
void SwOnly::printWrapperStoreMMAddress(raw_ostream &Out, const std::string AccelName) {
	Out << "\t//store the memory-mapped address for this instance of HW accelerator in the pthread variable\n"
	<< "\t*threadVar = (int)(" << AccelName << "_STATUS+LEGUP_ADDR_OFFSET);\n\n";
}

//print code to get the AccelID for this instance of HW accelerator
void SwOnly::printWrapperAccelID(raw_ostream &Out, Function *F, const wrapperType type, const int numAccelerated, const int AddressSize) {

	std::string AccelName = F->getName().str();
	if (numAccelerated > 1) {
		//if PCIe flow, need to use a semaphor
		if (LEGUP_CONFIG->isPCIeFlow()) {
			//define the number of instances for this parallel HW accelerator
			Out << "\t#define NUM_ACCELS_" << getWrapperName(F, type) << " " << numAccelerated << "\n\n";
			Out << "\tstatic val_sem_t sema = {PTHREAD_MUTEX_INITIALIZER, PTHREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER, NUM_ACCELS_" << getWrapperName(F, type) << ", 0};\n";

			//make a list of IDs, with a unique number for each instance of HW accelerator
			Out << "\tstatic unsigned IDList[NUM_ACCELS_" << getWrapperName(F, type) << "] = {0";
			for (int i = 1; i < numAccelerated; ++i) {
				Out << ", " << i;
			}
			Out << "};\n"
			<< "\t//get the accelID for this instance of HW accelerator\n"
			<< "\tunsigned accelID = getAccel(&sema, IDList);\n"
			<< "\t//get the address offset for this instance of HW accelerator\n"
			<< "\tint LEGUP_ADDR_OFFSET = accelID * " << AddressSize << ";\n\n";
		} else {
			//Out << "\tstatic int count = 0;\n"
			Out << "\t//get the accelID for this instance of HW accelerator\n"
			//<< "\tint legup_accelID_" << AccelName << " = getAccel(&legup_count_" << AccelName << ", legup_IDList_" << AccelName << ");\n"
			<< "\tint legup_accelID_" << AccelName << " = legup_count_" << AccelName << "++;\n"
			<< "\t//get the address offset for this instance of HW accelerator\n"
			<< "\tint LEGUP_ADDR_OFFSET = legup_accelID_" << AccelName << " * " << AddressSize/4 << ";\n"; 	
			//AddressSize is divided by 4 since it will be added to an integer pointer
			//When doing pointer arithematic to an integer pointer, adding 1 will offset by 4
		}
	} else {
		Out << "\tint LEGUP_ADDR_OFFSET = 0;\n\n";
	}
}

/// printWrapper - print the C code of the legup wrapper for a function to
// the wrapper file.
// look in CBackend
void SwOnly::printWrapperPCIe(raw_ostream &Out, Function *F, const unsigned long long startAddress, const wrapperType type, const int wrapperNum, const int numAccelerated, const int AddressSize) {
	string AccelName = F->getName();
	stripInvalidCharacters(AccelName);
	bool voidtype = false;

	printWrapperPrototype(Out, F, voidtype, type);
	
	// 4 bytes for status
	int numOfBytesForArgs = 0;
	Type * ITy;
	//for each argument in this function
	for (Function::arg_iterator i = F->arg_begin(), e = F->arg_end(); i != e; ++i) {
		//get the type of this argument
		ITy = i->getType();
		//if integer type
		if (ITy->isIntegerTy()) {
			//if 64 bits
			if (ITy->isIntegerTy(64)) {
				numOfBytesForArgs += 8;
			} else {
				numOfBytesForArgs += 4;
			}
		}
		else if (ITy->isDoubleTy()) {
			numOfBytesForArgs += 8;
		} else {
			numOfBytesForArgs += 4;
		}
	}

	Out << "\tchar LEGUP_BUF[" << numOfBytesForArgs << "] = {};\n"
		<< "\tint LEGUP_STATUS = 1;\n";

	const Type* rT = F->getReturnType();
	bool isVoidReturn = false;
	//print return type
	if (type == pthreadcall || type == ompcall) {
		//if calling function, it's always void
		isVoidReturn = true;
	} else {
		if (rT->getTypeID() == Type::VoidTyID) {
			isVoidReturn = true;
		} else {
			Out << "\t" << printType(rT, false) << "LEGUP_RET_VAL;\n";
			isVoidReturn = false;
		}
	}

	Out << "\n";

	printWrapperAccelID(Out, F, type, numAccelerated, AddressSize);
/*
	if (numAccelerated > 1) {
		Out << "\t#define NUM_ACCELS_" << getWrapperName(F, type) << " " << numAccelerated << "\n\n";

		Out << "\tstatic val_sem_t sema = {PTHREAD_MUTEX_INITIALIZER, PTHREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER, NUM_ACCELS_" << getWrapperName(F, type) << ", 0};\n";
		Out << "\tstatic unsigned IDList[NUM_ACCELS_" << getWrapperName(F, type) << "] = {0";
	
		for (int i = 1; i < numAccelerated; ++i)
			Out << ", " << i;

		Out << "};\n";

		Out << "\tunsigned accelID = getAccel(&sema, IDList);\n";
		Out << "\tint LEGUP_ADDR_OFFSET  = accelID * " << AddressSize << ";\n\n";
	} else {
		Out << "\tint LEGUP_ADDR_OFFSET  = 0;\n\n";
	}*/


	// 4 bytes for status
	int byteNum = 0, byteOffset = 0;
	std::string argName;

	//for each argument in this function
	for (Function::arg_iterator i = F->arg_begin(), e = F->arg_end(); i != e; ++i) {
		Out << "\t*";

		//get the type of this argument
		ITy = i->getType();
		//if integer type
		if (ITy->isIntegerTy()) {
			//if 64 bits
			if (ITy->isIntegerTy(64)) {
				Out << "(long long *) ";
				byteOffset = 8;
			} else {
				Out << "(int *) ";
				byteOffset = 4;
			}
		}
		else if (ITy->isFloatTy()) {
			Out << "(float *) ";
			byteOffset = 4;
		}
		else if (ITy->isDoubleTy()) {
			Out << "(double *) ";
			byteOffset = 8;
		}
		else {
			Out << "(int *) ";
			byteOffset = 4;
		}
		//get name of argument
		argName = i->getName();
		stripInvalidCharacters(argName);
		Out << "(LEGUP_BUF + " << byteNum << ") = ";
		if (ITy->isPointerTy()) {
			Out << "(int)";
		}
		Out << argName << ";\n";
		byteNum += byteOffset;
	}
	Out << "\n";

	//Auto-generate debug simulation output
	Out << "#ifdef DEBUG_SIM\n";
	Out << "\tprintf(\"" << AccelName << " %d";

	for (Function::arg_iterator i = F->arg_begin(), e = F->arg_end(); i != e; ++i) {
		Out << " 0x%08x";
		ITy = i->getType();
		// 64-bit values will be split into two arguments
		if (ITy->isIntegerTy(64) || ITy ->isDoubleTy()) {
			Out << " 0x%08x";
		}
	}

	if (numAccelerated == 1) {
		Out << "\\n\", 0";
	} else {
		Out << "\\n\", accelID";
	}

	for (Function::arg_iterator i = F->arg_begin(), e = F->arg_end(); i != e; ++i) {
		ITy = i->getType();
		if (ITy->isIntegerTy()) {
			Out << ", (int)" << i->getName();
			if (ITy->isIntegerTy(64)) {
				Out << ", (int)(" << i->getName() << " >> 32)";
			}
		} else if (ITy->isFloatTy()) {
			Out << ", *(int *)&" << i->getName();
		} else if (ITy->isDoubleTy()) {
			Out << ", *(int *)&" << i->getName();
			Out << ", *((int *)&" << i->getName() << " + 1)";
		} else {
			Out << ", (int)" << i->getName();
		}
	}

	Out << ");\n";

	Out << "#endif\n";

	if (F->arg_size() != 0) {
		Out << "\tpci_write_direct(LEGUP_BUF, sizeof(LEGUP_BUF), (int)("
			<< AccelName << "_ARG1) + LEGUP_ADDR_OFFSET);\n";
	}

	Out << "\tpci_write_direct(&LEGUP_STATUS, sizeof(LEGUP_STATUS), (int)("
		<< AccelName << "_STATUS) + LEGUP_ADDR_OFFSET);\n"
		<< "\tdo {\n"
		<< "\t\tpci_read_direct(&LEGUP_STATUS, sizeof(LEGUP_STATUS), (int)("
		<< AccelName << "_STATUS) + LEGUP_ADDR_OFFSET);\n";
	
	Out << "\t} while (LEGUP_STATUS == 0);\n"
		<< "\n";

	if (!isVoidReturn) {
		Out << "\tpci_read_direct(&LEGUP_RET_VAL, sizeof(LEGUP_RET_VAL), (int)("
			<< AccelName << "_DATA) + LEGUP_ADDR_OFFSET);\n";
	}

	if (numAccelerated > 1) {
		Out << "\tfreeAccel(&sema, accelID, IDList);\n\n";
	}

	Out << "#ifdef DEBUG_SIM\n";
	Out << "\tprintf(\"poll_complete %d\\n\", ";
	if (numAccelerated == 1) {
		Out << 0;
	} else {
		Out << "accelID";
	}
	Out << ");\n";

	Out << "#endif\n";

	if (!isVoidReturn) {
		Out << "\treturn LEGUP_RET_VAL;\n";
	}

	Out << "}\n\n";
}


const char * SwOnly::printIntType (const Type * Ty)
{
	int NumBits;
	const char * type;
	const IntegerType *ITy = dyn_cast<IntegerType>(Ty);
	NumBits = ITy->getBitWidth();
	if (NumBits == 1) 
	  	type = "bool ";
	else if (NumBits <= 8)
		type = "char ";
	else if (NumBits <= 16)
		type = "short ";
	else if (NumBits <= 32)
		type = "int ";
	else if (NumBits <= 64)
		type = "long long ";
	else {
		assert(0 && "Unsupported Integer type!\n");
	}
	return type;	
}


const char * SwOnly::printFloatType (const Type * Ty)
{
	const char * type;
	if (Ty->isFloatTy()) 
		type = "float ";
	else if (Ty->isDoubleTy())
		type = "double ";
	else {
		assert(0 && "Unsupported Floating point type!\n");
	}
	return type;	
}

/*
bool SwOnly::isPthreadFunction (std::string AccelName, const std::map<Value*, std::string> &pthreadFcts) {
	for (std::map<Value*, string>::const_iterator iter=pthreadFcts.begin(); iter != pthreadFcts.end(); iter++) {
		if (iter->second == AccelName) {
			return true;
		}
	}	
	return false;
}
*/

void SwOnly::printDATApointer(raw_ostream &Out, const std::string AccelName, const Type *rT, unsigned long long &CurAddr) {
	//print only if not a void return type
	if (rT->getTypeID() != Type::VoidTyID){
/*		if (type == pthreadcall) { //if this is a pthread calling wrapper
			Out << "#define " << AccelName << "_DATA\t" << "(volatile";
		} else {*/
			Out << "#define " << AccelName << "_DATA\t" << "(volatile";
//		}
		if (rT->getTypeID() == Type::IntegerTyID) {
			if (cast<IntegerType>(rT)->getBitWidth() == 64) {
				Out << " long long * ) ";
			}
			else {
				Out << " int * ) ";
			}
		} else {
				Out << " int * ) ";
		}
		Out << "0x";   
		Out.write_hex(CurAddr);
		Out << "\n";	
	}

	//increment for next pointer
	//incrememt by 8 since it can be a long long pointer
	CurAddr = CurAddr + 8;
}

void SwOnly::printSTATUSpointer(raw_ostream &Out, const std::string AccelName, unsigned long long &CurAddr) {
/*	if (type == call) { //if this is a parallel function
		Out << "#define " << AccelName << accelNum << "_STATUS\t" << "(volatile int * ) 0x";
	} else {*/
		Out << "#define " << AccelName << "_STATUS\t" << "(volatile int * ) 0x";
//	}
	Out.write_hex(CurAddr);
	Out << "\n";
	
	//increment for next pointer
	CurAddr = CurAddr + 4;
}

void SwOnly::printARGpointers(raw_ostream &Out, const std::string AccelName, const wrapperType type, Function *F, unsigned long long &CurAddr) {

	int ArgNum = 1;
	bool arg64bit = false;
	//for every argument into the function, print a pointer
	for (Function::arg_iterator it = F->arg_begin(), e = F->arg_end(); it != e; ++it) {
		//if this is a parallel function
/*		if (type == call) { 
			//need to print out accelNum since there can be multiple instances of the same function
			Out << "#define " << AccelName << accelNum << "_ARG" << ArgNum << "\t" << "(volatile ";
		} else {*/
			Out << "#define " << AccelName << "_ARG" << ArgNum << "\t" << "(volatile ";	
//		}
		//if integer type
		if (const IntegerType *ITy = dyn_cast<IntegerType>(it->getType())) {
			//if 64 bits 
			if (ITy->getBitWidth() == 64) {
				Out << "long long * ) 0x";
				arg64bit = true;
			}
			//if 32 bits
			else {
				Out << "int * ) 0x";
				arg64bit = false;
			}
		}
		else if (it->getType()->isFloatTy()) {
			Out << "float * ) 0x";
			arg64bit = false;
		}
		else if (it->getType()->isDoubleTy()) {
			Out << "double * ) 0x";
			arg64bit = true;
		}
		//otherwise just use int
		else {
			Out << "int * ) 0x";
			arg64bit = false;
		}	
		Out.write_hex(CurAddr);
		//incrememt address for next pointer
		if (arg64bit) {
			CurAddr = CurAddr + 8;
		}
		else {
			CurAddr = CurAddr + 4;			
		}
		ArgNum++;
		Out << "\n";
	}

	//if locks are used, insert an extra argument to pass in the threadID
	//if (lockUsed && (type == pthreadcall || type == ompcall)) {
	if (lockUsed && type == pthreadcall) {
/*		if (type == call) { //if this is a parallel function
			Out << "#define " << AccelName << accelNum << "_ARG" << ArgNum << "\t" << "(volatile int * ) 0x";
		} else {*/
			Out << "#define " << AccelName << "_THREADID\t" << "(volatile int * ) 0x";	
//		}
		Out.write_hex(CurAddr);
		CurAddr = CurAddr + 4;			
		ArgNum++;
		Out << "\n";
	}
	//for openMP threads, insert extra arguments to pass in the numThreads and threadID
	if (type == ompcall) {
//		Out << "#define " << AccelName << "_NUMTHREADS\t" << "(volatile int * ) 0x";	
//		Out.write_hex(CurAddr);
//		CurAddr = CurAddr + 4;			
//		ArgNum++;
//		Out << "\n";
		Out << "#define " << AccelName << "_THREADID\t" << "(volatile int * ) 0x";	
		Out.write_hex(CurAddr);
		CurAddr = CurAddr + 4;			
		ArgNum++;
		Out << "\n";
	    if (lockUsed) {
            Out << "#define " << AccelName << "_THREADMUTEXID\t" << "(volatile int * ) 0x";	
            Out.write_hex(CurAddr);
            CurAddr = CurAddr + 4;			
            ArgNum++;
            Out << "\n";
        }
	}
	Out << "\n";
}

//Prints memory mapped addresses which will be used to send arguments, poll status, and retrieve return value
unsigned long long SwOnly::printMemAddr(raw_ostream &Out, Function *F, unsigned long long CurAddr, wrapperType type, unsigned countOfAccelType) {

	unsigned long long oldCurAddr = CurAddr;

	//get accel name	
	std::string AccelName = F->getName();
	stripInvalidCharacters(AccelName);

	//get return type
	const Type* rT = F->getReturnType();

	//print DATA, STATUS, and ARG pointers
	printDATApointer(Out, AccelName, rT, CurAddr);
	printSTATUSpointer(Out, AccelName, CurAddr);
	printARGpointers(Out, AccelName, type, F, CurAddr);

	//include pthread header for pthread wrappers
	if (type == pthreadcall && pthreadHeaderIncluded == false) {
		Out << "#include <pthread.h>\n";
		Out << "#include <legup_parallel.h>\n\n";
		pthreadHeaderIncluded = true;
	}

	return oldCurAddr + (CurAddr - oldCurAddr) * countOfAccelType;
}

void SwOnly::printCacheHWtcl() {

	formatted_raw_ostream *HW_tcl;
	std::string fileName = "data_cache_hw.tcl";
	HW_tcl = GetOutputStream(fileName);
	assert(HW_tcl);
	raw_ostream &tcl = *HW_tcl;

	int sdramSize;
	if (FPGABoard == "DE4") {
		sdramSize = 256;
	} else {
		sdramSize = 32;
	}
	int byteenableSize = sdramSize/8;

	tcl << "package require -exact sopc 11.0\n\n"
	<< "set_module_property NAME data_cache\n"
	<< "set_module_property VERSION 1.0\n"
	<< "set_module_property INTERNAL false\n"
	<< "set_module_property OPAQUE_ADDRESS_MAP true\n"
	<< "set_module_property DISPLAY_NAME data_cache\n"
	<< "set_module_property TOP_LEVEL_HDL_FILE data_cache.v\n"
	<< "set_module_property TOP_LEVEL_HDL_MODULE data_cache\n"
	<< "set_module_property INSTANTIATE_IN_SYSTEM_MODULE true\n"
	<< "set_module_property EDITABLE true\n"
	<< "set_module_property ANALYZE_HDL TRUE\n\n"

	<< "add_file data_cache.v {SYNTHESIS SIMULATION}\n\n"
	
	<< "add_interface clockreset clock end\n"
	<< "set_interface_property clockreset clockRate 0\n"
	<< "set_interface_property clockreset ENABLED true\n"
	<< "add_interface_port clockreset csi_clockreset_clk clk Input 1\n"
	<< "add_interface_port clockreset csi_clockreset_reset_n reset_n Input 1\n\n";
 
	if (multiportedCache && dcacheType == "MP" && FPGABoard == "DE4") {
		tcl << "add_interface clockreset2X clock end\n"
		<< "set_interface_property clockreset2X clockRate 0\n"
		<< "set_interface_property clockreset2X ENABLED true\n"
		<< "add_interface_port clockreset2X csi_clockreset2X_clk clk Input 1\n\n"

		<< "add_interface clockreset2X_reset reset end\n"
		<< "set_interface_property clockreset2X_reset associatedClock clockreset\n"
		<< "set_interface_property clockreset2X_reset synchronousEdges DEASSERT\n"
		<< "set_interface_property clockreset2X_reset ENABLED true\n"
		<< "add_interface_port clockreset2X_reset csi_clockreset2X_reset_n reset_n Input 1\n\n";
	}

	tcl << "add_interface PROC avalon_streaming start\n"
	<< "set_interface_property PROC associatedClock clockreset\n"
	<< "set_interface_property PROC dataBitsPerSymbol 8\n"
	<< "set_interface_property PROC errorDescriptor \"\"\n"
	<< "set_interface_property PROC maxChannel 0\n"
	<< "set_interface_property PROC readyLatency 0\n"
	<< "set_interface_property PROC ENABLED true\n"
	<< "add_interface_port PROC aso_PROC_data data Output 8\n\n";

	for (int i=0; i<dcacheports; i++) {
		tcl << "add_interface CACHE" << i << " avalon end\n"
		<< "set_interface_property CACHE" << i << " addressAlignment DYNAMIC\n"
		<< "set_interface_property CACHE" << i << " addressUnits WORDS\n"
		<< "set_interface_property CACHE" << i << " associatedClock clockreset\n"
		<< "set_interface_property CACHE" << i << " burstOnBurstBoundariesOnly false\n"
		<< "set_interface_property CACHE" << i << " explicitAddressSpan 0\n"
		<< "set_interface_property CACHE" << i << " holdTime 0\n"
		<< "set_interface_property CACHE" << i << " isMemoryDevice false\n"
		<< "set_interface_property CACHE" << i << " isNonVolatileStorage false\n"
		<< "set_interface_property CACHE" << i << " linewrapBursts false\n"
		<< "set_interface_property CACHE" << i << " maximumPendingReadTransactions 0\n"
		<< "set_interface_property CACHE" << i << " printableDevice false\n"
		<< "set_interface_property CACHE" << i << " readLatency 0\n"
		<< "set_interface_property CACHE" << i << " readWaitTime 1\n"
		<< "set_interface_property CACHE" << i << " setupTime 0\n"
		<< "set_interface_property CACHE" << i << " timingUnits Cycles\n"
		<< "set_interface_property CACHE" << i << " writeWaitTime 0\n"
		<< "set_interface_property CACHE" << i << " ENABLED true\n\n"

		<< "add_interface_port CACHE" << i << " avs_CACHE" << i << "_begintransfer begintransfer Input 1\n"
		<< "add_interface_port CACHE" << i << " avs_CACHE" << i << "_read read Input 1\n"
		<< "add_interface_port CACHE" << i << " avs_CACHE" << i << "_write write Input 1\n"
		<< "add_interface_port CACHE" << i << " avs_CACHE" << i << "_writedata writedata Input 128\n"
		<< "add_interface_port CACHE" << i << " avs_CACHE" << i << "_readdata readdata Output 128\n"
		<< "add_interface_port CACHE" << i << " avs_CACHE" << i << "_waitrequest waitrequest Output 1\n\n"

		<< "add_interface dataMaster" << i << " avalon start\n"
		<< "set_interface_property dataMaster" << i << " addressUnits SYMBOLS\n"
		<< "set_interface_property dataMaster" << i << " associatedClock clockreset\n"
		<< "set_interface_property dataMaster" << i << " burstOnBurstBoundariesOnly false\n"
		<< "set_interface_property dataMaster" << i << " doStreamReads false\n"
		<< "set_interface_property dataMaster" << i << " doStreamWrites false\n"
		<< "set_interface_property dataMaster" << i << " linewrapBursts false\n"
		<< "set_interface_property dataMaster" << i << " readLatency 0\n"
		<< "set_interface_property dataMaster" << i << " ENABLED true\n\n";


		if(LEGUP_CONFIG->getParameterInt("USE_QSYS")) {
				tcl << "add_interface_port dataMaster" << i << " avm_dm" << i << "_read read Output 1\n"
				<< "add_interface_port dataMaster" << i << " avm_dm" << i << "_write write Output 1\n"
				<< "add_interface_port dataMaster" << i << " avm_dm" << i << "_address address Output 32\n"

				<< "add_interface_port dataMaster" << i << " avm_dm" << i << "_writedata writedata Output " << sdramSize <<"\n"
				<< "add_interface_port dataMaster" << i << " avm_dm" << i << "_byteenable byteenable Output " << byteenableSize << "\n"
				<< "add_interface_port dataMaster" << i << " avm_dm" << i << "_readdata readdata Input " << sdramSize <<"\n"
			
				<< "add_interface_port dataMaster" << i << " avm_dm" << i << "_waitrequest waitrequest Input 1\n"
				<< "add_interface_port dataMaster" << i << " avm_dm" << i << "_readdatavalid readdatavalid Input 1\n\n";
		} else {
			tcl << "add_interface_port dataMaster" << i << " avm_dataMaster" << i << "_read read Output 1\n"
			<< "add_interface_port dataMaster" << i << " avm_dataMaster" << i << "_write write Output 1\n"
			<< "add_interface_port dataMaster" << i << " avm_dataMaster" << i << "_address address Output 32\n"

			<< "add_interface_port dataMaster" << i << " avm_dataMaster" << i << "_writedata writedata Output " << sdramSize <<"\n"
			<< "add_interface_port dataMaster" << i << " avm_dataMaster" << i << "_byteenable byteenable Output " << byteenableSize << "\n"
			<< "add_interface_port dataMaster" << i << " avm_dataMaster" << i << "_readdata readdata Input " << sdramSize <<"\n"
			
			<< "add_interface_port dataMaster" << i << " avm_dataMaster" << i << "_beginbursttransfer beginbursttransfer Output 1\n"
			<< "add_interface_port dataMaster" << i << " avm_dataMaster" << i << "_burstcount burstcount Output 6\n"
			<< "add_interface_port dataMaster" << i << " avm_dataMaster" << i << "_waitrequest waitrequest Input 1\n"
			<< "add_interface_port dataMaster" << i << " avm_dataMaster" << i << "_readdatavalid readdatavalid Input 1\n\n";
		}
	}

	delete HW_tcl;
}

//print _hw.tcl file to specify interface specs for the accelerator (needed for SOPC builder)
void SwOnly::printHWtcl(Function * F, wrapperType type, int AddrOffset) {

	formatted_raw_ostream *HW_tcl;
	string AccelName = F->getName();
	stripInvalidCharacters(AccelName);
	HW_tcl = GetOutputStream(AccelName.append("_hw.tcl"));
	assert(HW_tcl);
	raw_ostream &tcl = *HW_tcl;
	AccelName = F->getName();
	stripInvalidCharacters(AccelName);
	string AccelFilename = std::getenv ("LEGUP_ACCELERATOR_FILENAME");
	//getting the bitwidth of address bus of accelerator
	//bitwidth = (log (#_of_32bit_arguments)+ 3) / log(2)
	//one 64bit argument = two 32bit arguments
	int AddressBusWidth = ceil(log (AddrOffset/4) / log (2));
	tcl << "package require -exact sopc 11.0\n\n"

	<< "set_module_property NAME " << AccelName <<"\n"
	<< "set_module_property VERSION 1.0\n"
	<< "set_module_property INTERNAL false\n"
	<< "set_module_property GROUP \"\"\n"
	<< "set_module_property DISPLAY_NAME " << AccelName <<"\n"
	<< "set_module_property TOP_LEVEL_HDL_FILE " << AccelFilename << ".v\n" 
	<< "set_module_property TOP_LEVEL_HDL_MODULE " << AccelName << "_top\n"
	<< "set_module_property INSTANTIATE_IN_SYSTEM_MODULE true\n"
	<< "set_module_property EDITABLE true\n"
	<< "set_module_property ANALYZE_HDL TRUE\n\n"
	<< "add_file " << AccelFilename << ".v {SYNTHESIS SIMULATION}\n\n"
	<< "add_interface clockreset clock end\n"
	<< "set_interface_property clockreset ENABLED true\n"
	<< "add_interface_port clockreset csi_clockreset_clk clk Input 1\n"
	<< "add_interface_port clockreset csi_clockreset_reset reset Input 1\n\n"

	<< "add_interface s1 avalon end\n"
	<< "set_interface_property s1 addressAlignment DYNAMIC\n"
	<< "set_interface_property s1 associatedClock clockreset\n"
	<< "set_interface_property s1 burstOnBurstBoundariesOnly false\n"
	<< "set_interface_property s1 explicitAddressSpan 0\n"
	<< "set_interface_property s1 holdTime 0\n"
	<< "set_interface_property s1 isMemoryDevice false\n"
	<< "set_interface_property s1 isNonVolatileStorage false\n"
	<< "set_interface_property s1 linewrapBursts false\n"
	<< "set_interface_property s1 maximumPendingReadTransactions 0\n"
	<< "set_interface_property s1 printableDevice false\n"
	<< "set_interface_property s1 readLatency 0\n"
	<< "set_interface_property s1 readWaitTime 1\n"
	<< "set_interface_property s1 setupTime 0\n"
	<< "set_interface_property s1 timingUnits Cycles\n"
	<< "set_interface_property s1 writeWaitTime 0\n\n"

	<< "set_interface_property s1 ASSOCIATED_CLOCK clockreset\n"
	<< "set_interface_property s1 ENABLED true\n\n"

	<< "add_interface_port s1 avs_s1_address address Input " << AddressBusWidth << "\n"
	<< "add_interface_port s1 avs_s1_read read Input 1\n"
	<< "add_interface_port s1 avs_s1_write write Input 1\n"
	<< "add_interface_port s1 avs_s1_writedata writedata Input 32\n" //always 32 bits since its a 32 bit processor
	<< "add_interface_port s1 avs_s1_readdata readdata Output 32\n" 

 	<< "add_interface ACCEL avalon start\n"
	<< "set_interface_property ACCEL associatedClock clockreset\n"
	<< "set_interface_property ACCEL burstOnBurstBoundariesOnly false\n"
	<< "set_interface_property ACCEL doStreamReads false\n"
	<< "set_interface_property ACCEL doStreamWrites false\n"
	<< "set_interface_property ACCEL linewrapBursts false\n\n"

	<< "set_interface_property ACCEL ASSOCIATED_CLOCK clockreset\n"
	<< "set_interface_property ACCEL ENABLED true\n"

	<< "add_interface_port ACCEL avm_ACCEL_readdata readdata Input 128\n"
	<< "add_interface_port ACCEL avm_ACCEL_waitrequest waitrequest Input 1\n"
	<< "add_interface_port ACCEL avm_ACCEL_address address Output 32\n"
	<< "add_interface_port ACCEL avm_ACCEL_writedata writedata Output 128\n"
	<< "add_interface_port ACCEL avm_ACCEL_write write Output 1\n"
	<< "add_interface_port ACCEL avm_ACCEL_read read Output 1\n\n";

//	if ((ompUsed && type == ompcall) || lockUsed || barrierUsed) {
//	if (ompUsed || lockUsed || barrierUsed) {
//	if (lockUsed || barrierUsed) {
/*	if (barrierUsed) {
	 	tcl << "add_interface API avalon start\n"
		<< "set_interface_property API associatedClock clockreset\n"
		<< "set_interface_property API burstOnBurstBoundariesOnly false\n"
		<< "set_interface_property API doStreamReads false\n"
		<< "set_interface_property API doStreamWrites false\n"
		<< "set_interface_property API linewrapBursts false\n\n"

		<< "set_interface_property API ASSOCIATED_CLOCK clockreset\n"
		<< "set_interface_property API ENABLED true\n"

		<< "add_interface_port API avm_API_readdata readdata Input 32\n"
		<< "add_interface_port API avm_API_waitrequest waitrequest Input 1\n"
		<< "add_interface_port API avm_API_address address Output 32\n"
		<< "add_interface_port API avm_API_writedata writedata Output 32\n"
		<< "add_interface_port API avm_API_write write Output 1\n"
		<< "add_interface_port API avm_API_read read Output 1\n\n";
	}*/

	delete HW_tcl;
}

//initial part of sopc file, removing the connection only needs to be done once
void SwOnly::printSopcFileInitial(raw_ostream &sopc) {

	//need to first remove data cache and add it again in order to add accelerator port to cache
	sopc << "remove_module data_cache_0\n"
	<< "add_module data_cache data_cache_0\n"
	<< "set_avalon_base_address data_cache_0.CACHE1 \"0x01000000\"\n";

	//add the clock connection
	if (FPGABoard == "DE4") {
		sopc << "add_connection ddr2.sysclk data_cache_0.clockreset\n";
	} else {
		sopc << "add_connection clk.clk data_cache_0.clockreset\n";
	}

	if (multiportedCache && dcacheType == "MP" && FPGABoard == "DE4") {
		sopc << "add_connection ddr2.auxfull data_cache_0.clockreset2X\n";
	}

	//connect the other ports from data cache
	sopc << "add_connection data_cache_0.PROC tiger_top_0.PROC\n";

	for (int i=0; i<dcacheports; i++) {
		sopc << "add_connection data_cache_0.dataMaster"<< i << "  pipeline_bridge_MEMORY.s1\n";
	}

    //setting this to false for testing
    parallelAccelUsed = false;
    //if parallel accelerators are used, add pipeline bridge to improve Fmax
    if (parallelAccelUsed) {
        for (int i=0; i<dcacheports-1; i++) {
            
            //add pipeline bridge between accelerators and cache to improve Fmax	
            sopc << "add_module altera_avalon_pipeline_bridge pipeline_bridge_" << i+1 << "\n"; 
            if (FPGABoard == "DE4") {
            sopc << "add_connection ddr2.sysclk pipeline_bridge_" << i+1 << ".clk\n";
            } else {
            sopc << "add_connection clk.clk pipeline_bridge_" << i+1 << ".clk\n";
            }
            //the bridge is pipelined from master-to-slave 
            sopc << "set_parameter pipeline_bridge_" << i+1 << " dataWidth 128\n";

            //pipeline downstream (from accelerator to cache)
            //sopc << "set_parameter pipeline_bridge_" << i+1 << " downstreamPipeline true\n";
            //don't pipeline downstream (from accelerator to cache)
            sopc << "set_parameter pipeline_bridge_" << i+1 << " downstreamPipeline false\n";
            
            //pipeline upstream (from cache to accelerator)
            //sopc << "set_parameter pipeline_bridge_" << i+1 << " upstreamPipeline true\n";
            //don't pipeline upstream (from cache to accelerator)
            sopc << "set_parameter pipeline_bridge_" << i+1 << " upstreamPipeline false\n";

            //pipeline waitrequest
            sopc << "set_parameter pipeline_bridge_" << i+1 << " waitrequestPipeline true\n";
            //don't pipeline waitrequest
            //sopc << "set_parameter pipeline_bridge_" << i+1 << " waitrequestPipeline false\n";

            sopc << "add_connection pipeline_bridge_" << i+1 << ".m1 data_cache_0.CACHE" << i%(dcacheports-1)+1 << "\n";
            sopc << "set_avalon_base_address pipeline_bridge_" << i+1 << ".s1 0x00\n";
        }
    }
	sopc << "add_connection tiger_top_0.CACHE data_cache_0.CACHE0\n\n";

	//add omp, lock, barrier, perf counter cores if needed
	printSopcFileAPIcores(sopc);
}

void SwOnly::printSopcFileAPIcores(raw_ostream &sopc) {
/*
	//if OpenMP is used, connect the omp_core to the processor 
	if (ompUsed) {
		if (FPGABoard == "DE4") {
			sopc << "add_module omp_core omp_core_0\n"
			<< "add_connection ddr2.sysclk omp_core_0.clockreset\n"
			<< "add_connection tiger_top_0.procMaster omp_core_0.s1\n"
			<< "set_avalon_base_address omp_core_0.s1 \"0xc5000000\"\n\n";
		} else {
			sopc << "add_module omp_core omp_core_0\n"
			<< "add_connection clk.clk omp_core_0.clockreset\n"
			<< "add_connection tiger_top_0.procMaster omp_core_0.s1\n"
			<< "set_avalon_base_address omp_core_0.s1 \"0xc5000000\"\n\n";
		}
	}*/

	//if locks are used, connect the mutex core to the processor
	if (lockUsed) {
		int mutexNum = mutexMap.size();

		unsigned long long mutexAddr = 0xc5000000;
		for (int i=0; i<mutexNum; i++) {
			sopc << "add_module mutex mutex_" << i << "\n"
//			<< "add_connection tiger_top_0.procMaster mutex_" << i << ".s1\n"
			<< "set_avalon_base_address mutex_" << i << ".s1 " << mutexAddr << "\n";
			if (FPGABoard == "DE4") {
				sopc << "add_connection ddr2.sysclk mutex_" << i << ".clockreset\n\n";
			} else {
				sopc << "add_connection clk.clk mutex_" << i << ".clockreset\n\n";
			}
			mutexAddr += 0x20;
		}
	}

	//if barriers are used, connect the mutex core to the processor
	if (barrierUsed) {
		sopc << "add_module legup_barrier legup_barrier_0\n"
		<< "add_connection tiger_top_0.procMaster legup_barrier_0.s1\n"
		<< "set_avalon_base_address legup_barrier_0.s1 \"0xc5001000\"\n";
		if (FPGABoard == "DE4") {
			sopc << "add_connection ddr2.sysclk legup_barrier_0.clockreset\n\n";
		} else {
			sopc << "add_connection clk.clk legup_barrier_0.clockreset\n\n";
		}
	}
	
    //if perf counters are used, connect the perf counter core to the processor
	if (perfCounterUsed) {
		unsigned long long perfCounterAddr = 0x01000000;
		for (int i=-1; i<numPerfCounter; i++) {
			sopc << "add_module performance_counter performance_counter_" << i+1 << "\n"
			<< "add_connection pipeline_bridge_PERIPHERALS.m1 performance_counter_" << i+1 << ".s1\n"
			<< "set_avalon_base_address performance_counter_" << i+1 << ".s1 " << perfCounterAddr << "\n";
			if (FPGABoard == "DE4") {
				sopc << "add_connection ddr2.sysclk performance_counter_" << i+1 << ".clockreset\n\n";
			} else {
				sopc << "add_connection clk.clk performance_counter_" << i+1 << ".clockreset\n\n";
			}
			perfCounterAddr += 0x8;
		}
	}
}

//prints tcl commands to generate SOPC
void SwOnly::printSopcFile(raw_ostream &sopc, Function * F, wrapperType type, unsigned long long baseAddr, int AccelCount, int AccelIndex) {
	
	//for accelerator
	std::string moduleName = F->getName();
	stripInvalidCharacters(moduleName);
	sopc << "add_module " << F->getName() << " " << moduleName << "_" << AccelCount << "\n";

	//for DE4, connect the sysclk from ddr2 controller to the clock of accelerator
	//for DE2, connect the system clock to the clock of accelerator
	if (FPGABoard == "DE4") {
		sopc << "add_connection ddr2.sysclk " << moduleName << "_" << AccelCount << ".clockreset\n";
	} else {
		sopc << "add_connection clk.clk " << moduleName << "_" << AccelCount << ".clockreset\n";
	}

	//connect accelerator to pipeline bridge
	sopc << "add_connection pipeline_bridge_PERIPHERALS.m1 " << moduleName << "_" << AccelCount << ".s1\n"
	<< "set_avalon_base_address " << moduleName << "_" << AccelCount << ".s1 \"0x"; 
	sopc.write_hex(baseAddr);
	sopc << "\"\n";

	//int cachePortNum = ((AccelCount+1)%dcacheports);
	//for now we don't share the processor port with other accelerators
	//hence -1 and the -1
	//int cachePortNum = (AccelCount%(dcacheports-1))+1;
	int cachePortNum = (AccelIndex%(dcacheports-1))+1;
    //if it is a sequential accelerator, connect directly to cache
    //otherwise connect to pipeline bridge
    //for testing
    type = seq;
    if (type == seq) {
    	//connect accelerator to data cache
    	sopc << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL data_cache_0.CACHE" << cachePortNum << "\n\n";
    } else {
    	//connect to the pipeline bridge
    	sopc << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL pipeline_bridge_" << cachePortNum << ".s1\n\n";
    }

	//errs () << "cachePortNum = " << cachePortNum << "\n";	
	

//disable the 2nd port for now, all accelerator connect to one port of the cache
//	sopc << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL data_cache_0.CACHE1\n\n";
	

	//connect accelerator to omp core
/*	if (ompUsed) {
		sopc << "add_connection " << moduleName << "_" << AccelCount << ".API omp_core_0.s1\n\n";
	}*/

	//connect accelerator to mutex core
	if (lockUsed) {

//		std::set<int> mutex;
//		findMutexes(F, mutex, mutexMap);
//        errs() << "lock used = " << lockUsed << "\n";
		std::set<Function*> funcSet;
  //      errs() << "function name = " << F->getName().str() << "\n";
		//look through the mutex function map
		for (std::map<int, std::set<Function*> >::iterator it = mutexFunctionMap.begin(); it != mutexFunctionMap.end(); it++) {
			funcSet = (*it).second;

//		    for (std::set<Function*>::iterator it2 = funcSet.begin(); it2 != funcSet.end(); it2++) {
//                errs() << "FuncSet Names = " << (*it2)->getName().str() << "\n";
  //          }

			//if you find this function in the map
			if (funcSet.find(F) != funcSet.end()) {
				//get the mutex number and connect this accelerator to the mutex
				sopc << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL mutex_" << (*it).first << ".s1\n\n";
			}
		}
/*
		for (std::set<int>::iterator it=mutex.begin(); it!=mutex.end(); it++) {
			sopc << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL mutex_" << *it << ".s1\n\n";
		}*/
//		for (int i=0; i<mutex.size(); i++) {
//			sopc << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL mutex_" << mutex[i] << ".s1\n\n";
//		}
	}

	//connect accelerator to barrier core
	if (barrierUsed) {
		//sopc << "add_connection " << moduleName << "_" << AccelCount << ".API legup_barrier_0.s1\n\n";
		sopc << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL legup_barrier_0.s1\n\n";
	}		
}

//initial part of qsys file, removing the connection only needs to be done once
void SwOnly::printQSYSFileInitial(raw_ostream &qsys) {

	//need to first remove data cache and add it again in order to add accelerator port to cache
	qsys << "remove_instance data_cache_0\n"
	<< "add_instance data_cache_0 data_cache\n";

	//add the clock connection
	if (FPGABoard == "DE4") {
		qsys << "add_connection sysclk.out_clk data_cache_0.clockreset\n";
		qsys << "add_connection clk.clk_reset data_cache_0.clockreset_reset\n";
	} else {
		qsys << "add_connection clk.clk data_cache_0.clockreset\n";
		qsys << "add_connection clk.clk_reset data_cache_0.clockreset_reset\n";
	}

	if (multiportedCache && dcacheType == "MP" && FPGABoard == "DE4") {
		qsys << "add_connection ddr2.auxfull data_cache_0.clockreset2X\n";
	}

	//connect the other ports from data cache
	qsys << "add_connection data_cache_0.PROC tiger_top_0.PROC\n";

	for (int i=0; i<dcacheports; i++) {
		qsys << "add_connection data_cache_0.dataMaster"<< i << "  sdram.s1\n";
	}


    //setting this to false for testing
    parallelAccelUsed = false;
    //if parallel accelerators are used, add pipeline bridge to improve Fmax
    if (parallelAccelUsed) {
        for (int i=0; i<dcacheports-1; i++) {
            
            //add pipeline bridge between accelerators and cache to improve Fmax	
            qsys << "add_instance pipeline_bridge_" << i+1 << "altera_avalon_pipeline_bridge\n"; 
            if (FPGABoard == "DE4") {
			qsys << "add_connection sysclk.out_clk pipeline_bridge_" << i+1 << ".clockreset\n";
			qsys << "add_connection clk.clk_reset pipeline_bridge_" << i+1 << ".clockreset_reset\n";
            } else {
			qsys << "add_connection clk.clk pipeline_bridge_" << i+1 << ".clockreset\n";
			qsys << "add_connection clk.clk_reset pipeline_bridge_" << i+1 << ".clockreset_reset\n";
            }
            //the bridge is pipelined from master-to-slave 
            qsys << "set_instance_parameter_value pipeline_bridge_" << i+1 << " dataWidth 128\n";

            //pipeline downstream (from accelerator to cache)
            qsys << "set_instance_parameter_value pipeline_bridge_" << i+1 << " downstreamPipeline true\n";
            //don't pipeline downstream (from accelerator to cache)
            //sopc << "set_parameter pipeline_bridge_" << i+1 << " downstreamPipeline false\n";
            
            //pipeline upstream (from cache to accelerator)
            qsys << "set_instance_parameter_value pipeline_bridge_" << i+1 << " upstreamPipeline true\n";
            //don't pipeline upstream (from cache to accelerator)
            //sopc << "set_parameter pipeline_bridge_" << i+1 << " upstreamPipeline false\n";

            //pipeline waitrequest
            qsys << "set_instance_parameter_value pipeline_bridge_" << i+1 << " waitrequestPipeline true\n";
            //don't pipeline waitrequest
            //sopc << "set_parameter pipeline_bridge_" << i+1 << " waitrequestPipeline false\n";

            qsys << "add_connection pipeline_bridge_" << i+1 << ".m1 data_cache_0.CACHE" << i%(dcacheports-1)+1 << "\n";
            qsys << "set_avalon_base_address pipeline_bridge_" << i+1 << ".s1 0x00\n";
			qsys << "set_connection_parameter_value data_cache_0.CACHE" << i%(dcacheports-1)+1 << "/pipeline_bridge_" << i+1 << ".s1 baseAddress \"0x00\"\n";
        }
    }

	qsys << "add_connection tiger_top_0.CACHE data_cache_0.CACHE0\n";

	//add omp, lock, barrier cores if needed
	printQSYSFileAPIcores(qsys);
}

void SwOnly::printQSYSFileAPIcores(raw_ostream &qsys) {
/*
	//if OpenMP is used, connect the omp_core to the processor 
	if (ompUsed) {
		if (FPGABoard == "DE4") {
			qsys << "add_instance omp_core_0 omp_core\n"
			<< "add_connection sysclk.out_clk omp_core_0.clockreset\n"
			<< "add_connection clk.clk_reset omp_core_0.clockreset_reset\n"
			<< "add_connection tiger_top_0.procMaster omp_core_0.s1\n"
			<< "set_connection_parameter_value tiger_top_0.procMaster/omp_core_0.s1 baseAddress \"0xc5000000\"\n";
		} else {
			qsys << "add_instance omp_core_0 omp_core\n"
			<< "add_connection clk.clk omp_core_0.clockreset\n"
			<< "add_connection clk.clk_reset omp_core_0.clockreset_reset\n"
			<< "add_connection tiger_top_0.procMaster omp_core_0.s1\n"
			<< "set_connection_parameter_value tiger_top_0.procMaster/omp_core_0.s1 baseAddress \"0xc5000000\"\n";
		}
	}*/

	//if locks are used, connect the mutex core to the processor
	if (lockUsed) {
		int mutexNum = mutexMap.size();
		for (int i=0; i<mutexNum; i++) {

			qsys << "add_instance mutex_" << i << " mutex\n";
	//		<< "add_connection tiger_top_0.procMaster mutex_0.s1\n"
		
			if (FPGABoard == "DE4") {
				qsys << "add_connection sysclk.out_clk mutex_" << i << ".clockreset\n";
				qsys << "add_connection clk.clk_reset mutex_" << i << ".clockreset_reset\n";
			} else {
				qsys << "add_connection clk.clk mutex_" << i << ".clockreset\n";
				qsys << "add_connection clk.clk_reset mutex_" << i << ".clockreset_reset\n";
			}
		}
	}

	//if barriers are used, connect the mutex core to the processor
	if (barrierUsed) {
		qsys << "add_instance legup_barrier_0 legup_barrier\n"
		<< "add_connection tiger_top_0.procMaster legup_barrier_0.s1\n"
		<< "set_connection_parameter_value tiger_top_0.procMaster/legup_barrier_0.s1 baseAddress \"0xc5000040\"\n";
		if (FPGABoard == "DE4") {
			qsys << "add_connection sysclk.out_clk legup_barrier_0.clockreset\n";
			qsys << "add_connection clk.clk_reset legup_barrier_0.clockreset_reset\n";
		} else {
			qsys << "add_connection clk.clk legup_barrier_0.clockreset\n";
			qsys << "add_connection clk.clk_reset legup_barrier_0.clockreset_reset\n";
		}
	}

   //if perf counters are used, connect the perf counter core to the processor
	if (perfCounterUsed) {
		unsigned long long perfCounterAddr = 0x01000000;
		for (int i=-1; i<numPerfCounter; i++) {
			qsys << "add_instance performance_counter_" << i+1 << "performance_counter\n"
			<< "add_connection performance_counter_" << i+1 << ".s1 pipeline_bridge_PERIPHERALS.m1\n"
			<< "set_connection_parameter_value pipeline_bridge_PERIPHERALS.m1/performance_counter_" << i+1 << ".s1 baseAddress \"" << perfCounterAddr << "\"\n";

			if (FPGABoard == "DE4") {
				qsys << "add_connection sysclk.out_clk performance_counter_" << i+1 << ".clockreset\n";
				qsys << "add_connection clk.clk_reset performance_counter_" << i+1 << ".clockreset_reset\n\n";
			} else {
				qsys << "add_connection clk.clk performance_counter_" << i+1 << ".clockreset\n";
				qsys << "add_connection clk.clk_reset performance_counter_" << i+1 << ".clockreset_reset\n\n";
			}
			perfCounterAddr += 0x8;
		}
	}
}

//prints tcl commands to generate QSYS
void SwOnly::printQSYSFile(raw_ostream &qsys, Function * F, wrapperType type, unsigned long long baseAddr, int AccelCount, int AccelIndex) {
	
	//for accelerator	
	std::string moduleName = F->getName();
	stripInvalidCharacters(moduleName);
	qsys << "add_instance " << moduleName << "_" << AccelCount << " " << F->getName() << "\n";

	//for DE4, connect the sysclk from ddr2 controller to the clock of accelerator
	//for DE2, connect the system clock to the clock of accelerator
	if (FPGABoard == "DE4") {
		qsys << "add_connection sysclk.out_clk " << moduleName << "_" << AccelCount << ".clockreset\n";
		qsys << "add_connection clk.clk_reset " << moduleName << "_" << AccelCount << ".clockreset_reset\n\n";
	} else {
		qsys << "add_connection clk.clk " << moduleName << "_" << AccelCount << ".clockreset\n";
		qsys << "add_connection clk.clk_reset " << moduleName << "_" << AccelCount << ".clockreset_reset\n\n";
	}

	//connect accelerator to pipeline bridge
	qsys << "add_connection pipeline_bridge_PERIPHERALS.m0 " << moduleName << "_" << AccelCount << ".s1\n";
	//set the base address of the connection between accel and pipeline bridge
	qsys << "set_connection_parameter_value pipeline_bridge_PERIPHERALS.m0/" << moduleName << "_" << AccelCount << ".s1 baseAddress \"0x"; 
	qsys.write_hex(baseAddr);
	qsys << "\"\n\n";

	//int cachePortNum = ((AccelCount+1)%dcacheports);
	//for now we don't share the processor port with other accelerators
	//hence -1 and the -1
	//int cachePortNum = (AccelCount%(dcacheports-1))+1;
	int cachePortNum = (AccelIndex%(dcacheports-1))+1;
    //if it is a sequential accelerator, connect directly to cache
    //otherwise connect to pipeline bridge
//    if (type == seq) {
    	//connect accelerator to data cache
		qsys << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL data_cache_0.CACHE" << cachePortNum << "\n\n";
		//set Base Address of Cache/Accel connection
		qsys << "set_connection_parameter_value " << moduleName << "_" << AccelCount << ".ACCEL/data_cache_0.CACHE" << cachePortNum << " baseAddress \"0x01000000\"\n\n";
//    } else {
    	//connect to the pipeline bridge
//    	qsys << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL pipeline_bridge_" << cachePortNum << ".s1\n\n";
//    }

	//errs () << "cachePortNum = " << cachePortNum << "\n";	
	

//disable the 2nd port for now, all accelerator connect to one port of the cache
//	qsys << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL data_cache_0.CACHE1\n\n";

	//connect accelerator to omp core
/*	if (ompUsed) {
		qsys << "add_connection " << moduleName << "_" << AccelCount << ".API omp_core_0.s1\n\n";
	}*/

	//connect accelerator to mutex core
	if (lockUsed) {

//		std::set<int> mutex;
//		findMutexes(F, mutex, mutexMap);

		unsigned long long mutexAddr = 0xc5000000;
		std::set<Function*> funcSet;
		//look through the mutex function map
		for (std::map<int, std::set<Function*> >::iterator it = mutexFunctionMap.begin(); it != mutexFunctionMap.end(); it++) {
			funcSet = (*it).second;

			//if you find this function in the map
			if (funcSet.find(F) != funcSet.end()) {
				//get the mutex number and connect this accelerator to the mutex
				qsys << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL mutex_" << (*it).first << ".s1\n\n";
//base Address
qsys << "set_connection_parameter_value " << moduleName << "_" << AccelCount << ".ACCEL/mutex_" << (*it).first << ".s1 baseAddress \"" << mutexAddr << "\"\n\n";
			}
		}
/*
		for (std::set<int>::iterator it=mutex.begin(); it!=mutex.end(); it++) {
			qsys << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL mutex_" << *it << ".s1\n\n";
		}*/
//		for (int i=0; i<mutex.size(); i++) {
//			qsys << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL mutex_" << mutex[i] << ".s1\n\n";
//		}
	}

	if (barrierUsed) {
		qsys << "add_connection " << moduleName << "_" << AccelCount << ".ACCEL legup_barrier_0.s1\n";
	}		
}


//prints tcl commands to generate QSYS
void SwOnly::printQSYSFilePCIe(raw_ostream &qsys, Function * F, unsigned long long baseAddr, int AccelCount) {

	// Runs for each accelerator	
	std::string moduleName = F->getName();
	stripInvalidCharacters(moduleName);
	qsys << "set num_threads " << AccelCount << "\n";
	qsys << "set baseAddress 0x";
	qsys.write_hex(baseAddr);
	qsys << "\n\n";

	qsys << "for {set i 0} {$i < $num_threads} {incr i} {" << "\n";
	qsys << "\tadd_instance " << moduleName << "_$i " << moduleName << "\n";


	// Connect clock to accelerators
	qsys << "\t# Connect clock to accelerators\n";
	qsys << "\tadd_connection pcie_ip.pcie_core_clk " << moduleName << "_$i.clockreset\n";
	qsys << "\tadd_connection pcie_ip.pcie_core_reset " << moduleName << "_$i.clockreset_reset\n";


	qsys << "\t# Connect PCIe to each accelerator\n";
	qsys << "\tadd_connection pcie_ip.bar0 " << moduleName << "_$i.s1\n";
	qsys << "\tset_connection_parameter_value pcie_ip.bar0/" << moduleName << "_$i.s1 baseAddress \"[expr $baseAddress + 64*$i]\"\n";
	qsys << "}\n\n";

	qsys << "save_system\n";
}

//prints tcl commands to generate QSYS
void SwOnly::printQSYSFilePCIeShared(raw_ostream &qsys, Function * F, unsigned long long baseAddr, int AccelCount) {

	static bool commonInstancesAdded = false;

	// Runs for each accelerator	
	std::string moduleName = F->getName();
	stripInvalidCharacters(moduleName);

	if (!commonInstancesAdded)
	{
		// Add on chip memory
		qsys << "# Add on chip memory\n";
		qsys << "add_instance onchip_memory altera_avalon_onchip_memory2\n\n";
		commonInstancesAdded = true;

		// Connect the on chip memory to SGDMA controller
		qsys << "# Connect the onchip memory to sgdma controller\n";
		qsys << "add_connection sgdma.m_read onchip_memory.s1\n";
		qsys << "add_connection sgdma.m_write onchip_memory.s1\n\n";

		// Configure the on chip memory
		qsys << "# Configure the onchip memory\n";
		qsys << "set_instance_parameter_value onchip_memory dualPort \"true\"\n";
		qsys << "set_instance_parameter_value onchip_memory dataWidth \"64\" \n";
		qsys << "set_instance_parameter_value onchip_memory memorySize \"786432\"\n";
		qsys << "set_connection_parameter_value sgdma.m_read/onchip_memory.s1 baseAddress \"0x40000000\"\n";
		qsys << "set_connection_parameter_value sgdma.m_write/onchip_memory.s1 baseAddress \"0x40000000\"\n";
		qsys << "add_connection pcie_ip.pcie_core_clk onchip_memory.clk1\n";
		qsys << "add_connection pcie_ip.pcie_core_clk onchip_memory.clk2\n";
		qsys << "add_connection pcie_ip.pcie_core_reset onchip_memory.reset1 \n";
		qsys << "add_connection pcie_ip.pcie_core_reset onchip_memory.reset2 \n\n";
	}
	qsys << "set num_threads " << AccelCount << "\n";
	qsys << "set baseAddress 0x";
	qsys.write_hex(baseAddr);
	qsys << "\n\n";

	qsys << "for {set i 0} {$i < $num_threads} {incr i} {" << "\n";
	qsys << "\tadd_instance " << moduleName << "_$i " << moduleName << "\n";

	// Add the accel_mem_bridge
	qsys << "\tadd_instance " << moduleName << "_accel_mem_bridge_$i accel_to_mem_bridge\n";

	// Connect clock to accelerators
	qsys << "\t# Connect clock to accelerators\n";
	qsys << "\tadd_connection pcie_ip.pcie_core_clk " << moduleName << "_$i.clockreset\n";
	qsys << "\tadd_connection pcie_ip.pcie_core_reset " << moduleName << "_$i.clockreset_reset\n";

	qsys << "\t# Connect clock to bridge\n";
	qsys << "\tadd_connection pcie_ip.pcie_core_clk " << moduleName << "_accel_mem_bridge_$i.clock\n";
	qsys << "\tadd_connection pcie_ip.pcie_core_reset " << moduleName << "_accel_mem_bridge_$i.reset\n";

	qsys << "\t# Connect PCIe to each accelerator\n";
	qsys << "\tadd_connection pcie_ip.bar0 " << moduleName << "_$i.s1\n";
	qsys << "\tset_connection_parameter_value pcie_ip.bar0/" << moduleName << "_$i.s1 baseAddress \"[expr $baseAddress + 64*$i]\"\n";

	qsys << "\t# Connect each accelerator to the bridge\n";
	qsys << "\tadd_connection " << moduleName << "_$i.ACCEL " << moduleName << "_accel_mem_bridge_$i.accel\n";

	qsys << "\tset_connection_parameter_value " << moduleName << "_$i.ACCEL/" << moduleName << "_accel_mem_bridge_$i.accel baseAddress \"0x0\"\n";

	qsys << "\t# Connect the bridge to mem\n";
	qsys << "\tadd_connection " << moduleName << "_accel_mem_bridge_$i.mem onchip_memory.s2\n";
	qsys << "\tset_connection_parameter_value " << moduleName << "_accel_mem_bridge_$i.mem/onchip_memory.s2 baseAddress \"0x0\"\n";
	qsys << "}\n\n";

	qsys << "save_system\n";
}

std::string SwOnly::printType(const Type * Ty, bool MemAddr) {
	std::string str_front = "";
	std::string str_back = "";

	if (dyn_cast<IntegerType>(Ty)) {
		str_front += printIntType(Ty);
	}
	else if (Ty->isFloatingPointTy()) {
		str_front += printFloatType(Ty);
	}
	else if (const PointerType *PTy = dyn_cast<PointerType>(Ty)) {
		const Type * ETy = PTy->getElementType();
		str_front = printType(ETy, MemAddr);
		if (!MemAddr) {
			str_back = "*";
			str_front.append(str_back);
		}
	}
	else if (const ArrayType *ATy = dyn_cast<ArrayType>(Ty)) { //if argument is an array
		const Type * ETy = ATy->getElementType();
		str_front = printType(ETy, MemAddr);
		if (!MemAddr) {
			str_back = "*";
			str_front.append(str_back);
		}	
	}
	else if (dyn_cast<StructType>(Ty)) {
		str_front = "void ";
	}
	else {
		assert(0 && "Unsupported Argument for Accelerator\n");
	}
	return str_front;
}

void SwOnly::getSwFunctions(Module &M, std::set<GlobalValue*> &SwFcts) {

	for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {
		if (F->getName() == "main") {
			SwFcts.insert(F);
			for (Function::iterator BB = F->begin(), EE = F->end(); BB != EE; ++BB) {
				for (BasicBlock::iterator I = BB->begin(), EE = BB->end(); I != EE; ++I) {
					if (CallInst *CI = dyn_cast<CallInst>(I)) {
						Function *calledFunction = CI->getCalledFunction();

						// ignore indirect function calls
						if (!calledFunction) continue;

						if (!(LEGUP_CONFIG->isAccelerated(*calledFunction))) {
							SwFcts.insert(calledFunction);
							addSwFunctions(calledFunction, SwFcts);
						}
					}
				}
			}
		}
	}
}

void SwOnly::addSwFunctions(Function *F, std::set<GlobalValue*> &SwFcts) {

	for (Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB) {
		for (BasicBlock::iterator I = BB->begin(), EE = BB->end(); I != EE;
				++I) {
			if (CallInst *CI = dyn_cast<CallInst>(I)) {

				Function *calledFunction = CI->getCalledFunction();
				// ignore indirect function calls
				if (!calledFunction) continue;

				if (!(LEGUP_CONFIG->isAccelerated(*calledFunction)) && SwFcts.find(calledFunction) == SwFcts.end()) {
					SwFcts.insert(calledFunction);
					addSwFunctions(calledFunction, SwFcts);
				}
			}
		}
	}
}

void SwOnly::getdeleteFunctions(Module &M, std::set<GlobalValue*> SwFcts, std::set<GlobalValue*> &deleteFcts) {

	for (Module::iterator I = M.begin(), E = M.end(); I != E; ++I) {
		Function * F = I;
		if (SwFcts.find(F) == SwFcts.end()) {
			deleteFcts.insert(F);
		}
	}
}

//include common signals signals for processor such as top, decode, branch, and cache signals and configure the modelsim wave file
void SwOnly::initWaveFile(raw_ostream &wave) {

	//add processor, instruction/data cache signals to waves

	if(LEGUP_CONFIG->getParameterInt("USE_QSYS")) {
			//waves for QSYS generated testbench
			wave << "onerror {resume}\n"
			<< "quietly WaveActivateNextPane {} 0\n"
			<< "add wave -noupdate -label clk -radix hexadecimal /tiger_tb/tiger_inst/tiger_top_0/clk\n"
			<< "add wave -noupdate -label pc -radix hexadecimal /tiger_tb/tiger_inst/tiger_top_0/core/pc\n"
			<< "add wave -noupdate -label ins -radix hexadecimal /tiger_tb/tiger_inst/tiger_top_0/ins\n"
			<< "add wave -noupdate -label reset_n -radix hexadecimal /tiger_tb/tiger_inst/tiger_top_0/reset\n"
			<< "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/tiger_top_0/*\n"
			<< "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/data_cache_0/*\n"
			<< "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/data_cache_0/state\n"
			<< "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/tiger_top_0/InsCache/*\n"
			<< "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/tiger_top_0/InsCache/state\n"
			<< "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/tiger_top_0/core/*\n"
			<< "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/tiger_top_0/core/de/*\n"
			<< "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/tiger_top_0/core/de/b/*\n\n";

			//add omp, lock, barrier cores to waves if they are used
//			if (ompUsed) {
//				wave << "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/omp_core_0/*\n\n";
//			}
			if (lockUsed) {
				wave << "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/mutex_0/*\n\n";
			}
			if (barrierUsed) {
				wave << "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/legup_barrier_0/*\n\n";
			}

			//add multi-ported caches to waves if they are used
			for (int i=0; i<dcacheway; i++) {
				if (dcacheType == "MP") {
					wave << "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/data_cache_0/loop_instantiateRAM[" << i << "]/dcacheMemIns/*\n\n";
				} else if (dcacheType == "LVT") {
					wave << "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/data_cache_0/loop_instantiateRAMArbiter[" << i << "]/dcacheMemIns/*\n\n";
					wave << "add wave -noupdate -radix hexadecimal /tiger_tb/tiger_inst/data_cache_0/loop_instantiateRAMArbiter[" << i << "]/cacheWrite_arbiter/*\n\n";
				}
			}
	} else {
		//waves for QSYS generated testbench
		wave << "onerror {resume}\n"
		<< "quietly WaveActivateNextPane {} 0\n"
		<< "add wave -noupdate -label clk -radix hexadecimal /test_bench/DUT/the_tiger_top_0/tiger_top_0/clk\n"
		<< "add wave -noupdate -label pc -radix hexadecimal /test_bench/DUT/the_tiger_top_0/tiger_top_0/core/pc\n"
		<< "add wave -noupdate -label ins -radix hexadecimal /test_bench/DUT/the_tiger_top_0/tiger_top_0/ins\n"
		<< "add wave -noupdate -label reset_n -radix hexadecimal /test_bench/DUT/the_tiger_top_0/tiger_top_0/reset\n"
		<< "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_tiger_top_0/tiger_top_0/*\n"
		<< "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_data_cache_0/data_cache_0/*\n"
		<< "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_data_cache_0/data_cache_0/state\n"
		<< "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_tiger_top_0/tiger_top_0/InsCache/*\n"
		<< "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_tiger_top_0/tiger_top_0/InsCache/state\n"
		<< "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_tiger_top_0/tiger_top_0/core/*\n"
		<< "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_tiger_top_0/tiger_top_0/core/de/*\n"
		<< "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_tiger_top_0/tiger_top_0/core/de/b/*\n\n";

		//add omp, lock, barrier cores to waves if they are used
//		if (ompUsed) {
//			wave << "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_omp_core_0/omp_core_0/*\n\n";
//		}
		if (lockUsed) {
			wave << "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_mutex_0/mutex_0/*\n\n";
		}
		if (barrierUsed) {
			wave << "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_legup_barrier_0/legup_barrier_0/*\n\n";
		}

		//add multi-ported caches to waves if they are used
		for (int i=0; i<dcacheway; i++) {
			if (dcacheType == "MP") {
				wave << "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_data_cache_0/data_cache_0/loop_instantiateRAM[" << i << "]/dcacheMemIns/*\n\n";
			} else if (dcacheType == "LVT") {
				wave << "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_data_cache_0/data_cache_0/loop_instantiateRAMArbiter[" << i << "]/dcacheMemIns/*\n\n";
				wave << "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_data_cache_0/data_cache_0/loop_instantiateRAMArbiter[" << i << "]/cacheWrite_arbiter/*\n\n";
			}
		}
	}
}

//adding accelerator_top signals to modelsim wave file 
void SwOnly::addAcceltoWaveFile(raw_ostream &wave, Function *F, int AccelCount) {

	std::string AccelName = F->getName();
	stripInvalidCharacters(AccelName);
	wave << "add wave -noupdate -radix hexadecimal /test_bench/DUT/the_" << AccelName << "_" << AccelCount << "/";
//	toLowerCase(AccelName);
	wave << AccelName << "_" << AccelCount << "/*\n\n";
}

//finishing modelsim wave file with some configurations 
void SwOnly::finishWaveFile(raw_ostream &wave) {

	wave << "TreeUpdate [SetDefaultTree]\n"
	<< "configure wave -namecolwidth 556\n"
	<< "configure wave -valuecolwidth 145\n"
	<< "configure wave -justifyvalue left\n"
	<< "configure wave -signalnamewidth 0\n"
	<< "configure wave -snapdistance 10\n"
	<< "configure wave -datasetprefix 0\n"
	<< "configure wave -rowmargin 4\n"
	<< "configure wave -childrowmargin 2\n"
	<< "configure wave -gridoffset 0\n"
	<< "configure wave -gridperiod 1\n"
	<< "configure wave -griddelta 40\n"
	<< "configure wave -timeline 0\n"
	<< "configure wave -timelineunits ns\n"
	<< "WaveRestoreZoom {109112943 ps} {109266688 ps}\n"
	<< "update\n";
}

void SwOnly::printCacheParametersFile () {
	
	//make file
	formatted_raw_ostream *file;
	std::string fileName = "cache_parameters.v";
	file = GetOutputStream(fileName);
	assert(file);
	raw_ostream &Out = *file;

	//assign the bitwidth of burst count
	int DburstCountWidth = 6; //these are hard-coded to 10 bits for now (up to 1024 bursts)
	int IburstCountWidth = 6;
	int burstCountWidth_divider;
	int sdramWidth;

	if (FPGABoard == "DE4") {
		burstCountWidth_divider = 32;
		sdramWidth = 256;
	} else {
		burstCountWidth_divider = 4;
		sdramWidth = 32;
	}

	std::string NUMPORTS;
	if (dcacheports == 2) {
		NUMPORTS = "TWO_PORTS";
	} else if (dcacheports == 4) {
		NUMPORTS = "FOUR_PORTS";
	}

	Out << "`ifndef CACHE_PARAMETERS_H\n"
	<< "`define CACHE_PARAMETERS_H\n\n";

	//defines cache parameters
	Out << "//define which board to use\n"
	<< "`define " << FPGABoard << "\n\n";

	Out << "//defines the number of ports to the cache\n"
	<< "//1 when only the processor, 2 when accelerator present\n"
	<< "`define NUM_DCACHE_PORTS " << dcacheports << "\n"
	<< "`define NUM_ICACHE_PORTS 1\n\n"

	<< "//this is defined if an accelerator is present\n"
	<< "`define ACCELERATOR_PORT\n"
	<< "`define " << NUMPORTS << "\n\n";

	if (multiportedCache) {
		Out << "`define " << dcacheType << "\n";
	}

	Out << "//defines the associativity of the cache\n"
	<< "`define DWAYS " << dcacheway << "\n"
	<< "`define IWAYS " << icacheway << "\n\n";

	Out << "//define the cache size\n"
	<< "`define DCACHE_SIZE "<< dcachenumlines << "\n" //1024-bit bus
	<< "`define DBLOCKSIZE " << dcachelinesize << "\n"
	<< "`define ICACHE_SIZE "<< icachenumlines << "\n"
	<< "`define IBLOCKSIZE " << icachelinesize << "\n\n"

	<< "//external memory bus width (256 = DDR2 on DE4, 32 = SDRAM on DE2)\n"
	<< "`define SDRAM_WIDTH " << sdramWidth << "\n"
	<< "`define BURSTCOUNT_DIV " << burstCountWidth_divider << "\n"
	<< "`define DBURSTCOUNTWIDTH " << DburstCountWidth << "\n"
	<< "`define IBURSTCOUNTWIDTH " << IburstCountWidth << "\n\n";

	Out << "//other definitions\n"
	<< "`define BYTE 8\n"
	<< "`define BYTEEN_WIDTH `SDRAM_WIDTH/`BYTE\n\n"

	<< "`define MP_cacheSize		`DCACHE_SIZE //log2(number of lines in cache)\n"
	<< "`define MP_cacheDepth		(2 ** `DCACHE_SIZE) //number of lines in cache\n"
	<< "`define MP_blockSize  `DBLOCKSIZE\n"
	<< "`define MP_blockSizeBits	(8*(2**`MP_blockSize)) //total data bits per cache line\n"
	<< "`define MP_cacheWidth		(`MP_blockSizeBits + `MP_tagSizeBits + 1) //total bits per cache line\n\n"

	<< "`define WORD_WIDTH	   `MP_cacheWidth\n"
	<< "`define WORD			 [`WORD_WIDTH-1:0]\n\n"

	<< "`define MEM_ADDR_WIDTH   `MP_cacheSize\n"
	<< "`define MEM_ADDR		 [`MEM_ADDR_WIDTH-1:0]\n\n"

	<< "`define MEM_DEPTH		`MP_cacheDepth\n"
	<< "`define MEM			  [`MEM_DEPTH-1:0]\n\n"

	<< "`define MP_tagSizeBits		31\n"
	<< "`define HIGH 1'b1\n"
	<< "`define LOW  1'b0\n\n"

	<< "// Used for write enables\n"
	<< "`define READ  `LOW\n"
	<< "`define WRITE `HIGH\n\n"

	<< "// Multipumping phases\n"
	<< "`define PHASE_WIDTH	 1\n"
	<< "`define PHASE		   [`PHASE_WIDTH-1:0]\n\n"

	<< "`define PHASE_0 `PHASE_WIDTH'd0\n"
	<< "`define PHASE_1 `PHASE_WIDTH'd1\n\n"

	<< "`define BYTE_EN [((2**`MP_blockSize)+4)-1:0]\n\n"

	<< "// Table pointing to which bank holds live register value\n"
	<< "`define LVT_ENTRY_WIDTH   2\n"
	<< "`define LVT_ENTRY		 [`LVT_ENTRY_WIDTH-1:0]\n\n"

	<< "// One entry for each register\n"
	<< "`define LVT_DEPTH		 (1 << `MEM_ADDR_WIDTH)\n\n"

	<< "// This changes with number of ports in memory\n"
	<< "`define ACCEL_0 `LVT_ENTRY_WIDTH'd0\n"
	<< "`define ACCEL_1 `LVT_ENTRY_WIDTH'd1\n"
	<< "`define ACCEL_2 `LVT_ENTRY_WIDTH'd2\n"
	<< "`define ACCEL_3 `LVT_ENTRY_WIDTH'd3\n\n"

	<< "`define HIGH 1'b1\n"
	<< "`define LOW  1'b0\n\n"

	<< "// Used for various write enables\n"
	<< "`define READ  `LOW\n"
	<< "`define WRITE `HIGH\n\n"

	<< "`endif\n";

	delete file;
}

//replace call to pthread_create to LegUp calling wrapper calls
bool SwOnly::replacePthreadCreate(Module &M, CallInst *CI, BasicBlock::iterator I, accelFcts &accel, const std::set<Function*> &internalAccels, unsigned loopTripCount) {

	//get the name of the function being forked to
	std::string FuncName = (CI->getArgOperand(2))->stripPointerCasts()->getName().str();
	//get the thread variable
	Value * threadVar = CI->getArgOperand(0);
	//get the argument into the thread
	Value * threadArg = CI->getArgOperand(3);
	//store into map
	//pthreadFcts[threadVar] = FuncName;
	//errs () << "function name is = " << FuncName << "\n";		

	//now you need to find the function pointer for the function
	Function * pthreadFuncPtr =	findFuncPtr(M, FuncName.c_str());
	assert(pthreadFuncPtr);
	
	Instruction* pthread_create_instr = CI;
	//delete the original call instruction 
	deleteInstruction(pthread_create_instr);

	//store data into accel struct
//	accelFcts accel;
	accel.fct = pthreadFuncPtr;
	accel.type = pthreadcall;
	accel.numAccelerated = loopTripCount;

	//see if this function is an avalon accelerator or an internal accelerator
	bool isAvalonAccel = (internalAccels.find(pthreadFuncPtr) == internalAccels.end());
/*
	int numcallingWrapper = 0;


	//only add to the vector if it's an avalon accelerator
	if (isAvalonAccel) {
		std::vector<accelFcts>::iterator it = std::find(AcceleratedFcts.begin(), AcceleratedFcts.end(), accel);
		//if the function hasn't already been added, add it to the vector
		if (it == AcceleratedFcts.end()) {
			AcceleratedFcts.push_back(accel);					
		} else { //if the same function already exists, then increment numAccelerated;
			numcallingWrapper = (*it).numAccelerated;
			(*it).numAccelerated++;
		}
	}
*/

	//updateAcceleratedFct(const bool isAvalonAccel, accelFcts accel, std::vector<accelFcts> &AcceleratedFcts) {

/*	
	//add to AcceleratedFcts only if it doesn't exist already
	std::vector<accelFcts>::iterator found = find(AcceleratedFcts.begin(), AcceleratedFcts.end(), accel);

	// errs() << " Accelerator: " << accel.fct->getName().str() << "\n";
	if ((found == AcceleratedFcts.end()) && isAvalonAccel) {
		AcceleratedFcts.push_back(accel);	
		// errs() << " Added " << accel.numAccelerated << " accelerators\n";
	} else {
		// errs() << "  Updating numAccelerated from: " << found->numAccelerated << " to ";
		found->numAccelerated += accel.numAccelerated;

		// errs() << found->numAccelerated << "\n";
	}
}*/

	//The pthread variable will be added as an argument
	//this argument will be used to store the base address of the HW accelerator at run time
	//this address will be read from in the join wrapper, in order to determine which HW accelerator to read from

	//first add the type of the arguments to this vector
	std::vector<Type*> newParamType;
	//set the argument type to the argument of the original pthread_create function
	//only add the threadArg type if it is not a null pointer
//	if (threadArg->isDereferenceablePointer()) {
	if (!dyn_cast<ConstantPointerNull>(threadArg)) {
		newParamType.push_back(threadArg->getType());
	}
	//add the thread variable type 
	newParamType.push_back(threadVar->getType());

	//now add the actual arguments to this vector
	std::vector<Value*> newParam;
	//only add the threadArg if it is not a null pointer
	if (!dyn_cast<ConstantPointerNull>(threadArg)) {
//	if (threadArg->isDereferenceablePointer()) {
		newParam.push_back(threadArg);
	}
	newParam.push_back(threadVar);

	//inserting new call to calling wrapper function//
	//get the name of the calling wrapper
	std::string wrapperName = getWrapperName(pthreadFuncPtr, pthreadcall);
/*	std::stringstream ss;
	ss << numcallingWrapper;
	wrapperName += ss.str();*/

	//add the function definition
	Constant* FCache = M.getOrInsertFunction(wrapperName, FunctionType::get(Type::getVoidTy(M.getContext()), newParamType, false));
	//create the new call instructions
	CallInst::Create(FCache, newParam, "", I);

	return isAvalonAccel;
}

// looks for calls in function F to any functions not in HwFcts
// if found, add the new function to HwFcts and call addCalledFunctions() on
// the new function
void SwOnly::updateAccelCount(Function *F, const std::set<Function*> &internalAccels, std::vector<accelFcts> &AcceleratedFcts, unsigned accelCount) {

	for (Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB) {
		for (BasicBlock::iterator I = BB->begin(), EE = BB->end(); I != EE;
				++I) {
			if (CallInst *CI = dyn_cast<CallInst>(I)) {

				Function *calledFunction = CI->getCalledFunction();
				std::string FuncName = calledFunction->getName().str();
				// errs() << "  FuncName = " << FuncName << "\n\n";

				// ignore indirect function calls
				if (!calledFunction) continue;

				if (LEGUP_CONFIG->isAccelerated(*calledFunction)) {
					// I know that we've already seen it, and marked it as a single accelerator
					// So need to add accelCount - 1 to it
					updateAcceleratedFcts(calledFunction, internalAccels, AcceleratedFcts, accelCount);
					//errs() << "  Updating numAccelerated from: " << found->numAccelerated << " to ";
					//found->numAccelerated += accelCount - 1;
					//errs() << found->numAccelerated << "\n";
				}
				else {
					// Recursive call to do this for all functions in tree
					// errs() << "  ";
					updateAccelCount(calledFunction, internalAccels, AcceleratedFcts, accelCount);
				}
			}

		}
	}
}

unsigned int SwOnly::getLoopTripCount(Function *pF, Function::iterator BB) {

	LI = &getAnalysis<LoopInfo>(*pF);
	assert(LI);

	// By default, if there is a hardware accel, its 1 so far
	unsigned int loopTripCount = 1;

	// Check if it actually has a loop
	if (Loop *loop = LI->getLoopFor(BB))
	{
		// Instantiate this many hardware accelerators
		loopTripCount = loop->getSmallConstantTripCount();
		// errs() << "\n\n Num Loop Iterations = " << numCallsToHardwareAccel << "\n\n";
	}
	assert(loopTripCount != 0 && "Could not analyze number of insts of accelerator");

	return loopTripCount;
}

void SwOnly::identifyPthreadFunctionsPCIe(Module &M, CallInst *CI, std::vector<accelFcts> &AcceleratedFcts, const std::set<Function*> &internalAccels, unsigned loopTripCount) {

	// Get the name of the function being forked to from the pthread_create call
	std::string FuncName = (CI->getArgOperand(2))->stripPointerCasts()->getName().str();
	// errs() << "\n\nPthread forks to FuncName = " << FuncName << "\n\n";
	Function *pthreadFuncPtr = findFuncPtr(M, FuncName.c_str());

	// Now recursively traverse the possible call stack
	// Update the count of accelerators for any calls
	// under this function
	updateAccelCount(pthreadFuncPtr, internalAccels, AcceleratedFcts, loopTripCount);
}

//replace call to pthread_join to LegUp polling wrapper calls
void SwOnly::replacePthreadJoin(Module &M, CallInst *CI, BasicBlock::iterator I, accelFcts &accel) {

	//get the thread variable
	Value * threadVar = CI->getArgOperand(0);
/*	Value *threadVar;
	if (LoadInst *load = dyn_cast<LoadInst>(Arg)) {
		threadVar = load->getOperand(0);
	} else {
		assert(0 && "Thread variable not found for pthread_join!\n");
	}*/

	bool returnNull = false;
	//get the return variable
	Value *retVar = CI->getArgOperand(1);
	if (isa<ConstantPointerNull>(retVar)) {
		returnNull = true;
	}

/*
	//find the name of thread routine in the map
	std::string FuncName = pthreadFcts.find(threadVar)->second;

	//errs () << "join function name is = " << FuncName << "\n";		

	Function * pthreadFuncPtr =	M.getFunction(FuncName);
	assert(pthreadFuncPtr);
*/

	Instruction* pthread_join_instr = CI;
	//delete the original call instruction 
	deleteInstruction(pthread_join_instr);
	//store data into accel struct
//	accelFcts accel;
	accel.fct = NULL; //set the function pointer to NULL because this is not needed for pthread join functions
	accel.type = pthreadpoll;
	accel.numAccelerated = 1;
	//bool isAvalonAccel = true; //set it to true to add it to pthreadFcts map

	//bool isAvalonAccel = (internalAccels.find(pthreadFuncPtr) == internalAccels.end());

/*	int numpollingWrapper = 0;

	//see if this function is an avalon accelerator or an internal accelerator
	
	//only add to the vector if it's an avalon accelerator
	if (isAvalonAccel) {
		std::vector<accelFcts>::iterator it = std::find(AcceleratedFcts.begin(), AcceleratedFcts.end(), accel);
		//if the function hasn't already been added, add it to the vector
		if (it == AcceleratedFcts.end()) {
			AcceleratedFcts.push_back(accel);					
		} else { //if the same function already exists, then increment numAccelerated;
			numpollingWrapper = (*it).numAccelerated;
			(*it).numAccelerated++;
		}
	}
*/


	//The pthread variable will be added as an argument
	//this argument will be used to load the base address of the HW accelerator at run time
	//reading from thread variable will be done inside the wrapper	

	//first add the type of the arguments to this vector
	std::vector<Type*> newParamType;
	//add the type of the thread variable
	newParamType.push_back(threadVar->getType());

	//now add the actual arguments to this vector
	std::vector<Value*> newParam;
	newParam.push_back(threadVar);
/*
	std::vector<Type*>params;
	//inserting new call to polling wrapper function//
	//get the name of the calling wrapper*/
//	std::string wrapperName = getWrapperName(pthreadFuncPtr, poll);
	std::string wrapperName = "legup_pthreadpoll";
/*	std::stringstream ss;
	ss << numpollingWrapper;
	wrapperName += ss.str();*/
	//add the function definition
	Constant* FCache;

	if (returnNull) {
		FCache = M.getOrInsertFunction(wrapperName, FunctionType::get(Type::getVoidTy(M.getContext()), newParamType, false));
	} else {
		FCache = M.getOrInsertFunction(wrapperName, FunctionType::get(PointerType::get(IntegerType::get(M.getContext(), 8), 0), newParamType, false));
	}

	//create the new call instructions
//	CallInst *NewCI = CallInst::Create(FCache, "", I);
	CallInst *NewCI = CallInst::Create(FCache, newParam, "", I);
	//if there is a return value, add a store instruction to store the return value to the second parameter in pthread_join
	//store i8*	NewCI, i8** retVar
	if (!returnNull) {
		//StoreInst::StoreInst(NewCI, retVar, ++I);
		new StoreInst(NewCI, retVar, false, I);
	}
}

void SwOnly::generateParallelAccelConfigs(const std::vector<accelFcts> &AcceleratedFcts) {

	formatted_raw_ostream *Out;
	std::string fileName = "parallelaccels.tcl";
	Out = GetOutputStream(fileName);
	assert(Out);
	raw_ostream &tcl = *Out;
	wrapperType type;

	if (!AcceleratedFcts.empty()) {
		for (std::vector<accelFcts>::const_iterator I = AcceleratedFcts.begin(), E = AcceleratedFcts.end(); I != E; ++I) {
			type = (*I).type;	
			if (type == pthreadcall || type == ompcall) {
				std::string funcName = ((*I).fct)->getName();		
				tcl << "set_parallel_accelerator_function \"" << funcName << "\"\n";
			}
		}
	}

	delete Out;
}

void SwOnly::setdefaultCacheParameters(const std::string cacheType, const int cachesize, int &cachelinesize, int &cacheway, int &cachenumlines) {

	//check that they are properly defined	
	//the parameters for a type of cache need to be either all defined or none defined
	//if none defined, they will use default values
	if (cachesize == 0 && cachelinesize == 0 && cacheway == 0) { //if none of the parameters are defined, assign default values
		cacheway = 1;
		if (FPGABoard == "DE4") {
			cachenumlines = 9;
			cachelinesize = 7;
		} else {
			cachenumlines = 9;
			cachelinesize = 4;
		}
	} else if (cachesize != 0 && cachelinesize != 0 && cacheway != 0) { //if they are all defined, change to log2 base
		cachenumlines = log2((cachesize*1024)/(cachelinesize*cacheway));
		cachelinesize = log2(cachelinesize);		
	} else {
		errs() << "If any of the parameters are defined for " << cacheType << " cache, they all need to be defined!\n";
		assert(0);
	}

}

void SwOnly::checkMaximumLinesize(const std::string cacheType, const int cachelinesize, const int maxValue) {
	if (cachelinesize > maxValue ) {
		int maxBytes = pow(2, maxValue);
		errs() << cacheType << " cache line size too large! The maximum " << cacheType << " cache line size is " << maxBytes << " bytes!\n";
		assert(0);
	}
} 

void SwOnly::checkLinesizes() {
	//check that the linesizes are smaller than the maximum allowed. 
	//the burstcount width is set to 6 bits (since DDR2 controller allows max burst of 64),
	//the maximum line size for DE4 is 256*64 = 16384 bits = 2048 bytes -> log2(2048) = 11
	//the maximum line size for DE2 is 32*64 = 2048 bits = 256 bytes -> log2(256) = 8
	if (FPGABoard == "DE4") {
		checkMaximumLinesize("data", dcachelinesize, 11);
		checkMaximumLinesize("instruction", icachelinesize, 11);
	} else {
		checkMaximumLinesize("data", dcachelinesize, 8);
		checkMaximumLinesize("instruction", icachelinesize, 8);
	}
}

void SwOnly::setCacheParameters() {

	//check that they are properly defined	
	//the parameters for a type of cache need to be either all defined or none defined
	//if none defined, they will use default values
	setdefaultCacheParameters("data", dcachesize, dcachelinesize, dcacheway, dcachenumlines);
	setdefaultCacheParameters("instruction", icachesize, icachelinesize, icacheway, icachenumlines);

	//set number of data cache ports
	if (dcacheports == 0) {
		dcacheports = 2; //default value is 2 if not specified
	}

	//check that the linesizes are smaller than the maximum allowed. 
	//the burstcount width is set to 6 bits (since DDR2 controller allows max burst of 64),
	//the maximum line size for DE4 is 256*64 = 16384 bits = 2048 bytes -> log2(2048) = 11
	//the maximum line size for DE2 is 32*64 = 2048 bits = 256 bytes -> log2(256) = 8
	checkLinesizes();
}

} // end of legup namespace
