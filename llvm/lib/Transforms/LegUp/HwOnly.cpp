//===- HwOnly.cpp - LegUp pre-LTO pass ------------------------------------===//
//
// This file is distributed under the LegUp license. See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
// The HwOnly pass strips away software only (non-accelerated) functions
//
//===----------------------------------------------------------------------===//

#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/Module.h"
#include "llvm/Instructions.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#include "utils.h"
#include <set>
#include <map>
#include "llvm/Support/CallSite.h"
#include "llvm/Attributes.h"
//#include "llvm/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"

#include "llvm/Analysis/CallGraph.h"
using namespace llvm;

namespace legup {

class HwOnly  : public ModulePass {
public:
    static char ID; // Pass identification, replacement for typeid
    HwOnly() : ModulePass(ID) {
        lockUsed = false;
        numOMPatomic = 0;
    }

    virtual bool runOnModule(Module &F);

    virtual void getAnalysisUsage(AnalysisUsage &AU) const {
    }

private:
	void getParallelAcceleratedFunctions(Module &M, std::set<GlobalValue*> &HwFcts);
	void replaceFuncCallwithNewFunc(Function *F, std::string oldfuncName, std::string newfuncName, Type *newfuncType, std::vector<Value*> newParam);
	void createOMPCloneFunctions(Module &M, const Function *HwFuncPtr, std::set<GlobalValue*> &HwFcts);
	bool replaceOMPFunctions(Module &M, CallInst *CI, Function *calledFunction, bool &isAvalonAccel, Function::iterator BB, BasicBlock::iterator &I, const std::set<Function*> &internalAccels, std::set<GlobalValue*> &HwFcts);
	bool replacePthreadFunctions(Module &M, CallInst *CI, Function *calledFunction, std::set<GlobalValue*> &HwFcts);
	//bool replaceSyncronizationFunctions(Module &M, CallInst *CI, Function *calledFunction);
	bool replaceSyncronizationFunctions(Module &M);
    //void replaceOMPFunctions(Module &M, const std::map<Function*, int> &ompFunctions, std::set<GlobalValue*> &HwFcts);
    //void replaceAPIFunctions(Module &M, const std::map<Function*, int> &parallelFunctions, std::set<GlobalValue*> &HwFcts);
    //void replaceAPIFunctions(Module &M, const std::set<Function*> &parallelFunctions, std::set<GlobalValue*> &HwFcts);
    void replaceAPIFunctions(Module &M, std::set<GlobalValue*> &HwFcts, const std::string APIFuncName, const std::string argName);
    //void replaceAPIFunctions(Module &M, std::set<GlobalValue*> &HwFcts);
//	bool replaceOMPParallel(Module &M, BasicBlock::iterator I, CallInst *CI, int &numOMPthreads, std::string &ompFuncName, const std::set<Function*> &internalAccels, Function *NF);
	std::map<Function*, int> ompFunctions;
//	std::map<Function*, int> pthreadFunctions;
	std::set<Function*> pthreadFunctions;
	std::map<std::string, int> mutexMap;
	std::vector<Function*> newOmpFunctions;
	Instruction* CItest;
    bool lockUsed;
    int numOMPatomic;
};

char HwOnly::ID = 0;
static RegisterPass<HwOnly> Y("legup-hw-only",
        "Strip away non-accelerated functions");

/// remove all functions that are s/w only (not accelerated)
/// Adapted from GlobalDCE pass
bool HwOnly::runOnModule(Module &M) {

    std::set<GlobalValue*> HwFcts;

    getParallelAcceleratedFunctions(M, HwFcts);
    getAcceleratedFunctions(M, HwFcts);

    return isolateGV(M, HwFcts);

}

void HwOnly::getParallelAcceleratedFunctions(Module &M,
        std::set<GlobalValue*> &HwFcts) {

	// skip in PCIe flow
	if (LEGUP_CONFIG->isPCIeFlow()) return;

	std::set<Function*> internalAccels;

	findInternalAccels(M, internalAccels);
	bool isAvalonAccel = true;

	//designate all omp functions as hardware accelerators
	for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {
	    for (Function::iterator BB = F->begin(), EE = F->end(); BB != EE; ++BB) {
	        for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E;) {

	            if (CallInst *CI = dyn_cast<CallInst>(I++)) {

	                Function *calledFunction = CI->getCalledFunction();

	                // ignore indirect function calls
	                if (!calledFunction) continue;

					if (replaceOMPFunctions(M, CI, calledFunction, isAvalonAccel, BB, I, internalAccels, HwFcts)) continue; 

					if (replacePthreadFunctions(M, CI, calledFunction, HwFcts)) continue;
					
//					if (replaceSyncronizationFunctions(M, CI, calledFunction)) continue;
		        }
		    }
		}
	}

    //std::set<Function*> ompChildFcts;
    //std::set<std::pair<std::set<GlobalValue*>, int>>> ompChildFctsSet;
    //std::map<Function*, int> newOmpFunctions;
    std::set<Function*> newOmpFunctions;
    std::vector<std::string> argNames;
	std::vector<Value*> argValues;
	int ompNumThreads;
	//build the vectors for extra arguments
//	argNames.push_back("numThreads");
	argNames.push_back("threadID");
//    if (lockUsed) {
//        argNames.push_back("threadMutexID");
//    }
	//insert new arguments into OMP functions
	for (std::map<Function*, int>::iterator it = ompFunctions.begin(); it != ompFunctions.end(); it++) {
		IntegerType * intparam = IntegerType::get(M.getContext(), 32);
		ompNumThreads = (*it).second;
		Value* newARG = ConstantInt::get(intparam, ompNumThreads);
//		argValues.push_back(newARG);
		argValues.push_back(newARG);
		//insert extra arguments into the function, and replace all calls to original function with the new fuction
		Function *NF = insertNewArgumentsAndReplaceCalls(*((*it).first), HwFcts, argNames, argValues, true);
		//replace the uses of the values returned from omp functions with the inserted arguments, numThreads and threadID	
		replaceOMPCalls(NF, ompNumThreads);
		//delete the original function from the module
		M.getFunctionList().remove((*it).first);
		argValues.clear();
        //insert the new omp functions to a set, these will be used later
		//newOmpFunctions.insert(std::pair<Function*, int>(NF, ompNumThreads));
        newOmpFunctions.insert(NF);

        //add descendant functions of current omp function to set
        //descendant function will be inserted with threadID and numThreads argument
        //addCalledFunctions2(NF, ompChildFcts); 
        //build a set of pairs, where it constains the set of descendant functions and the number of threads 
        //ompChildFctsSet.insert( std::pair<std::set<GlobalValue*>, int>>(ompChildFcts, ompNumThreads));
	}

    //clean up HwFcts
    for (std::set<GlobalValue*>::iterator it = HwFcts.begin(); it != HwFcts.end(); it++) {
        if ((*it) == NULL) {
          //  errs () << "empty function!\n";
            HwFcts.erase(*it);
        }
    }
//    for (std::set<Function*>::iterator it = internalAccels.begin(); it != internalAccels.end(); it++) {
//        errs () << "internal function Name is = " << (*it)->getName().str() <<"\n";
  //  }
//    for (std::set<GlobalValue*>::iterator it = HwFcts.begin(); it != HwFcts.end(); it++) {
//        errs () << "Function Name is = " << (*it)->getName().str() <<"\n";
  //  }
    //go through the program one more time to detect if there are calls to omp_get_thread_num()
    //replaceOMPFunctions(M, newOmpFunctions, HwFcts);
    replaceAPIFunctions(M, HwFcts, "omp_get_thread_num", "threadID");
    replaceAPIFunctions(M, HwFcts, "pthread_mutex_lock", "threadMutexID");
    replaceAPIFunctions(M, HwFcts, "GOMP_critical_start", "threadMutexID");
    replaceAPIFunctions(M, HwFcts, "GOMP_atomic_start", "threadMutexID");
    //replaceAPIFunctions(M, newOmpFunctions, HwFcts);
    //replaceAPIFunctions(M, pthreadFunctions, HwFcts);

   /*
	//insert threadID argument into OMP descendant functions
    //this is in case descenant function use omp_get_thread_num(), in which they will need to know the threadID
	for (std::set<Function*>::iterator it = ompChildFcts.begin(); it != ompChildFcts.end(); it++) {
		IntegerType * intparam = IntegerType::get(M.getContext(), 32);
		ompNumThreads = 1;
		Value* newARG = ConstantInt::get(intparam, ompNumThreads);
		argValues.push_back(newARG);
        Function* F = *it;
		//insert extra arguments into the function, and replace all calls to original function with the new fuction
		Function *NF = insertNewArgumentsAndReplaceCalls2(*F, HwFcts, argNames, argValues);
		//replace the uses of the values returned from omp functions with the inserted arguments, numThreads and threadID	
        //this function replaces the calls to omp_get_num_threads and omp_get_thread_num with the values of added arguments
		replaceOMPCalls(NF, ompNumThreads);
		//delete the original function from the module
		M.getFunctionList().remove(F);
		argValues.clear();
	}*/

	replaceSyncronizationFunctions(M);
}

//void HwOnly::replaceAPIFunctions(Module &M, const std::map<Function*, int> &parallelFunctions, std::set<GlobalValue*> &HwFcts) {
void HwOnly::replaceAPIFunctions(Module &M, std::set<GlobalValue*> &HwFcts, const std::string APIFuncName, const std::string argName) {
//void HwOnly::replaceOMPFunctions(Module &M, const std::map<Function*, int> &ompFunctions, std::set<GlobalValue*> &HwFcts) {

	std::string HwFuncName;
	Function *HwFuncPtr, *lockFuncPtr;
	Value *threadID;	
	Type *voidTy;
	std::vector<Value*>params;

    std::set<Function*> callerFunctions;

    //std::set< std::pair< std::set<Function*>, int > > callerFunctionsVector;
    //std::vector< std::pair< std::set<Function*>, int > > callerFunctionsVector;
	for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {
	    for (Function::iterator BB = F->begin(), EE = F->end(); BB != EE; ++BB) {
	        for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E;) {

	            if (CallInst *CI = dyn_cast<CallInst>(I++)) {

	                Function *calledFunction = CI->getCalledFunction();

	                // ignore indirect function calls
	                if (!calledFunction) continue;

					//if (calledFunction->getName().str() == "omp_get_thread_num") {
					if (calledFunction->getName().str() == APIFuncName) {
                        //add this function to the set 
                        callerFunctions.insert(F);
                        //find all callers to this function
                        findCallerFunctions(F, callerFunctions);
                    }
		        }
		    }
		}
	}

     
    //delete from callerFunctions if it not a hwfct
    //since we don't need to operate on functions which will not be in HW
    for (std::set<Function*>::iterator it = callerFunctions.begin(); it != callerFunctions.end(); it++) {
        //change isaccelerated to detect from parallelconfig.tcl which is 
        //if (HwFcts.find(*it) == HwFcts.end() || LEGUP_CONFIG->isAccelerated((*it)->getName().str())) {
        if (HwFcts.find(*it) == HwFcts.end()) {
            callerFunctions.erase(it);
        }
    }
    //errs () << APIFuncName << "\n";
    //for (std::set<GlobalValue*>::iterator it = HwFcts.begin(); it != HwFcts.end(); it++) {
        //errs () << "Function Name is = " << (*it)->getName().str() <<"\n";
    //}
    //for (std::set<Function*>::iterator it = callerFunctions.begin(); it != callerFunctions.end(); it++) {
        //errs () << "Caller Function Name is = " << (*it)->getName().str() <<"\n";
    //}


    //go through the callerFunctions
    //add an extra argument (threadID) to the function prototype
	std::vector<std::string> argNames;
//    std::vector<Function*> newOmpFunctions;
    //std::vector<Function*> newParallelFunctions;
    std::vector<pair<Function*,Function*> > newParallelFunctions;
    std::set<CallInst*> ompCallInsts;
    int ompNumThreads;
	//build the vectors for extra arguments
	//argNames.push_back("threadID");
	argNames.push_back(argName);
    bool argExists;
    for (std::set<Function*>::iterator it = callerFunctions.begin(); it != callerFunctions.end(); it++) {
        argExists = false;
        Function *F = (*it);
    
        //if top-level accelerator is a pthread accelerator AND argument is ThreadID
        //then don't need to add it
        //TO DO: FIX THIS HACK
        if ((LEGUP_CONFIG->isAccelerated(*F) || LEGUP_CONFIG->isParallelAccel(*F)) && (pthreadFunctions.find(F) != pthreadFunctions.end()) && argName == "threadID") {
            continue;
        }
        //check if this function prototype already has the argument
        for (Function::arg_iterator I = F->arg_begin(), E = F->arg_end(); I != E; ++I) {
            //if ((I)->getName() == "threadID") {
            if ((I)->getName() == argName) {
                argExists = true;
                break;
            }
        }
        //if it doesn't exists
        if (!argExists) {
            //insert extra argument into the function prototype
            //Function *NF = insertNewArgument(M, HwFcts, F->getName(), "threadID", IntegerType::get(M.getContext(), 32));
            Function *NF = insertNewArgument(M, HwFcts, F->getName(), argName, IntegerType::get(M.getContext(), 32));
            //replace the uses of the values returned from omp functions with the inserted arguments, numThreads and threadID	
            //this function replaces the calls to omp_get_num_threads and omp_get_thread_num with the values of added arguments
            replaceOMPCalls(NF, 4);
            newParallelFunctions.push_back(std::make_pair(F, NF));
//            newParallelFunctions.push_back(NF);
//
//
//

            //errs () << "Old Function = " << F->getName() << " New Function = " << NF->getName() << "\n";
        } else {
            //if it exists, replace calls in the original function and delete it from functions set since the call instruction has already been changed?
            replaceOMPCalls(F, 4);
            //newParallelFunctions.push_back(F);
            //callerFunctions.erase(it);
        }
    }
   
    for (std::vector<pair<Function*, Function*> >::iterator it = newParallelFunctions.begin(); it != newParallelFunctions.end(); it++) {

        Function *F = it->first;
        Function *NF = it->second;
        //add threadID to all call instructions calling the functions which were just added with extra argument
        //if it is a top-level function (avalon accelerator), call instruction doesn't have to be changed since it will be stripped away from HW portion
        Instruction *Ins;
        //errs () << "Going over function " << NF->getName().str() << "\n";
        //check if it is a top-level function 
        //TO DO:need to change this later so user doesn't have to specify (espcially for omp functions)
        if (!(LEGUP_CONFIG->isAccelerated(*NF) || LEGUP_CONFIG->isParallelAccel(*NF))) {
            //get all call site for function F
            for (Value::use_iterator UI = F->use_begin(), E = F->use_end(); UI != E;++UI){ 
                if (Ins = dyn_cast<Instruction>(*UI)) {
                    CallSite CS(Ins);
                    Instruction *Inst = CS.getInstruction();
                    if (CallInst *Call = dyn_cast<CallInst>(Inst)){

                        argExists = false;
                        std::vector<Value*> params;
                        Value *arg;
                        int argSize = Call->getNumArgOperands ();
                        //copy previous arguments 
                        for (int i=0; i<argSize; i++) {
                            arg = Call->getArgOperand(i);
                            //check if this call instruction already has the argument
                            //if (arg->getName().str() == "threadID") {
                            if (arg->getName().str() == argName) {
                                argExists = true;
                                break;
                            } 
                            params.push_back(Call->getArgOperand(i));
                        }
                        //if it does, or if this is an openmp function call (which already have been replaced),
                        //skip this call instruction
                        int parallelMetaData = getMetadataInt(Call, "OMP");
                        //if (argExists || parallelMetaData != 0) {
                        if (argExists) {
                            continue;
                        }
                        Function* parentF = Call->getParent()->getParent();
                        Value *newARG;
                        //find the presiously added argument from the function prototype
                        for (Function::arg_iterator I = parentF->arg_begin(), E = parentF->arg_end(); I != E; ++I) {
                            //if ((I)->getName() == "threadID") {
                            if ((I)->getName() == argName) {
                                newARG = I;
                            }
                        }
                        assert(newARG);
                        //add the new argument
                        params.push_back(newARG);
                        //ReplaceCallWith(NF->getName().str().c_str(), Call, params, NF->getReturnType());
                       
                        //create the new call instruction 
                        CallInst *New = llvm::CallInst::Create(NF, params, "", Call);
                        llvm::cast<llvm::CallInst>(New)->setCallingConv(Call->getCallingConv());
                        llvm::cast<llvm::CallInst>(New)->setAttributes(Call->getAttributes());
                        if (llvm::cast<llvm::CallInst>(Call)->isTailCall())
                          llvm::cast<llvm::CallInst>(New)->setTailCall();
                         params.clear();

                         copyMetaData(Call, New);
                         /*
                         //copy the metadata from old to new call instruction
                          SmallVector<std::pair<unsigned, MDNode *>, 4> MDs;
                          Call->getAllMetadata(MDs);
                          for (SmallVectorImpl<std::pair<unsigned, MDNode *> >::iterator
                               MI = MDs.begin(), ME = MDs.end(); MI != ME; ++MI) {
                           // MDNode *Old = MI->second;
                           // MDNode *New = MapValue(Old, VMap, Flags, TypeMapper);
                           // if (New != Old)
                              New->setMetadata(MI->first, MI->second);
                          }*/
                         //replace uses with the old instruction
                         if (!Call->use_empty())
                           Call->replaceAllUsesWith(New);
                         //take the name
                         New->takeName(Call);
                         // Finally, remove the old call from the program, reducing the use-count of
                         // F.
                         Call->eraseFromParent();
                         //errs () << "Function Name is = " << (*it)->getName().str() <<"\n";
                    }
                }
            }
        }
        //delete the original function from the module
        M.getFunctionList().remove(F);
    }
/*    //add threadID to all call instructions calling the functions which were just added with extra argument
    //if it is a top-level function (avalon accelerator), call instruction doesn't have to be changed since it will be stripped away from HW portion
	Function *CallerF;
	Instruction *Ins;
    //std::vector<Function*>::iterator it3 = newOmpFunctions.begin();
    std::vector<Function*>::iterator it3 = newParallelFunctions.begin();
    for (std::set<Function*>::iterator it = callerFunctions.begin(); it != callerFunctions.end(); it++) {
        Function* F = *it;
        Function* NF = *it3;
        errs () << "Going over function " << NF->getName().str() << "\n";
        //check if it is a top-level function 
        //TO DO:need to change this later so user doesn't have to specify (espcially for omp functions)
        if (LEGUP_CONFIG->isAccelerated(*NF) || LEGUP_CONFIG->isParallelAccel(*NF)) {
            errs() << "skipping function " << F->getName().str() << "\n";
            it3++;
            continue;
        }
        //get all call site for function F
        for (Value::use_iterator UI = F->use_begin(), E = F->use_end(); UI != E;++UI){ 
            if (Ins = dyn_cast<Instruction>(*UI)) {
                CallSite CS(Ins);
                Instruction *Inst = CS.getInstruction();
                if (CallInst *Call = dyn_cast<CallInst>(Inst)){

                    argExists = false;
                    std::vector<Value*> params;
                    Value *arg;
                    int argSize = Call->getNumArgOperands ();
                    //copy previous arguments 
                    for (int i=0; i<argSize; i++) {
                        arg = Call->getArgOperand(i);
                        //check if this call instruction already has the argument
                        //if (arg->getName().str() == "threadID") {
                        if (arg->getName().str() == argName) {
                            argExists = true;
                            break;
                        } 
                        params.push_back(Call->getArgOperand(i));
                    }
                    //if it does, or if this is an openmp function call (which already have been replaced),
                    //skip this call instruction
                    int parallelMetaData = getMetadataInt(Call, "OMP");
                    //if (argExists || parallelMetaData != 0) {
                    if (argExists) {
                        continue;
                    }
                    Function* parentF = Call->getParent()->getParent();
                    Value *newARG;
                    //find the presiously added argument from the function prototype
                    for (Function::arg_iterator I = parentF->arg_begin(), E = parentF->arg_end(); I != E; ++I) {
                        //if ((I)->getName() == "threadID") {
                        if ((I)->getName() == argName) {
                            newARG = I;
                        }
                    }
                    assert(newARG);
                    //add the new argument
                    params.push_back(newARG);
                    //ReplaceCallWith(NF->getName().str().c_str(), Call, params, NF->getReturnType());
                   
                    //create the new call instruction 
                    CallInst *New = llvm::CallInst::Create(NF, params, "", Call);
                    llvm::cast<llvm::CallInst>(New)->setCallingConv(Call->getCallingConv());
                    llvm::cast<llvm::CallInst>(New)->setAttributes(Call->getAttributes());
                    if (llvm::cast<llvm::CallInst>(Call)->isTailCall())
                      llvm::cast<llvm::CallInst>(New)->setTailCall();
                     params.clear();

                     //copy the metadata from old to new call instruction
                      SmallVector<std::pair<unsigned, MDNode *>, 4> MDs;
                      Call->getAllMetadata(MDs);
                      for (SmallVectorImpl<std::pair<unsigned, MDNode *> >::iterator
                           MI = MDs.begin(), ME = MDs.end(); MI != ME; ++MI) {
                       // MDNode *Old = MI->second;
                       // MDNode *New = MapValue(Old, VMap, Flags, TypeMapper);
                       // if (New != Old)
                          New->setMetadata(MI->first, MI->second);
                      }
                     //replace uses with the old instruction
                     if (!Call->use_empty())
                       Call->replaceAllUsesWith(New);

                     //take the name
                     New->takeName(Call);
                     // Finally, remove the old call from the program, reducing the use-count of
                     // F.
                     Call->eraseFromParent();
                     //errs () << "Function Name is = " << (*it)->getName().str() <<"\n";
                }
            }
        }
        it3++;
        //delete the original function from the module
        if (F != NF) {
            M.getFunctionList().remove(F);
        }
	}
    */
}


bool HwOnly::replaceSyncronizationFunctions(Module &M) {

	bool replaced = false;
	std::string HwFuncName;
	Function *HwFuncPtr, *lockFuncPtr;
	Value *threadID;	
	Type *voidTy;
	std::vector<Value*>params;

	for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {
	    for (Function::iterator BB = F->begin(), EE = F->end(); BB != EE; ++BB) {
	        for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E;) {

	            if (CallInst *CI = dyn_cast<CallInst>(I++)) {

	                Function *calledFunction = CI->getCalledFunction();

	                // ignore indirect function calls
	                if (!calledFunction) continue;

					if (calledFunction->getName().str() == "pthread_mutex_lock" || calledFunction->getName().str() == "GOMP_critical_start"  
                            || calledFunction->getName().str() == "GOMP_atomic_start") {
						//for now use name to differentiate mutexes
						//maybe have to change this later
    					std::string mutexName;
                        if (calledFunction->getName().str() == "pthread_mutex_lock") {
    						mutexName = CI->getArgOperand(0)->getName().str();
                        } else if (calledFunction->getName().str() == "GOMP_critical_start") {
    						mutexName = "OMP";
                        } else if (calledFunction->getName().str() == "GOMP_atomic_start") {
                            //this is for atomic section, which means it is used for a single memory update right after the pragma
                            //and every atomic section uses a different lock
                            mutexName = "OMP_atomic_" + utostr(numOMPatomic);
                        }
						
						//insert the mutex into map if it hasn't been already
						if (mutexMap.find(mutexName) == mutexMap.end()) {
                            //errs()<<"inserting mutex " << mutexName << "with index number " << mutexMap.size() << "\n\n\n\n";
							mutexMap.insert( std::pair<std::string, int>(mutexName, mutexMap.size()));
						}
						//errs () << "mutex " << mutexName << "\n";
						//errs () << "mutex size" << mutexMap.size() << "\n";

						//get the threadID argument from this function
						Function *F = CI->getParent()->getParent();
						for (Function::arg_iterator I = F->arg_begin(), E = F->arg_end(); I != E; ++I) {
							if ((I)->getName() == "threadMutexID") {
							//if ((I)->getName() == "threadID") {
								threadID = I;
							}
						}
						//errs() << "threadID name = " << threadID->getName() << "\n";
						//errs() << "mutex offset = " << mutexMap.find(mutexName)->second << "\n";
						assert(threadID);
					
						//get void type	
						voidTy = Type::getVoidTy(M.getContext());
						
						//create offset integer for mutex which steers memory access
						//ConstantInt* offset = ConstantInt::get(IntegerType::get(F->getContext(), 32), mutexMap.find(mutexName)->second, false);
                        //errs()<<"lock getting mutex " << mutexName << "with index number " << mutexMap.find(mutexName)->second << "\n";
						ConstantInt* offset = ConstantInt::get(IntegerType::get(M.getContext(), 32), mutexMap.find(mutexName)->second, false);
						//insert threadID
						params.push_back(threadID);
						//insert mutex offset
						params.push_back(offset);
				//		replaceFuncCallwithNewFunc(F, "pthread_mutex_lock", "legup_lock", voidTy, params);

						std::vector<Type*>paramType;
						paramType.push_back(threadID->getType());
						paramType.push_back(IntegerType::get(M.getContext(), 32));

						//create the new function definition
						Constant* FCache = M.getOrInsertFunction("legup_lock", FunctionType::get(voidTy, paramType, false));
						//insert the call to the new function
						Instruction* newCI = CallInst::Create(FCache, params, "", CI);

						Instruction* ins = CI;
					//	ins->replaceAllUsesWith(newCI);
						//delete the original call instruction 
						deleteInstruction(ins);

						params.clear();
						replaced = true;
					} else if (calledFunction->getName().str() == "pthread_mutex_unlock" || calledFunction->getName().str() == "GOMP_critical_end"
                            || calledFunction->getName().str() == "GOMP_atomic_end") {
						//for now use name to differentiate mutexes
						//maybe have to change this later
    					std::string mutexName;
                        if (calledFunction->getName().str() == "pthread_mutex_unlock") {
						    mutexName = CI->getArgOperand(0)->getName().str();
                        } else if (calledFunction->getName().str() == "GOMP_critical_end") {
    						mutexName = "OMP";
                        } else if (calledFunction->getName().str() == "GOMP_atomic_end") {
                            //this is for atomic section, which means it is used for a single memory update right after the pragma
                            //and every atomic section uses a different lock
                            mutexName = "OMP_atomic_" + utostr(numOMPatomic);
                            numOMPatomic++;
                        }
						
						//get the threadID argument from this function
						Function *F = CI->getParent()->getParent();
						for (Function::arg_iterator I = F->arg_begin(), E = F->arg_end(); I != E; ++I) {
							if ((I)->getName() == "threadMutexID") {
								threadID = I;
							}			
						}
						//errs() << "threadID name = " << threadID->getName() << "\n";
						//errs() << "mutex offset = " << mutexMap.find(mutexName)->second << "\n";
						assert(threadID);

						//get void type	
						voidTy = Type::getVoidTy(M.getContext());		
						//create offset integer for mutex which steers memory access
                        //errs()<<"unlock getting mutex " << mutexName << "with index number " << mutexMap.find(mutexName)->second << "\n";
						ConstantInt* offset = ConstantInt::get(IntegerType::get(M.getContext(), 32), mutexMap.find(mutexName)->second, false);

						//insert threadID
						params.push_back(threadID);
						//insert mutex offset
						params.push_back(offset);

						//replaceFuncCallwithNewFunc(F, "pthread_mutex_unlock", "legup_unlock", voidTy, params);


						std::vector<Type*>paramType;
						paramType.push_back(threadID->getType());
						paramType.push_back(IntegerType::get(M.getContext(), 32));
						//paramType.push_back(offset->getType());

						//create the new function definition
						Constant* FCache = M.getOrInsertFunction("legup_unlock", FunctionType::get(voidTy, paramType, false));
						//insert the call to the new function
						CallInst::Create(FCache, params, "", CI);

						Instruction* ins = CI;
						//delete the original call instruction 
						deleteInstruction(ins);
						params.clear();
						replaced = true;
					}
		        }
		    }
		}
	}

	return replaced;
}

bool HwOnly::replacePthreadFunctions(Module &M, CallInst *CI, Function *calledFunction, std::set<GlobalValue*> &HwFcts) {

	bool replaced = false;
	std::string HwFuncName;
	Function *HwFuncPtr, *lockFuncPtr;
//	Value *newARG;	
	Type *voidTy;
	std::vector<Value*>params;

	if (calledFunction->getName().str() == "pthread_create") {
		//get the name of the pthread routine 
		HwFuncName = (CI->getArgOperand(2))->stripPointerCasts()->getName().str();
		//find a function pointer to that routine		
		HwFuncPtr =	findFuncPtr(M, HwFuncName.c_str());
		assert(HwFuncPtr);
		//check if locks are used
		//Function * funcPtr = findFuncPtr(M, "pthread_mutex_lock");
		lockFuncPtr = findFuncPtr(M, "pthread_mutex_lock");	
		//if locks are found in the program
		if (lockFuncPtr != NULL) {
			//get the accelerated function, and insert a new argument into the accelerator, which passes in the threadID//
			//insert a new argument, threadID, into the pthread routine function
			//insertNewArgument(M, HwFcts, HwFuncName, "threadID", IntegerType::get(M.getContext(), 32));
			insertNewArgument(M, HwFcts, HwFuncName, "threadMutexID", IntegerType::get(M.getContext(), 32));
			//find the function pointer to the pthread routine function,
			//and iterate through the function to find calls to pthread_mutex_lock, pthread_mutex_unlock
			//replace these with calls to legup_lock, legup_unlock and send in the new argument as a parameter
			//find a function pointer to that routine		
			HwFuncPtr =	findFuncPtr(M, HwFuncName.c_str());
		
            lockUsed = true;    
//			params.push_back(newARG);
//			voidTy = Type::getVoidTy(M.getContext());
		//	replaceFuncCallwithNewFunc(HwFuncPtr, "pthread_mutex_lock", "legup_lock", voidTy, params);
//			replaceFuncCallwithNewFunc(HwFuncPtr, "pthread_mutex_unlock", "legup_unlock", voidTy, params);

		}

		pthreadFunctions.insert(HwFuncPtr);
		//always add pthreads as accelerators
		HwFcts.insert(HwFuncPtr);
		
		//add descendant functions
        addCalledFunctions(HwFuncPtr, HwFcts);
		replaced = true;
	}

	return replaced;
}

bool HwOnly::replaceOMPFunctions(Module &M, CallInst *CI, Function *calledFunction, bool &isAvalonAccel, Function::iterator BB, BasicBlock::iterator &I, const std::set<Function*> &internalAccels, std::set<GlobalValue*> &HwFcts) {

	bool isaOMPFuncCall, replaced = false;
	std::string HwFuncName;
	Function *HwFuncPtr;
	int numOMPthreads;
	accelFcts accel;

	//if this is a call to fork OMP threads
	if (calledFunction->getName().str() == "GOMP_parallel_start") {
		//get the OMP function
		HwFuncName = (CI->op_begin())->get()->getName().str();
		HwFuncPtr = findFuncPtr(M, HwFuncName.c_str());
		//get the number of threads
		ConstantInt * constInt = dyn_cast<ConstantInt>((CI->op_begin()+2)->get());
		assert(constInt);
		int numOMPthreads = constInt->getValue().getZExtValue();
		//add noinline attribute to the OMP function, since this should never be inlined
		HwFuncPtr->addFnAttr(1<<11);

		//check if it's top-level accelerator on Avalon
		isAvalonAccel = (internalAccels.find(HwFuncPtr) == internalAccels.end());
//		if (!isAvalonAccel) {
			//if not add it to the list of internal functions
			//these will be replaced later
			ompFunctions.insert(std::pair<Function*, int>(HwFuncPtr, numOMPthreads));
			//delete the call instruction
			deleteInstruction(CI);
//		}

		//add descendant functions
        addCalledFunctions(HwFuncPtr, HwFcts);

		replaced = true;
	} 
	//GOMP_parallel_end
	else if (calledFunction->getName().str() == "GOMP_parallel_end") {
		//if (!isAvalonAccel) {
			deleteInstruction(CI);
			//set it back to true
			isAvalonAccel = true;
		//}
		replaced = true;
	} 
	else if (calledFunction->getName().str() == "GOMP_critical_start" || calledFunction->getName().str() == "GOMP_atomic_start") {
        lockUsed = true;
	} 
	
	return replaced;
}

//this function iterates though function F and replaces a function call from oldfuncName to newfuncName with parameters newParam and a return type of newfuncType
void HwOnly::replaceFuncCallwithNewFunc(Function *F, std::string oldfuncName, std::string newfuncName, Type *newfuncType, std::vector<Value*> newParam) {

	Module *M = F->getParent();
	//iterate through this function to find the function call to funcName
	for (Function::iterator BB = F->begin(), EE = F->end(); BB != EE; ++BB) {
	    for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E;) {
	        if (CallInst *CI = dyn_cast<CallInst>(I++)) {

				Function *calledFunction = CI->getCalledFunction();

                // ignore indirect function calls
                if (!calledFunction) continue;

				//if this is a call to the wanted function
				if (calledFunction->getName().str().c_str() == oldfuncName) {
					std::vector<Type*>params;
					
					for (std::vector<Value*>::iterator iter = newParam.begin(); iter != newParam.end(); iter++) {
						params.push_back((*iter)->getType());
					}
					
					//create the new function definition
					Constant* FCache = M->getOrInsertFunction(newfuncName, FunctionType::get(newfuncType, params, false));
					//insert the call to the new function
					CallInst::Create(FCache, newParam, "", I);

					Instruction* ins = CI;
					//delete the original call instruction 
					deleteInstruction(ins);
				}
			}
		}
	}
}

void HwOnly::createOMPCloneFunctions(Module &M, const Function *HwFuncPtr, std::set<GlobalValue*> &HwFcts) {

	std::string FuncName; 
	
//	for (int i=0; i<numOMPthreads; i++) {

		FuncName = "legup_" + HwFuncPtr->getName().str();
		ValueToValueMapTy VMap;
		ClonedCodeInfo *CodeInfo = NULL;
		Function* duplicateFunction = CloneFunctionWithNewName(FuncName, HwFuncPtr, VMap, /*ModuleLevelChanges=*/true, CodeInfo);
		HwFcts.insert(duplicateFunction);
		newOmpFunctions.push_back(duplicateFunction);
		duplicateFunction->setLinkage(HwFuncPtr->getLinkage());
		//duplicateFunction->dump();
		M.getFunctionList().push_back(duplicateFunction);
//	}
}

/*
bool HwOnly::replaceOMPParallel(Module &M, BasicBlock::iterator I, CallInst *CI,  
	int &numOMPthreads, std::string &ompFuncName, const std::set<Function*> &internalAccels, Function *NF) {

	//get the name of the function being forked to
	ompFuncName = (CI->op_begin())->get()->getName().str();

	//get the number of threads
	ConstantInt * constInt = dyn_cast<ConstantInt>((CI->op_begin()+2)->get());
	assert(constInt);
	numOMPthreads = constInt->getValue().getZExtValue();

	//now you need to find the function pointer for the function
	Function * ompFuncPtr =	findFuncPtr(M, ompFuncName.c_str());

	//search to see if this function is one of the internal accelerators
	//if not found, that means its an avalon accelerator
	bool isAvalonAccel = (internalAccels.find(ompFuncPtr) == internalAccels.end());

	std::string NFname = ompFuncName;

	assert(NF);

	//get the argument into the OMP function
	Value * OMP_arg = CI->getArgOperand(1);	

	//delete the original call instruction 
	deleteInstruction(CI);	
		

	Type *intparam = Type::getInt32Ty(M.getContext());
//	insertNewArgument(M, HwFct, NFname, "numThreads", intparam);

	//inserting the call to legup_set_num_threads to set the number of threads in the omp core
	std::vector<Type*>paramType;
	std::vector<Value*>params;
	paramType.push_back(OMP_arg->getType());
//	paramType.push_back(intparam);

	//pushing integer type for numThreads
//	 Type *intparam = Type::getInt32Ty(M.getContext());
//	ArgTypes.push_back(Int32Ty);


//	paramType.push_back(intparam);
	ConstantInt* numThreads = ConstantInt::get(M.getContext(), APInt(32, numOMPthreads, false));

	params.push_back(OMP_arg);
//	params.push_back(numThreads);
	std::string wrapperName;
	CallInst* voidCI; Constant* FCache;

	if (isAvalonAccel) {
				
			//insert new calls to wrapper functions
			//get the name of the calling wrapper
			wrapperName = getWrapperName(NF, ompcall);
			//add the function definition
			FCache = M.getOrInsertFunction(wrapperName, FunctionType::get(Type::getVoidTy(M.getContext()), paramType, false));
			//create the new call instructions
			voidCI = CallInst::Create(FCache, params, "", I);
			//increment the calling wrapper counter
//			numcallingWrapper++;
//		}
	} else {
		wrapperName = NF->getName().str();
		FCache = M.getOrInsertFunction(wrapperName, FunctionType::get(NF->getReturnType(), paramType, false));
		voidCI = CallInst::Create(FCache, params, "", I);
		voidCI->setMetadata("OMP", MDNode::get(M.getContext(), MDString::get(M.getContext(), utostr(numOMPthreads))));
		voidCI->setMetadata("OMPTYPE", MDNode::get(M.getContext(), MDString::get(M.getContext(), "ompcall")));

	}
	
	return isAvalonAccel;
}
*/
} // end of legup namespace






