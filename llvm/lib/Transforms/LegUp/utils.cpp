//===- utils.cpp - LegUp pre-LTO helper functions -------------------------===//
//
// This file is distributed under the LegUp license. See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
// Legup helper functions
//
//===----------------------------------------------------------------------===//

#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/Module.h"
#include "llvm/CodeGen/IntrinsicLowering.h"
#include "llvm/Support/IRBuilder.h"
#include "llvm/Support/FormattedStream.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Instructions.h"
#include "llvm/GlobalValue.h"
#include "llvm/Intrinsics.h"
#include "llvm/Target/TargetData.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/Support/Debug.h"
#include "LegupConfig.h"
#include "utils.h"
#include <string>
#include "llvm/Support/Signals.h"
#include "llvm/LLVMContext.h"
#include "llvm/Support/CallSite.h"
#include "llvm/Attributes.h"
//#include "llvm/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
using namespace llvm;
using namespace std;

namespace legup {


//copy the metadata from old to new instruction
void copyMetaData(Instruction* Old, Instruction* New) {
    SmallVector<std::pair<unsigned, MDNode *>, 4> MDs;
    Old->getAllMetadata(MDs);
    for (SmallVectorImpl<std::pair<unsigned, MDNode *> >::iterator
        MI = MDs.begin(), ME = MDs.end(); MI != ME; ++MI) {
        New->setMetadata(MI->first, MI->second);
    }
}


    // looks for calls in function F to any functions not in HwFcts
// if found, add the new function to HwFcts and call addCalledFunctions() on
// the new function
void addCalledFunctions(Function *F, std::set<GlobalValue*> &HwFcts) {

    for (Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB) {
        for (BasicBlock::iterator I = BB->begin(), EE = BB->end(); I != EE;
                ++I) {
            if (CallInst *CI = dyn_cast<CallInst>(I)) {

                Function *calledFunction = CI->getCalledFunction();
                // ignore indirect function calls
                if (!calledFunction) continue;

                if (HwFcts.find(calledFunction) == HwFcts.end()) {
                    HwFcts.insert(calledFunction);
                    addCalledFunctions(calledFunction, HwFcts);
                }
            }
        }
    }
}

//adds the descendant functions to HwFcts which of a set of Function pointers
void addCalledFunctions2(Function *F, std::set<Function*> &HwFcts) {

    for (Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB) {
        for (BasicBlock::iterator I = BB->begin(), EE = BB->end(); I != EE;
                ++I) {
            if (CallInst *CI = dyn_cast<CallInst>(I)) {

                Function *calledFunction = CI->getCalledFunction();
                // ignore indirect function calls
                if (!calledFunction) continue;

                if (HwFcts.find(calledFunction) == HwFcts.end()) {
                    HwFcts.insert(calledFunction);
                    addCalledFunctions2(calledFunction, HwFcts);
                }
            }
        }
    }
}

/// get all non-accelerated functions to be deleted
void getAcceleratedFunctions(Module &M,
        std::set<GlobalValue*> &HwFcts) {

    for (Module::iterator I = M.begin(), E = M.end(); I != E; ++I) {
        if (LEGUP_CONFIG->isAccelerated(*I) || LEGUP_CONFIG->isParallelAccel(*I)) {
          HwFcts.insert(I);
          addCalledFunctions(I, HwFcts);
        }
    }

}

CallInst *ReplaceCallWith(const char *NewFn, CallInst *CI,
								 vector<Value*> Args,
                                 Type *RetTy) {
	// If we haven't already looked up this function, check to see if the
	// program already contains a function with this name.
	Module *M = CI->getParent()->getParent()->getParent();
	// Get or insert the definition now.
	std::vector<Type *> ParamTys;
	std::vector<Value*> Params;
	if (!Args.empty()) {
		for (vector<Value*>::iterator it = Args.begin(); it != Args.end(); ++it) {
			ParamTys.push_back((*it)->getType());
		}
	}

	Constant* FCache = M->getOrInsertFunction(NewFn, FunctionType::get(RetTy, ParamTys, false));

	Instruction * Ins = CI;
	CallInst *NewCI = CallInst::Create(FCache, Args, "", Ins);
	NewCI->setName(CI->getName());
	if (!CI->use_empty()) {
		CI->replaceAllUsesWith(NewCI);
	}

	// CI doesn't get erased, so do it explicitly
	CI->eraseFromParent();
	return NewCI;
}

std::vector<Value*> copyArguments (User::op_iterator startIdx, User::op_iterator endIdx) {
	std::vector<Value*> newParam;
	for (User::op_iterator it = startIdx; it != endIdx; ++it) {
		newParam.push_back(*it);
	}
	return newParam;
}

//find the function pointer with the given function name
Function * findFuncPtr (Module &M, const char *funcName) {
	for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {
		if(strcmp(F->getName().str().c_str(), funcName) == 0) {
			return F;
		}
	}
//	errs() << "Function " << *funcName << "not found!\n";
//	assert(0);
	return NULL;
}

void set_all_linkage(Module &M, GlobalValue::LinkageTypes LT) {
  for (Module::global_iterator I = M.global_begin(), E = M.global_end(); I != E;
      ++I)
    if (!I->isDeclaration()) {
      I->setLinkage(LT);
    }
  for (Module::iterator I = M.begin(), E = M.end(); I != E; ++I)
    if (!I->isDeclaration()) {
      I->setLinkage(LT);
    }
}

// copied isolateGV() and deleteGV() from ExtractGV
// TODO: How do I call another pass from here?
// opt: Pass.cpp:234: void<unnamed>::PassRegistrar::RegisterPass(const llvm::PassInfo&): Assertion `Inserted && "Pass registered multiple times!"' failed.
// ModulePass *GV = createGVExtractionPass(GVs, true, false);
// return GV->runOnModule(M);
bool isolateGV(Module &M, std::set<GlobalValue*> &Named) {

  // Mark all globals internal
  // FIXME: what should we do with private linkage?
  set_all_linkage(M, GlobalValue::InternalLinkage);

  // All of the functions may be used by global variables or the named
  // globals.  Loop through them and create a new, external functions that
  // can be "used", instead of ones with bodies.
  std::vector<Function*> NewFunctions;

  Function *Last = --M.end();  // Figure out where the last real fn is.

  for (Module::iterator I = M.begin(); ; ++I) {
    if (std::find(Named.begin(), Named.end(), &*I) == Named.end()) {
      Function *New = Function::Create(I->getFunctionType(),
                                       GlobalValue::ExternalLinkage);
      New->copyAttributesFrom(I);

      // If it's not the named function, delete the body of the function
      I->dropAllReferences();

      M.getFunctionList().push_back(New);
      NewFunctions.push_back(New);
      New->takeName(I);
    }

    if (&*I == Last) break;  // Stop after processing the last function
  }

  // Now that we have replacements all set up, loop through the module,
  // deleting the old functions, replacing them with the newly created
  // functions.
  if (!NewFunctions.empty()) {
    unsigned FuncNum = 0;
    Module::iterator I = M.begin();
    do {
      if (std::find(Named.begin(), Named.end(), &*I) == Named.end()) {
        // Make everything that uses the old function use the new dummy fn
        I->replaceAllUsesWith(NewFunctions[FuncNum++]);

        Function *Old = I;
        ++I;  // Move the iterator to the new function

        // Delete the old function!
        M.getFunctionList().erase(Old);

      } else {
        ++I;  // Skip the function we are extracting
      }
    } while (&*I != NewFunctions[0]);
  }

  // set function linkage back to external
  set_all_linkage(M, GlobalValue::ExternalLinkage);

  return true;
}

bool deleteGV(std::set<GlobalValue*> &Named) {

  // Let a later invocation of clang run Dead-Code Elimination for the PCIe flow rather than here
  if (LEGUP_CONFIG->isPCIeFlow()) {
    return false;
  }

  for (std::set<GlobalValue*>::iterator GI = Named.begin(), 
         GE = Named.end(); GI != GE; ++GI) {
    if (Function* NamedFunc = dyn_cast<Function>(*GI)) {
     NamedFunc->setLinkage(GlobalValue::ExternalLinkage);
     NamedFunc->deleteBody();
     assert(NamedFunc->isDeclaration() && "This didn't make the function external!");
   } else {
      if (!(*GI)->isDeclaration()) {
        cast<GlobalVariable>(*GI)->setInitializer(0);  //clear the initializer
        (*GI)->setLinkage(GlobalValue::ExternalLinkage);
      }
    }
  }
  return true;
}

/// GetOutputStream - return a stream to the given file
formatted_raw_ostream * GetOutputStream(string & OutputFilename) {

    // stdout
    if (OutputFilename == "-")
        return &fouts();

    // Make sure that the Out file gets unlinked from the disk if we get a
    // SIGINT
    sys::RemoveFileOnSignal(sys::Path(OutputFilename));

    std::string error;
    raw_fd_ostream *FDOut =
        new raw_fd_ostream(OutputFilename.c_str(), error,
                raw_fd_ostream::F_Binary);
    if (!error.empty()) {
        errs() << error << '\n';
        delete FDOut;
        exit(1);
    }
    formatted_raw_ostream *Out =
        new formatted_raw_ostream(*FDOut, formatted_raw_ostream::DELETE_STREAM);

    return Out;
}

void findInternalAccels(Module &M, std::set<Function*> &internalAccels) {

	std::string FuncName;
	Function *FuncPtr;
	for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {
	    for (Function::iterator BB = F->begin(), EE = F->end(); BB != EE; ++BB) {
			for (BasicBlock::iterator I = BB->begin(), EE = BB->end(); I != EE; ++I) {
			    if (CallInst *CI = dyn_cast<CallInst>(I)) {
			        Function *calledFunction = CI->getCalledFunction();
					//errs() << "called Function " << calledFunction->getName() << "\n\n";
			        // ignore indirect function calls
			        if (!calledFunction) continue;

					// Don't consider OpenMP or Pthread functions for PCIe flow
					if (LEGUP_CONFIG->isPCIeFlow()) {
						// if user-designated function
						if (LEGUP_CONFIG->isAccelerated(calledFunction->getName().str())) {
							FuncPtr = findFuncPtr(M, calledFunction->getName().str().c_str());
							addInternalAccels(M, FuncPtr, internalAccels);
						}
						continue;
					}

					std::string Name = calledFunction->getName().str();
					// if OpenMP function
					//if (calledFunction->getName().str() == "GOMP_parallel_start") {
					if (Name == "GOMP_parallel_start") {
						//get the name of the function being forked to
						FuncName = (CI->op_begin())->get()->getName().str();
						FuncPtr = findFuncPtr(M, FuncName.c_str());
						addInternalAccels(M, FuncPtr, internalAccels);
					} 
					// if Pthread function
					else if (calledFunction->getName().str() == "pthread_create") {
						//get the name of the function being forked to
						FuncName = (CI->getArgOperand(2))->stripPointerCasts()->getName().str();
						FuncPtr = findFuncPtr(M, FuncName.c_str());
						addInternalAccels(M, FuncPtr, internalAccels);
					}
					// if user-designated function
					else if (LEGUP_CONFIG->isAccelerated(calledFunction->getName().str())) {
						FuncPtr = findFuncPtr(M, calledFunction->getName().str().c_str());
						addInternalAccels(M, FuncPtr, internalAccels);
					}
			    }
			}
		}
	}
}

void addInternalAccels(Module &M, Function *F, std::set<Function*> &internalAccels) {

	std::string FuncName;
	Function *FuncPtr;
    for (Function::iterator BB = F->begin(), E = F->end(); BB != E; ++BB) {
        for (BasicBlock::iterator I = BB->begin(), EE = BB->end(); I != EE; ++I) {
            if (CallInst *CI = dyn_cast<CallInst>(I)) {

                Function *calledFunction = CI->getCalledFunction();
                // ignore indirect function calls
                if (!calledFunction) continue;

				if (calledFunction->getName().str() == "GOMP_parallel_start") {
					//get the name of the function being forked to
					FuncName = (CI->op_begin())->get()->getName().str();
					FuncPtr = findFuncPtr(M, FuncName.c_str());
					internalAccels.insert(FuncPtr);
					addInternalAccels(M, FuncPtr, internalAccels);
				} 
				else if (calledFunction->getName().str() == "pthread_create") {
					//get the name of the function being forked to
					FuncName = (CI->getArgOperand(2))->stripPointerCasts()->getName().str();
					FuncPtr = findFuncPtr(M, FuncName.c_str());
					internalAccels.insert(FuncPtr);
					addInternalAccels(M, FuncPtr, internalAccels);
				}
				// if user-designated function
				else if (LEGUP_CONFIG->isAccelerated(calledFunction->getName().str())) {
					FuncPtr = findFuncPtr(M, calledFunction->getName().str().c_str());
					internalAccels.insert(FuncPtr);
					addInternalAccels(M, FuncPtr, internalAccels);
				}
            }
        }
    }
}


//this function replaces the calls to omp_get_num_threads and omp_get_thread_num with the values of added arguments
void replaceOMPCalls(Function *F, int numThreads) {
	
	Value *numThreadsArg;
	Value *threadIDArg;
	
	for (Function::arg_iterator I = F->arg_begin(), E = F->arg_end(); I != E; ++I) {
		if ((I)->getName() == "numThreads") {
			numThreadsArg = I;
		}
		else if ((I)->getName() == "threadID") {
			threadIDArg = I;
		}
	}
	
	ConstantInt* numThreads1 = ConstantInt::get(IntegerType::get(F->getContext(), 32), numThreads, false);
//	ConstantInt* numThreads = ConstantInt::get(F->getContext(), APInt(32, numThreads, false));

	for (Function::iterator BB = F->begin(), EE = F->end(); BB != EE; ++BB) {
		for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E;) {
			Instruction * Inst = I++;
			//If it's a call instruction
			if (CallInst *CI = dyn_cast<CallInst>(Inst)) {	            
				Function *calledFunc = CI->getCalledFunction();
				// for indirect function calls
				if (!calledFunc) {
					Value *Callee = CI->getCalledValue();
			        if (ConstantExpr *CE = dyn_cast<ConstantExpr>(Callee)) {
			            if (Function *RF = dyn_cast<Function>(CE->getOperand(0))) {
			                Function *called = RF;
							if (called->getName().str() == "omp_get_num_threads") {
								//Inst->replaceAllUsesWith(numThreadsArg);
								Inst->replaceAllUsesWith(numThreads1);
								deleteInstruction(Inst);	
							}

							else if (called->getName().str() == "omp_get_thread_num") {
								Inst->replaceAllUsesWith(threadIDArg);
								deleteInstruction(Inst);	
							}
			            }
			        }
					//Value *a = CI->getArgOperand(0);
					continue;
				}
			
				if (calledFunc->getName().str() == "omp_get_num_threads") {
					//Inst->replaceAllUsesWith(numThreadsArg);
					Inst->replaceAllUsesWith(numThreads1);
					deleteInstruction(Inst);	
				}

				else if (calledFunc->getName().str() == "omp_get_thread_num") {
					Inst->replaceAllUsesWith(threadIDArg);
					deleteInstruction(Inst);	
				}
			
				/*
				else if ((CI->getArgOperand(0))->isCast()) {
					errs () <<"Cast found!\n\n";

				}
				else if ((CI->getArgOperand(0))->stripPointerCasts()->getName().str() == "omp_get_thread_num") {
					Inst->replaceAllUsesWith(threadIDArg);
					deleteInstruction(Inst);	
				}*/
			}
		}
	}
}

Function* cloneFunction(Module &M, const Function *HwFuncPtr, std::set<GlobalValue*> &HwFcts) {

	std::string FuncName; 
	
//	for (int i=0; i<numOMPthreads; i++) {

		FuncName = "legup_" + HwFuncPtr->getName().str();
		ValueToValueMapTy VMap;
		ClonedCodeInfo *CodeInfo = NULL;
		Function* duplicateFunction = CloneFunctionWithNewName(FuncName, HwFuncPtr, VMap, /*ModuleLevelChanges=*/true, CodeInfo);
		HwFcts.insert(duplicateFunction);
		duplicateFunction->setLinkage(HwFuncPtr->getLinkage());
		//duplicateFunction->dump();
		M.getFunctionList().push_back(duplicateFunction);
//	}
	return duplicateFunction;
}

bool replaceOMPParallelStart(Module &M, Function::iterator BB, BasicBlock::iterator I, CallInst *CI, accelFcts &accel, 
	int &numOMPthreads, std::string &ompFuncName, const std::set<Function*> &internalAccels) {

	//get the name of the function being forked to
	ompFuncName = (CI->op_begin())->get()->getName().str();

	//get the number of threads
	ConstantInt * constInt = dyn_cast<ConstantInt>((CI->op_begin()+2)->get());
	assert(constInt);
	numOMPthreads = constInt->getValue().getZExtValue();

	//now you need to find the function pointer for the function
	Function * ompFuncPtr =	findFuncPtr(M, ompFuncName.c_str());

	//search to see if this function is one of the internal accelerators
	//if not found, that means its an avalon accelerator
	bool isAvalonAccel = (internalAccels.find(ompFuncPtr) == internalAccels.end());

	//get the argument into the OMP function
	Value * OMP_arg = CI->getArgOperand(1);	

	//delete the original call instruction 
	deleteInstruction(CI);	
	
	accel.fct = ompFuncPtr;
	//accel.fct = ompFuncPtr;
	accel.type = ompcall;
	accel.numAccelerated = numOMPthreads;

	//inserting the call to legup_set_num_threads to set the number of threads in the omp core
	std::vector<Type*>paramType;
	std::vector<Value*>params;
/*	IntegerType * intparam = IntegerType::get(M.getContext(), 32);
	params.push_back(intparam);
	//insert the function definition to the program, which has an argument of type int, which is used to send in the number of threads
	Constant* omp_set_threads = M.getOrInsertFunction("legup_set_num_threads", FunctionType::get(Type::getVoidTy(M.getContext()), params, false));
	ConstantInt* numthreadsARG = ConstantInt::get(M.getContext(), APInt(32, numOMPthreads, false));
	//create the call instruction to legup_set_num_threads
	CallInst::Create(omp_set_threads, numthreadsARG, "", BB->begin());	

	//set the argument type to the argument of the original omp_parallel_start function
	params.clear();*/
	paramType.push_back(OMP_arg->getType());

	IntegerType * intparam = IntegerType::get(M.getContext(), 32);
//	paramType.push_back(intparam);
	//insert the function definition to the program, which has an argument of type int, which is used to send in the number of threads
	ConstantInt* numThreads = ConstantInt::get(M.getContext(), APInt(32, numOMPthreads, false));

	params.push_back(OMP_arg);
//	params.push_back(numThreads);
	std::string wrapperName;
	CallInst* voidCI; Constant* FCache;
	//if it's an avalon accelerator
	
//	insertNewArgument2(*ompFuncPtr, "numThreads", intparam);

	if (isAvalonAccel) {
		//insert new calls to wrapper functions
		//get the name of the calling wrapper
		wrapperName = getWrapperName(ompFuncPtr, ompcall);
		//add the function definition
		FCache = M.getOrInsertFunction(wrapperName, FunctionType::get(Type::getVoidTy(M.getContext()), paramType, false));
		//create the new call instructions
		voidCI = CallInst::Create(FCache, params, "", I);
	}
/*
	} else {
		//wrapperName = "legup_" + NF->getName().str();
		wrapperName = ompFuncPtr->getName().str();
		//create the new call instructions
		//add the function definition
//		FCache = M.getOrInsertFunction(wrapperName, FunctionType::get(ompFuncPtr->getReturnType(), paramType, false));
//		voidCI = CallInst::Create(FCache, params, "", I);
		//FCache = M.getOrInsertFunction(wrapperName, FunctionType::get(NF->getReturnType(), paramType, false));
		//voidCI = CallInst::Create(NF, params, "", I);
		//increment the calling wrapper counter
		//numcallingWrapper++;
//	insertNewArgument2(*(M.getFunction(ompFuncName)), "numThreads", intparam);
//		voidCI->setMetadata("OMP", MDNode::get(M.getContext(), MDString::get(M.getContext(), utostr(numOMPthreads))));
//		voidCI->setMetadata("OMPTYPE", MDNode::get(M.getContext(), MDString::get(M.getContext(), "ompcall")));
//		voidCI->setMetadata("OMP", MDNode::get(I->getContext(), MDString::get(I->getContext(), utostr(numOMPthreads))));
//		voidCI->setMetadata("OMPTYPE", MDNode::get(I->getContext(), MDString::get(I->getContext(), "ompcall")));

	}
	
*/	
	//actually if it's an internal accelerator, it will be removed from swonly, hence no need to add anything here 
	//metadata should be added before, either in parallel accels pass, or later in hwonly pass. 
	/*	
	else {
		//if it's an internal accelerator, only add the call instruction once
		//add metadata indication that it's OMP and save the number of parallel threads
		wrapperName = getWrapperName(ompFuncPtr, call);
		//add the function definition
		FCache = M.getOrInsertFunction(wrapperName, FunctionType::get(Type::getVoidTy(M.getContext()), params, false));
		//create the new call instructions
		voidCI = CallInst::Create(FCache, OMP_arg, "", I);
		//setMetadataIns(voidCI, "OMP", numOMPthreads);
		I->setMetadata("OMP", MDNode::get(I->getContext(), MDString::get(I->getContext(), utostr(numOMPthreads))));
	}*/

	return isAvalonAccel;
}

bool replaceOMPParallelEnd(Module &M, BasicBlock::iterator I, CallInst *CI, accelFcts &accel, 
	int numOMPthreads, std::string ompFuncName, const std::set<Function*> &internalAccels) {

	//errs () << "omp function name " << ompFuncName << "\n";
	//now you need to find the function pointer for the function
	Function * ompFuncPtr =	findFuncPtr(M, ompFuncName.c_str());

	//search to see if this function is one of the internal accelerators
	//if not found, that means its an avalon accelerator
	bool isAvalonAccel = (internalAccels.find(ompFuncPtr) == internalAccels.end());

	//errs () << "Avalon Accel ? " <<  isAvalonAccel << "\n\n";
	//delete the original call instruction 
	deleteInstruction(CI);
	
	accel.fct = ompFuncPtr;
	accel.type = omppoll;
	accel.numAccelerated = numOMPthreads;

///	int numpollingWrapper=0;
	Constant* FCache; CallInst* voidCI;
	std::string wrapperName;
	std::vector<Type*> params;
	if (isAvalonAccel) {
		//add it as many times as there are number of OMP threads																			
//		for (int i = 0; i < numOMPthreads; i++) {
			//insert new calls to polling wrapper functions
			//get the name of the polling wrapper
			wrapperName = getWrapperName(ompFuncPtr, omppoll);
//			std::stringstream ss;
//			ss << numpollingWrapper;
//			wrapperName += ss.str();

			//create the new call instructions
			FCache = M.getOrInsertFunction(wrapperName, FunctionType::get(Type::getVoidTy(M.getContext()), params, false));
			CallInst::Create(FCache, "", I);
//			numpollingWrapper++;	
//		}
	} else {
		wrapperName = ompFuncPtr->getName().str();
		//stripInvalidCharacters(wrapperName);
		//add the function definition
		//FCache = M.getOrInsertFunction(wrapperName, FunctionType::get(Type::getVoidTy(M.getContext()), params, false));
		//create the new call instructions
		//voidCI = CallInst::Create(FCache, "", I);
		//voidCI = CallInst::Create(ompFuncPtr, "", I);
		
		voidCI = CallInst::Create(ompFuncPtr, ConstantPointerNull::get(PointerType::get(IntegerType::get(M.getContext(), 8), 0)), "", I);
		//increment the calling wrapper counter
		voidCI->setMetadata("OMP", MDNode::get(I->getContext(), MDString::get(I->getContext(), utostr(numOMPthreads))));
		voidCI->setMetadata("OMPTYPE", MDNode::get(I->getContext(), MDString::get(I->getContext(), "omppoll")));
	}
	return isAvalonAccel;
}

/// getWrapperName - gives the name of the legup wrapper for a function
/// wrapperType may be seq, call, poll, lock, or unlock
/// if wrapperType is seq, call, or poll, append the function name at the end
string getWrapperName(Function *F, wrapperType type) {
	string wrapperName = "legup_";
    string funcName;
	if (type != pthreadpoll) {
		funcName = F->getName();
	}
    stripInvalidCharacters(funcName);	

	if (type == pthreadcall) {
		wrapperName += "pthreadcall_" + funcName;
	} else if (type == pthreadpoll) {
		wrapperName += "pthreadpoll"; //there is only one pthread polling wrapper for all pthread functions
	} else if (type == ompcall) {
		wrapperName += "ompcall_" + funcName;
	} else if (type == omppoll) {
		wrapperName += "omppoll_" + funcName;
	} else if (type == seq) {
		wrapperName += "seq_" + funcName;
	} else if (type == lock) {
		wrapperName += "lock";
	} else if (type == unlock) {
		wrapperName += "unlock";
	}

    return wrapperName;
}

void deleteInstruction(Instruction *I) {

	if (!I->use_empty()) {
		I->replaceAllUsesWith(UndefValue::get(I->getType()));
	}
	I->eraseFromParent();
}

//this function was copied from CloneFunction in Transforms/Utils/CloneFunction.cpp
Function *CloneFunctionWithNewName(std::string newName, const Function *F, ValueToValueMapTy &VMap,
                              bool ModuleLevelChanges,
                              ClonedCodeInfo *CodeInfo) {
  std::vector<Type*> ArgTypes;

  // The user might be deleting arguments to the function by specifying them in
  // the VMap.  If so, we need to not add the arguments to the arg ty vector
  //
  for (Function::const_arg_iterator I = F->arg_begin(), E = F->arg_end();
       I != E; ++I)
    if (VMap.count(I) == 0)  // Haven't mapped the argument to anything yet?
      ArgTypes.push_back(I->getType());

	 Type *Int32Ty = Type::getInt32Ty(F->getParent()->getContext());
	ArgTypes.push_back(Int32Ty);
  // Create a new function type...
  FunctionType *FTy = FunctionType::get(F->getFunctionType()->getReturnType(),
                                    ArgTypes, F->getFunctionType()->isVarArg());

  // Create the new function...
  Function *NewF = Function::Create(FTy, F->getLinkage(), newName);

Value *newARG = --NewF->arg_end();
newARG->setName("numThreads");

  // Loop over the arguments, copying the names of the mapped arguments over...
  Function::arg_iterator DestI = NewF->arg_begin();
  for (Function::const_arg_iterator I = F->arg_begin(), E = F->arg_end();
       I != E; ++I)
    if (VMap.count(I) == 0) {   // Is this argument preserved?
      DestI->setName(I->getName()); // Copy the name over...
      VMap[I] = DestI++;        // Add mapping to VMap
    }

  SmallVector<ReturnInst*, 8> Returns;  // Ignore returns cloned.
  CloneFunctionInto(NewF, F, VMap, ModuleLevelChanges, Returns, "", CodeInfo);
  return NewF;
}


//this function copies Fn to a new function, inserts new arguments in argValues to the new function, and replaces all calls from the original function to the new function
Function *insertNewArgumentsAndReplaceCalls(Function &Fn, std::set<GlobalValue*> &HwFct, const std::vector<std::string> &argNames, const std::vector<Value*> &argValues, bool isInternalAccel) {

	// Start by computing a new prototype for the function, which is the same as
	// the old function, but doesn't have isVarArg set.
	FunctionType *FTy = Fn.getFunctionType();

	std::vector<Type*> Params(FTy->param_begin(), FTy->param_end());
	unsigned NumArgs = Params.size();

	//insert the new arg types
	for (std::vector<Value*>::const_iterator it = argValues.begin(); it != argValues.end(); it++) {
		Params.push_back((*it)->getType());
	}
	
	FunctionType *NFTy = FunctionType::get(FTy->getReturnType(),
												 Params, false);

	// Create the new function body and insert it into the module...
	Function *NF = Function::Create(NFTy, Fn.getLinkage());
//	Function *NF = Function::Create(NFTy, Function::ExternalLinkage);
	NF->copyAttributesFrom(&Fn);
	Fn.getParent()->getFunctionList().insert(&Fn, NF);
	
	NF->takeName(&Fn);

	// Loop over the argument list, transferring uses of the old arguments over to
	// the new arguments, also transferring over the names as well.  While we're at
	// it, remove the dead arguments from the DeadArguments list.
	//
	for (Function::arg_iterator I = Fn.arg_begin(), E = Fn.arg_end(),
		I2 = NF->arg_begin(); I != E; ++I, ++I2) {
	 // Move the name and users over to the new version.
	 I->replaceAllUsesWith(I2);
	 I2->takeName(I);
		if (I == --(Fn.arg_end())) {
			//set the names of the new arguments
			for (std::vector<std::string>::const_iterator it = argNames.begin(); it != argNames.end(); it++) {
				I2++;
				I2->setName(*it);
			}
		}
	}

	NF->getBasicBlockList().splice(NF->begin(), Fn.getBasicBlockList());

	// Loop over all of the callers of the function, transforming the call sites
	// to pass in a smaller number of arguments into the new function.
	//
	std::vector<Value*> Args;
      llvm::Instruction *New;
      llvm::Instruction *Before;
	while (!Fn.use_empty()) {
	 CallSite CS(Fn.use_back());
	 Instruction *Call = CS.getInstruction();

	 // Pass all the same arguments.
	 Args.assign(CS.arg_begin(), CS.arg_begin() + NumArgs);
	
//	 Args.push_back(newARG1);
	
	//insert the new arguments
	for (std::vector<Value*>::const_iterator it = argValues.begin(); it != argValues.end(); it++) {
		Args.push_back(*it);
	}
	
	
      Before = Call;
	
      if (llvm::InvokeInst *II = llvm::dyn_cast<llvm::InvokeInst>(Call)) {
        New = llvm::InvokeInst::Create(NF, II->getNormalDest(), II->getUnwindDest(),
                                 Args, "", Before);
        llvm::cast<llvm::InvokeInst>(New)->setCallingConv(CS.getCallingConv());
        llvm::cast<llvm::InvokeInst>(New)->setAttributes(CS.getAttributes());
      } else {
        New = llvm::CallInst::Create(NF, Args, "", Before);
        llvm::cast<llvm::CallInst>(New)->setCallingConv(CS.getCallingConv());
        llvm::cast<llvm::CallInst>(New)->setAttributes(CS.getAttributes());
		if (isInternalAccel) {
			ConstantInt * constInt = dyn_cast<ConstantInt>((New->op_begin()+1)->get());
			assert(constInt);
			int numOMPthreads = constInt->getValue().getZExtValue();
			New->setMetadata("OMP", MDNode::get(NF->getParent()->getContext(), MDString::get(NF->getParent()->getContext(), utostr(numOMPthreads))));
			New->setMetadata("OMPTYPE", MDNode::get(NF->getParent()->getContext(), MDString::get(NF->getParent()->getContext(), "ompcall")));
		}
        if (llvm::cast<llvm::CallInst>(Call)->isTailCall())
          llvm::cast<llvm::CallInst>(New)->setTailCall();
      }
	 Args.clear();

	 if (!Call->use_empty())
	   Call->replaceAllUsesWith(New);

	 New->takeName(Call);
	//CItest = New;
	 // Finally, remove the old call from the program, reducing the use-count of
	 // F.
	 Call->eraseFromParent();
	}

    //delete old function from HwFct if it existed
/*    std::set<GlobalValue*>::iterator iter;
    iter = HwFct.find(&Fn);
    if (iter != HwFct.end()) {
        HwFct.erase(iter);
    }*/
	//add the new function
	HwFct.insert(NF);

    //errs() << "New Function = " << NF->getName() << "\n";


	return NF;
}

//this function copies Fn to a new function, inserts new arguments in argValues to the new function, and replaces all calls from the original function to the new function
Function *insertNewArgumentsAndReplaceCalls2(Function &Fn, std::set<GlobalValue*> &HwFct, std::set<CallInst*> &ompCallInsts, const std::vector<std::string> &argNames, const std::vector<Value*> &argValues) {

	// Start by computing a new prototype for the function, which is the same as
	// the old function, but doesn't have isVarArg set.
	FunctionType *FTy = Fn.getFunctionType();

	std::vector<Type*> Params(FTy->param_begin(), FTy->param_end());
	unsigned NumArgs = Params.size();

	//insert the new arg types
	for (std::vector<Value*>::const_iterator it = argValues.begin(); it != argValues.end(); it++) {
		Params.push_back((*it)->getType());
	}

    //create the new function type    
	FunctionType *NFTy = FunctionType::get(FTy->getReturnType(),
												 Params, false);

	// Create the new function body and insert it into the module...
	Function *NF = Function::Create(NFTy, Fn.getLinkage());
//	Function *NF = Function::Create(NFTy, Function::ExternalLinkage);
	NF->copyAttributesFrom(&Fn);
	Fn.getParent()->getFunctionList().insert(&Fn, NF);
	
	NF->takeName(&Fn);

	// Loop over the argument list, transferring uses of the old arguments over to
	// the new arguments, also transferring over the names as well.  While we're at
	// it, remove the dead arguments from the DeadArguments list.
	//
	for (Function::arg_iterator I = Fn.arg_begin(), E = Fn.arg_end(),
		I2 = NF->arg_begin(); I != E; ++I, ++I2) {
	 // Move the name and users over to the new version.
	 I->replaceAllUsesWith(I2);
	 I2->takeName(I);
        //if we are at the last argument
		if (I == --(Fn.arg_end())) {
			//set the names of the new arguments
			for (std::vector<std::string>::const_iterator it = argNames.begin(); it != argNames.end(); it++) {
				I2++;
				I2->setName(*it);
			}
		}
	}

    //copy the function body over to new function and delete the old function body. 
	NF->getBasicBlockList().splice(NF->begin(), Fn.getBasicBlockList());

	// Loop over all of the callers of the function, transforming the call sites
    // to call the new function with the added arguments
	//
	std::vector<Value*> Args;
      llvm::Instruction *New;
      llvm::Instruction *Before;
	while (!Fn.use_empty()) {
	 CallSite CS(Fn.use_back());
     Instruction *Call = CS.getInstruction();

	 // Pass all the same arguments.
	 Args.assign(CS.arg_begin(), CS.arg_begin() + NumArgs);
	
//	 Args.push_back(newARG1);
					

	//Value *newARG = --NF->arg_end();
	//insert the new arguments
	for (std::vector<Value*>::const_iterator it = argValues.begin(); it != argValues.end(); it++) {
		Args.push_back(*it);
	}
	
	
      Before = Call;

    //create the new call instruction    
        New = llvm::CallInst::Create(NF, Args, "", Before);
        llvm::cast<llvm::CallInst>(New)->setCallingConv(CS.getCallingConv());
        llvm::cast<llvm::CallInst>(New)->setAttributes(CS.getAttributes());
        if (llvm::cast<llvm::CallInst>(Call)->isTailCall())
          llvm::cast<llvm::CallInst>(New)->setTailCall();
	 Args.clear();

     //replace uses with the old instruction
	 if (!Call->use_empty())
	   Call->replaceAllUsesWith(New);

     //take the name
	 New->takeName(Call);
	//CItest = New;
	 // Finally, remove the old call from the program, reducing the use-count of
	 // F.
	 Call->eraseFromParent();
	}
	//add the new function
	HwFct.insert(NF);
	return NF;
}

/*
//this function copies Fn to a new function, inserts new arguments in argValues to the new function, and replaces all calls from the original function to the new function
Function *insertNewArgumentsAndReplaceCalls2(Function &Fn, std::set<GlobalValue*> &HwFct, const std::vector<std::string> &argNames, const std::vector<Value*> &argValues) {

	// Start by computing a new prototype for the function, which is the same as
	// the old function, but doesn't have isVarArg set.
	FunctionType *FTy = Fn.getFunctionType();

	std::vector<Type*> Params(FTy->param_begin(), FTy->param_end());
	unsigned NumArgs = Params.size();

	//insert the new arg types
	for (std::vector<Value*>::const_iterator it = argValues.begin(); it != argValues.end(); it++) {
		Params.push_back((*it)->getType());
	}

    //create the new function type    
	FunctionType *NFTy = FunctionType::get(FTy->getReturnType(),
												 Params, false);

	// Create the new function body and insert it into the module...
	Function *NF = Function::Create(NFTy, Fn.getLinkage());
//	Function *NF = Function::Create(NFTy, Function::ExternalLinkage);
	NF->copyAttributesFrom(&Fn);
	Fn.getParent()->getFunctionList().insert(&Fn, NF);
	
	NF->takeName(&Fn);

	// Loop over the argument list, transferring uses of the old arguments over to
	// the new arguments, also transferring over the names as well.  While we're at
	// it, remove the dead arguments from the DeadArguments list.
	//
	for (Function::arg_iterator I = Fn.arg_begin(), E = Fn.arg_end(),
		I2 = NF->arg_begin(); I != E; ++I, ++I2) {
	 // Move the name and users over to the new version.
	 I->replaceAllUsesWith(I2);
	 I2->takeName(I);
        //if we are at the last argument
		if (I == --(Fn.arg_end())) {
			//set the names of the new arguments
			for (std::vector<std::string>::const_iterator it = argNames.begin(); it != argNames.end(); it++) {
				I2++;
				I2->setName(*it);
			}
		}
	}

    //copy the function body over to new function and delete the old function body. 
	NF->getBasicBlockList().splice(NF->begin(), Fn.getBasicBlockList());

	// Loop over all of the callers of the function, transforming the call sites
    // to call the new function with the added arguments
	//
	std::vector<Value*> Args;
      llvm::Instruction *New;
      llvm::Instruction *Before;
	while (!Fn.use_empty()) {
	 CallSite CS(Fn.use_back());
	 Instruction *Call = CS.getInstruction();

	 // Pass all the same arguments.
	 Args.assign(CS.arg_begin(), CS.arg_begin() + NumArgs);
	
//	 Args.push_back(newARG1);
					

	//Value *newARG = --NF->arg_end();
	//insert the new arguments
	for (std::vector<Value*>::const_iterator it = argValues.begin(); it != argValues.end(); it++) {
		Args.push_back(*it);
	}
	
	
      Before = Call;

    //create the new call instruction    
        New = llvm::CallInst::Create(NF, Args, "", Before);
        llvm::cast<llvm::CallInst>(New)->setCallingConv(CS.getCallingConv());
        llvm::cast<llvm::CallInst>(New)->setAttributes(CS.getAttributes());
        if (llvm::cast<llvm::CallInst>(Call)->isTailCall())
          llvm::cast<llvm::CallInst>(New)->setTailCall();
	 Args.clear();

     //replace uses with the old instruction
	 if (!Call->use_empty())
	   Call->replaceAllUsesWith(New);

     //take the name
	 New->takeName(Call);
	//CItest = New;
	 // Finally, remove the old call from the program, reducing the use-count of
	 // F.
	 Call->eraseFromParent();
	}
	//add the new function
	HwFct.insert(NF);
	return NF;
}
*/
//this function copies a function into a new function with the same function body but with an extra argument of type argType
//it returns the pointer to the new function
//Value * insertNewArgument(Module &M, std::set<GlobalValue*> &HwFct, std::string funcName, std::string argName, Type *argType) {
Function* insertNewArgument(Module &M, std::set<GlobalValue*> &HwFct, std::string funcName, std::string argName, Type *argType) {

	Function * F =	findFuncPtr(M, funcName.c_str());
	assert(F);

    // Start by computing a new prototype for the function, which is the same as
	// the old function, but has an extra argument.
	const FunctionType *FTy = F->getFunctionType();

	/* Copy the argument types and add an extra argument */
	std::vector<Type*> params(FTy->param_begin(), FTy->param_end());
	params.push_back(argType);

	// Make a new parameter attribute list (they are immutable) that has the new
	// argument marked as byval. Since the parameter attributes include the
	// return type parameters at index 0, we don't use size() - 1, but just
	// size() as index.
	AttrListPtr PAL = F->getAttributes();

	// Create the new function type based on the recomputed parameters.
	FunctionType *NFTy = FunctionType::get(F->getReturnType(), params, false);

	// Create the new function body and insert it into the module...
//	Function *NF = Function::Create(NFTy, F->getLinkage());
	Function *NF = Function::Create(NFTy, Function::ExternalLinkage);
	NF->copyAttributesFrom(F);
	NF->setAttributes(PAL);
	F->getParent()->getFunctionList().insert(F, NF);
	//M.getFunctionList().insert(F, NF);
	NF->takeName(F);
	//std::string newfuncName = "legup" + F->getName().str();
	//NF->setName(newfuncName);
	//copy the names of the arguments
	for (Function::arg_iterator AI = F->arg_begin(), AE = F->arg_end(),
		  NAI = NF->arg_begin(); AI != AE; ++AI, ++NAI)
	  NAI->takeName(AI);
	
	Value *newARG = --NF->arg_end();
	newARG->setName(argName);

    // Since we have now created the new function, splice the body of the old    
	// function right into the new function, leaving the old rotting hulk of the 
	// function empty.
	NF->getBasicBlockList().splice(NF->begin(), F->getBasicBlockList());

	// Replace all uses of the old arguments with the new arguments
	for (llvm::Function::arg_iterator I = F->arg_begin(), E = F->arg_end(),
		   NI = NF->arg_begin(); I != E; ++I, ++NI)
	  I->replaceAllUsesWith(NI);

	HwFct.insert(NF);

//	return newARG;
	return NF;
}

//this function find all the caller functions of the current function 
//and adds the functions pointers to a set
void findCallerFunctions(Function *F, std::set<Function*> &callerSet) {

	Function *CallerF;
	Instruction *Ins;
	for (Value::use_iterator UI = F->use_begin(), E = F->use_end(); UI != E;++UI){ 
		if (Ins = dyn_cast<Instruction>(*UI)) {
			//CallSite CS(cast<Instruction>(*UI));
			CallSite CS(Ins);
			Instruction *Call = CS.getInstruction();
	//		errs () << Call->getParent()->getParent()->getName().str() << "\n";
			if (Call) {
				CallerF = Call->getParent()->getParent();
				//errs () << CallerF->getName().str() << "\n";
                assert(CallerF);
                //TO DO: fix this later to combine with findCallerFunctions2
                //if (CallerF->getName().str() == "main" || LEGUP_CONFIG->isAccelerated(CallerF->getName().str())) {
                //    continue;
                //}
				//insert into set
				callerSet.insert(CallerF);
				//recurse
				findCallerFunctions(CallerF, callerSet);
			} 
		}
	}
}

//TO DO: fix this later to combine with findCallerFunctions (this one is used in SwOnly, the other is used in HwOnly)
void findCallerFunctions2(Function *F, std::set<Function*> &callerSet) {

	Function *CallerF;
	Instruction *Ins;
    int ompNumThreads = 0;
	for (Value::use_iterator UI = F->use_begin(), E = F->use_end(); UI != E;++UI){ 
		if (Ins = dyn_cast<Instruction>(*UI)) {
			//CallSite CS(cast<Instruction>(*UI));
			CallSite CS(Ins);
			Instruction *Call = CS.getInstruction();
	//		errs () << Call->getParent()->getParent()->getName().str() << "\n";
			if (Call) {
                //get the current function which contains this call instruction
				CallerF = Call->getParent()->getParent();
                assert(CallerF);
				//else insert into set
				callerSet.insert(CallerF);
				//recurse
				findCallerFunctions2(CallerF, callerSet);
			} 
		}
	}
}

void replaceCallInstructions(Function &Fn) {
//Function *insertNewArgumentsAndReplaceCalls2(Function &Fn, std::set<GlobalValue*> &HwFct, const std::vector<std::string> &argNames, const std::vector<Value*> &argValues) {

    int NumArgs = Fn.arg_size();
	// Loop over all of the callers of the function, transforming the call sites
    // to call the new function with the added arguments
	//
	std::vector<Value*> Args;
      llvm::Instruction *New;
      llvm::Instruction *Before;
	while (!Fn.use_empty()) {
         CallSite CS(Fn.use_back());
         Instruction *Call = CS.getInstruction();

         // Pass all the same arguments.
        Args.assign(CS.arg_begin(), CS.arg_begin() + NumArgs);
        
        Value *newARG = --Fn.arg_end();
        errs () << "Function name = " << Fn.getName() << "arg name = " << newARG->getName() << "\n";
        //insert the new arguments
        //for (std::vector<Value*>::const_iterator it = argValues.begin(); it != argValues.end(); it++) {
            Args.push_back(newARG);
        //}
        
        
          Before = Call;

        //create the new call instruction    
            New = llvm::CallInst::Create(&Fn, Args, "", Before);
            llvm::cast<llvm::CallInst>(New)->setCallingConv(CS.getCallingConv());
            llvm::cast<llvm::CallInst>(New)->setAttributes(CS.getAttributes());
            if (llvm::cast<llvm::CallInst>(Call)->isTailCall())
              llvm::cast<llvm::CallInst>(New)->setTailCall();
         Args.clear();

         //replace uses with the old instruction
         if (!Call->use_empty())
           Call->replaceAllUsesWith(New);

         //take the name
         New->takeName(Call);
        //CItest = New;
         // Finally, remove the old call from the program, reducing the use-count of
         // F.
         Call->eraseFromParent();
	}
}
} // end of legup namespace
