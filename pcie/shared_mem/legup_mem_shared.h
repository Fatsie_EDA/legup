#ifndef _LEGUP_MEM_SHARED_H
#define _LEGUP_MEM_SHARED_H

#include <stddef.h>

// Starting address in the shared memory space
#define BASE_ADDR 0x40000000
// Size of shared memory space in bytes
#ifndef HEAP_SIZE
#define HEAP_SIZE 786432
#endif
// Alignment in bytes
#define ALIGN 8

#define SIZE(x) (size_t)((x)->hdr & 0x7fffffff)

enum FREE_OR_USED_HDR {
  FREE_HDR = 0,
  USED_HDR = 1
};

struct s_node{
  size_t addr;
  struct s_node *prev, *next; /* next and prev free blocks, if applicable */
  struct s_node *up, *down; /* physically adjacent blocks for coalescing */
  unsigned hdr; /* (free or used) << 31 | size) */
};

typedef struct s_node S_NODE;

void *malloc_shared_internal(size_t size);
void free_shared_internal(void *ptr);

size_t base_addr();

#endif
