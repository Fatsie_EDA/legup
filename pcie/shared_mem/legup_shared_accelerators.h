#ifndef _LEGUP_SHARED_ACCELERATORS_H
#define _LEGUP_SHARED_ACCELERATORS_H

#include <pthread.h>
#include <assert.h>

typedef struct val_sem {
    pthread_mutex_t guard, arrayGuard;
    pthread_cond_t cond;
    unsigned count, index;
} val_sem_t;

//static val_sem_t sema = {PTHREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER, 4};

void init( val_sem_t *s )
{
    pthread_mutex_init( &s->guard, 0 );
    pthread_cond_init( &s->cond, 0 );
    s->count = 0;
}

void up( val_sem_t *s )
{
    assert( s );
    pthread_mutex_lock( &s->guard );
    s->count += 1;
    pthread_cond_broadcast( &s->cond );
    pthread_mutex_unlock( &s->guard );
}

void down( val_sem_t *s )
{
    assert( s );
    pthread_mutex_lock( &s->guard );
    do {
        if (s->count >= 1) {
            s->count -= 1;
            break;
        }
        pthread_cond_wait( &s->cond, &s->guard );
    } while (1);
    pthread_mutex_unlock( &s->guard );
}

unsigned getAccel( val_sem_t *s, unsigned *IDList)
{
    down(s);
    pthread_mutex_lock( &s->arrayGuard );
    unsigned ID = IDList[s->index];
    s->index += 1;
    pthread_mutex_unlock( &s->arrayGuard );
    return ID;
}

void freeAccel( val_sem_t *s, unsigned id, unsigned *IDList)
{
    pthread_mutex_lock( &s->arrayGuard );
    s->index -= 1;
    IDList[s->index] = id;
    pthread_mutex_unlock( &s->arrayGuard );
    up(s);
}

#endif
