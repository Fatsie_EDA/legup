#include "legup_mem.h"
#include "legup_mem_shared.h"
#include "alt_up_pci_lib_helper.h"

#include <stdio.h>

// Initialize the initial free block. Its free list pointers create a circular linked list to itself
S_NODE free_blk = {BASE_ADDR, &free_blk, &free_blk, NULL, NULL, (FREE_HDR << 31) | HEAP_SIZE};

void *malloc_shared(size_t size, void *original_ptr)
{
  return malloc_shared_internal(size);
}

void free_shared(void *ptr)
{
  free_shared_internal(ptr);
}

size_t base_addr()
{
  return BASE_ADDR;
}

void memcpy_from_shared(void *dst, void *src, size_t num)
{
  pci_read_dma(dst, (int)num, (int)src);
  pci_dma_go();
#ifdef DEBUG_SIM
  int debug_num = (int)num;
  long long *debug_dst = (long long *)dst;
  long long *debug_src = (long long *)src;

  // second argument is number of 64-bit values to read, where num is number of bytes to read
  printf("read 0x%x %zu\n", (int)debug_src, (num + sizeof(long long) - 1) / sizeof(long long));

  while (debug_num > 0) {
    long long debug_val = *debug_dst;
    // mask out undefined data if needed
    if (debug_num < sizeof(long long)) {
      debug_val &= -1LL >> 8 * (sizeof(long long) - debug_num);
    }
    printf("# Expected: 0x%016llx\n", debug_val);
    debug_dst++;
    debug_num -= sizeof(long long);
  }
#endif
}

void memcpy_to_shared(void *dst, void *src, size_t num)
{
#ifdef DEBUG_SIM
  int debug_num = (int)num;
  long long *debug_dst = (long long *)dst;
  long long *debug_src = (long long *)src;
  while (debug_num > 0) {
    long long debug_val = *debug_src;
    // mask out undefined data if needed
    if (debug_num < sizeof(long long)) {
      debug_val &= -1LL >> 8 * (sizeof(long long) - debug_num);
    }
    printf("write 0x%08x 0x%016llx\n", (int)debug_dst, debug_val);
    debug_dst++;
    debug_src++;
    debug_num -= sizeof(long long);
  }
#endif
  pci_write_dma(src, (int)num, (int)dst);
  pci_dma_go();
}
